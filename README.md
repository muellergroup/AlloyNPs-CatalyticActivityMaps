# Catalytic Activity Map for Alloy Nanoparticles

* Folder **PtNiOHVac-KMC-CE-javaCodes**: Java classes specific to this project.

* Folder **PtNiOHVac-CE-IO-files**: Input and output files for fitting the cluster expansions used in this project.
