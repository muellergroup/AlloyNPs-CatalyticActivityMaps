1. “clusters-PtNiSVac-quadElements”  file contains the clusters for Pt-Ni-OH@Pt-Vacancy CE.
2. “clusters-PtNiVac-tripleElements”  file contains the clusters for Pt-Ni-Vacancy CE.
3. "structures-trainingSet" file constains the training structures for CE (in the format of .vasp).
4. "2023-02-02-ECIs-list-PtNiOHVac-PtNiVac.xlsx" lists the fitted ECIs and the DFT-calculated and CE-predicted formation energies of training structures for both CEs. 
5. "energyList-trainingSet-PtNiSVac.txt" lists the structure names and corresponding DFT-calculated energies of Pt-Ni-OH@Pt-Vacancy training set. Elment S stands for "OH@Pt" in this project. 
6. "energyList-trainingSet-PtNiVac.txt" lists the structure names and corresponding DFT-calculated energies of Pt-Ni-Vacancy training set.
7. "PRIM-PtNi.vasp" is the PRIM file for Pt-Ni-Vacancy CE. 
8 "PRIM-PtNiS.vasp" is the PRIM file for Pt-Ni-OH@Pt-Vacancy CE. 