1. "NanoFitClusterExpansion_PtNiOH.java" is used to fit the ECIs of CE. 

2. "NanoFixedShape_Snapshot_KMC_Current_Driver.java" is the driver to run the KMC simulation of *OH desorption rate (specific activity).

3. "NanoSiteConcentration_OHAdsDes_KMC_Recorder_SplitGCN.java" & "NanoGCCoverageDepend_KMCCurrent_Recorder_SplitGCN_KMCTime.java" is used to run the KMC simulation of *OH desorption rate (specific activity).





