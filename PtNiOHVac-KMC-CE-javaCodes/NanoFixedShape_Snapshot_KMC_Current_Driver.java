/**
 * 
 */
package PtNiOH_JCAS_2023;
/**
 * @author liang
 *
 * Created on October 27,2019, snapshot (single NP) used to simulate the coverage-dependent ORR currnet on the surface of NP (the adsorbate: *OH)
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.NumberFormat;
import java.util.Random;
import java.util.StringTokenizer;

//import new_managers.NanoManagerLowMemVacancyHop_AllowGrowth_LT;
import new_managers.NanoSiteConcentration_OHAdsDes_KMC_Recorder;
import new_managers.NanoSiteConcentration_OHAdsDes_KMC_Recorder_SplitGCN;
import new_managers.NanoSiteConcentration_OHAdsDes_KMC_Recorder_allSites;
import matsci.Element;
import matsci.Species;
import matsci.engine.monte.metropolis.Metropolis;
import matsci.io.app.log.Status;
import matsci.io.clusterexpansion.PRIM;
import matsci.io.clusterexpansion.StructureListFile;
import matsci.io.vasp.POSCAR;
import matsci.location.Coordinates;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.location.symmetry.operations.SymmetryOperation;
import matsci.structure.Structure;
import matsci.structure.decorate.DecorationTemplate;
import matsci.structure.decorate.function.ce.AbstractAppliedCE;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.FastAppliedCE;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.monte.DecorationCanonicalManager;
import matsci.structure.decorate.function.monte.NanoCanonicalManagerLowMemFixedShape;
import matsci.structure.decorate.function.monte.SiteConcentrationRecorder;
import matsci.structure.superstructure.SuperStructure;
//import purePt_111_OH.FitCE_purePt_111_OH;



public class NanoFixedShape_Snapshot_KMC_Current_Driver {


    public static String ROOT = "ce/PtNiOH/";
    public static String STRUCT_DIR = ROOT + "/structures/";
    public static String CLUST_DIR = ROOT + "/clusters/";
    public static String CLUST_DIR_PTNIOH = ROOT + "/clusters_PtNiOH/";
    public static String GROUND_STATE_DIR = ROOT + "/groundStates/";
    public static String FIXEDSHAPE_DIR = ROOT + "/fixedShape/";
    public static String LAYERRECORDER_DIR = ROOT + "/fixedShape/layer-composition/";
    public static String SITERECORDER_DIR = ROOT + "/fixedShape/site-composition/";
    public static String SITERECORDER_DIR_NEW = ROOT + "/fixedShape/site-composition-new/";
    public static String VACANCYSITE_DIR = ROOT + "/vacancySite/";
    public static String DIFFUSIONENNI_DIR = ROOT + "/diffusionEnergyNi/";
    public static String TEMPSTRUCT_DIR = ROOT + "/tempStruct/";
    
    public static String ORR_PREDICTION_DIR = ROOT + "/ORR-prediction/";
    
    public static String OXYGENADSORPTION_DIR = ROOT + "/oxygenAdsorption/";
    
    public static String KMC_DIR = ROOT + "/dummy_diffusion/";
    
    
    public static NumberFormat numberFormat = NumberFormat.getNumberInstance();
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub


        
        String[] abc = new String[args.length];
        for(int i = 0; i < args.length & i < 100; i++){
               abc[i]=args[i];
        }
        
        //Status.setLogLevelBasic();
        Status.setLogLevelDetail(); //print out the generated expectation matrix
        
        
        //used to predict the SA of nanoparticles
        
        double muOH_Inner_KMC = 0.0;
        double numKMCIterationsPerSite = 30;
        double equilibraRatio_KMC = 0.5;
        Species OH_Spec = Species.sulfur;
        
        String fileDir_outer = "/483-cellSize=9-KMC-vary-compPt/";

        int cellSize = 21;


        ClusterExpansion ce = NanoFitClusterExpansion_PtNiOH.getPreFittedCE20201101_PRIM_PtNiOH_PtNi111_355_maxOH056ML(); //chosen on 2020/11/02

        
        //runGCMonteCarlo_KMC_Current_splitGCN_driver(ce, "CE=20201101_355_Pt111=1.035-peakS=0.118-SplitGCN", "/purePt-octahedral-NP-vary-size/", "LBL.random.cellSize=11-GC-thermo-muPt=-.45", 1, 1, 1, 0, 10, 20, 11, muOH_Inner_KMC, 30, equilibraRatio_KMC, OH_Spec) ;
        //runGCMonteCarlo_KMC_Current_splitGCN_driver(ce, "CE=20201101_355_Pt111=1.035-peakS=0.118-SplitGCN", "/6175-cellSize=21-KMC-vary-compPt/", "LBL.random.cellSize=21-oct-1-truncated-Pt4631Ni1544", 1, 1, 1, 0, 10, 20, 21, muOH_Inner_KMC, 30, equilibraRatio_KMC, OH_Spec) ;
        runGCMonteCarlo_KMC_Current_splitGCN_driver(ce, "CE=20201101_355_Pt111=1.035-peakS=0.118-SplitGCN", "/885-cellSize=11-KMC-vary-compPt/", "LBL.random.cellSize=11-oct-1-truncated-Pt664Ni221", 1, 3, 1, 0, 10, 20, 11, muOH_Inner_KMC, 30, equilibraRatio_KMC, OH_Spec) ;
        //runGCMonteCarlo_KMC_Current_splitGCN_driver(ce, "CE=20201101_355_Pt111=1.035-peakS=0.118-SplitGCN", "/fixedShaped-GC-thermo-varySize/", "LBL.random.cellSize=11-GC-thermo-muPt=-.45", 1, 1, 1, 0, 10, 20, 11, muOH_Inner_KMC, 30, equilibraRatio_KMC, OH_Spec) ;
        //runGCMonteCarlo_KMC_Current_splitGCN_driver(ce, abc[0], abc[1], abc[2], Integer.parseInt(abc[3]), Integer.parseInt(abc[4]), 1, 0, 10, 20, Integer.parseInt(abc[5]), muOH_Inner_KMC, Double.parseDouble(abc[6]), equilibraRatio_KMC, OH_Spec) ;
        
        //runGCMonteCarlo_KMC_Current_test(ce, fileDir_outer, "/" + fileDir_inner + "/", structName, 1, 0, 10, 20, cellSize, muOH_Inner_KMC , numKMCIterationsPerSite, equilibraRatio_KMC, OH_Spec) ;
        
        

        //used to predict the SA of extended surfaces (slab)
        /*
        double muOH_Inner_KMC = 0.0;
        double numKMCIterationsPerSite = 30;
        double equilibraRatio_KMC = 0.5;
        Species OH_Spec = Species.sulfur;
        
        String fileDir_outer = "/representative-slabs/";

        //int cellSize = 12;
        //String fileDir_inner = "7-layer-Pt792Ni216-PNAS-Fig5E-cleanslab-prim=144"; //cellSize=12
        //String fileDir_inner = "9-layer-Pt1000Ni296-JPCC-Fig10-cleanslab-prim=144"; //cellSize=12
        //String fileDir_inner = "9-layer-slab-29.7-cleanslab-prim=144"; //cellSize=12
        //String fileDir_inner = "9-layer-slab-25.11-cleanslab-prim=144"; //cellSize=12
        //String fileDir_inner = "9-layer-slab-25.11-new-cleanslab-prim=144"; //cellSize=12
        //String fileDir_inner = "9-layer-slab-36.0-cleanslab-prim=144-singleS"; //cellSize=12
        //String fileDir_inner = "9-layer-slab-36.0-cleanslab-prim=144-3.996"; //cellSize=12
        //String fileDir_inner = "9-layer-slab-36.0-cleanslab-prim=144-2S"; //cellSize=12
        //String fileDir_inner = "9-layer-slab-36.0-cleanslab-prim=144-Pt1295Ni1"; //cellSize=12
        //int cellSize = 4;
        //String fileDir_inner = "9-layer-slab-36.0-cleanslab-prim=16-2S"; //cellSize=4
        int cellSize = 12;
        String fileDir_inner = "9-layer-slab-36.0-cleanslab-prim=144-2S"; //cellSize=4
        //cellSize = 24;
        //String fileDir_inner = "9-layer-slab-36.0-cleanslab-prim=576-2S"; //cellSize=24

        String structName = "snapShot-afterKMC.vasp";
        
        ClusterExpansion ce = NanoFitClusterExpansion_PtNiOH.getPreFittedCE20201101_PRIM_PtNiOH_PtNi111_355_maxOH056ML(); //chosen on 2020/11/02

        runGCMonteCarlo_KMC_Current_slab(ce, fileDir_outer, "/" + fileDir_inner + "/", structName, 1, 0, 10, 20, cellSize, muOH_Inner_KMC, numKMCIterationsPerSite, equilibraRatio_KMC, OH_Spec) ;
        */
        

        
        
    }
    


    public static void runGCMonteCarlo_KMC_Current_splitGCN_driver(ClusterExpansion ce, String ceVersion, String fileDir_outer, String fileName, int paraRunStartIndex, int paraRunEndIndex, int numPasses2, int startTemp2, int endTemp2, int tempIncre, int cellSize, double muOH_Inner_KMC, double numKMCIterationsPerSite, double equilibraRatio_KMC, Species OH_Spec) {
        
        try{

            String predictedSA_path = ORR_PREDICTION_DIR + "/" + ceVersion + "/";
            
            File dirPredictedSA  = new  File(predictedSA_path);
            if ( ! (dirPredictedSA.exists())  &&   ! (dirPredictedSA.isDirectory())) {
                    boolean  creadok  =  dirPredictedSA.mkdirs();
            }
            
            String sub_fileName = fileName.substring(20);
            
            FileWriter fw0 = new FileWriter(predictedSA_path + "/IteN=" + numKMCIterationsPerSite + "-" + sub_fileName + "-s=" + paraRunStartIndex + "-e=" + paraRunEndIndex +  ".txt", false);

            BufferedWriter bw0 = new BufferedWriter(fw0); 
          
            bw0.write("structName   temp numPt numNi numPtSurface numPt_6.67<=GCN numPt_EdgeVertex ratio_6.67<=GCN ratio_MA avgE avgE2 cV muOH_Inner_KMC avgCurrent avgAdsCurrent maxCurrent avgOHOccupancy avgGCNFaceOHOccupancy avgEdgeOHOccupancy avgEads avgGCNFaceEads avgEdgeEads avgEads2 stdDev " + "\r\n");
            bw0.flush();
            

            for (int entryNum = paraRunStartIndex; entryNum <= paraRunEndIndex; entryNum++) {
    
                System.out.println("\n\n**************************************************************");
                System.out.println("path and structure name used for KMC SAMA: ");
                System.out.println("fileDir_outer: " + fileDir_outer);
                System.out.println("fileName: " + fileName + "-" + entryNum);
                System.out.println("**************************************************************\n\n");

                
                String structName = "snapShot-afterKMC.vasp";
                int size1 = cellSize;

                
                double [] returnArr = runGCMonteCarlo_KMC_Current_splitGCN(ce, "/" + fileDir_outer + "/", fileName + "-" + entryNum + "/", structName, numPasses2, startTemp2, endTemp2, tempIncre, cellSize, muOH_Inner_KMC, numKMCIterationsPerSite, equilibraRatio_KMC, OH_Spec) ;


                Status.detail("structName: " + structName + "\tKMC_avgCur: " + returnArr[7] + "\tOH_coverage: " + returnArr[10]);

                bw0.write(structName +  "  " + returnArr[0] + "  " + returnArr[1] + "  " + returnArr[2] + "  " + returnArr[18] + "  " + returnArr[19] + "  " + returnArr[20] + "  " + returnArr[21] + "  " + returnArr[22] +
                
                        "  " + returnArr[3] + "  " + returnArr[4] + "  " + returnArr[5] + "    " + returnArr[6] + "  " + returnArr[7] + "  " + returnArr[8] + "  " + returnArr[9] + "  " + returnArr[10] + "  " + returnArr[11] + "  " + returnArr[12] + "    " + returnArr[13] + "    " + returnArr[14] + "    " + returnArr[15] + "    " + returnArr[16] + "    " + returnArr[17] + "\r\n");


                
                bw0.flush();

            }
            
            
            
            bw0.flush();
            bw0.close();
            fw0.close();
            

        }
            
        catch (IOException e) {
            e.printStackTrace();
        }

    }
    
    
    
    
    
    
    
    public static void runGCMonteCarlo_KMC_Current_eachSite_driver(ClusterExpansion ce, String ceVersion, String fileDir_outer, String fileName, int paraRunStartIndex, int paraRunEndIndex, int numPasses2, int startTemp2, int endTemp2, int tempIncre, int cellSize, double muOH_Inner_KMC, double numKMCIterationsPerSite, double equilibraRatio_KMC, Species OH_Spec) {
        
        try{

            String predictedSA_path = ORR_PREDICTION_DIR + "/" + ceVersion + "/";
            
            File dirPredictedSA  = new  File(predictedSA_path);
            if ( ! (dirPredictedSA.exists())  &&   ! (dirPredictedSA.isDirectory())) {
                    boolean  creadok  =  dirPredictedSA.mkdirs();
            }
            
            String sub_fileName = fileName.substring(20);
            
            FileWriter fw0 = new FileWriter(predictedSA_path + "/IteN=" + numKMCIterationsPerSite + "-" + sub_fileName + "-muOH=" + muOH_Inner_KMC + "-s=" + paraRunStartIndex + "-e=" + paraRunEndIndex +  ".txt", false);

            BufferedWriter bw0 = new BufferedWriter(fw0); 
          
            bw0.write("structName   temp numPt numNi numPtSurface numPt_111 numPt_EdgeVertex ratio_111 ratio_MA avgE avgE2 cV muOH_Inner_KMC avgCurrent avgAdsCurrent maxCurrent avgOHOccupancy avgFaceOHOccupancy avgEdgeOHOccupancy avgEads avgFaceEads avgEdgeEads avgEads2 stdDev " + "\r\n");
            bw0.flush();
            
            for (int entryNum = paraRunStartIndex; entryNum <= paraRunEndIndex; entryNum++) {
    
                System.out.println("\n\n**************************************************************");
                System.out.println("path and structure name used for KMC SAMA: ");
                System.out.println("fileDir_outer: " + fileDir_outer);
                System.out.println("fileName: " + fileName + "-" + entryNum);
                System.out.println("**************************************************************\n\n");

                //String structName = enerIn.getFileName(entryNum);
                //int size1 = (int)enerIn.getEnergy(entryNum);
                
                String structName = "snapShot-afterKMC.vasp";
                int size1 = cellSize;

                double [] returnArr = runGCMonteCarlo_KMC_Current_eachSite(ce, "/" + fileDir_outer + "/", fileName + "-" + entryNum + "/", structName, numPasses2, startTemp2, endTemp2, tempIncre, cellSize, muOH_Inner_KMC, numKMCIterationsPerSite, equilibraRatio_KMC, OH_Spec) ;


                Status.detail("structName: " + structName + "\tKMC_avgCur: " + returnArr[7] + "\tOH_coverage: " + returnArr[10]);

                bw0.write(structName +  "  " + returnArr[0] + "  " + returnArr[1] + "  " + returnArr[2] + "  " + returnArr[18] + "  " + returnArr[19] + "  " + returnArr[20] + "  " + returnArr[21] + "  " + returnArr[22] +
                
                        "  " + returnArr[3] + "  " + returnArr[4] + "  " + returnArr[5] + "    " + returnArr[6] + "  " + returnArr[7] + "  " + returnArr[8] + "  " + returnArr[9] + "  " + returnArr[10] + "  " + returnArr[11] + "  " + returnArr[12] + "    " + returnArr[13] + "    " + returnArr[14] + "    " + returnArr[15] + "    " + returnArr[16] + "    " + returnArr[17] + "\r\n");

                
                bw0.flush();

            }
            
            
            
            bw0.flush();
            bw0.close();
            fw0.close();
            

        }
            
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    
    
    
    
    public static double[] runGCMonteCarlo_KMC_Current_splitGCN(ClusterExpansion ce, String fileDir_outer, String fileDir_inner, String initialStruct, int numPasses2, int startTemp2, int endTemp2, int tempIncre, int cellSize, double muOH_Inner_KMC, double numKMCIterations, double equilibraRatio_KMC, Species OH_Spec) {
        
        double[] returnArr = new double[23];

        
        double kb = 8.6173423E-5;
        //double stepsFor4 = 10;
        
        String fileDir = ORR_PREDICTION_DIR + fileDir_outer + fileDir_inner + "/";

        
        File dirFile  = new  File(fileDir);
        if ( ! (dirFile.exists())  &&   ! (dirFile.isDirectory())) {
                boolean  creadok  =  dirFile.mkdirs();
        }
        
        
        String fileDirInner = fileDir + "muOH_KMC=" + muOH_Inner_KMC + "-numKMCItes=" + numKMCIterations + "-KMCEquRatio=" + equilibraRatio_KMC + "/";
        File dirFileInner  = new  File(fileDirInner);
        if ( ! (dirFileInner.exists())  &&   ! (dirFileInner.isDirectory())) {
                boolean  creadok  =  dirFileInner.mkdirs();
        }
        
        ClusterGroup[] activeGroups = ce.getAllGroups(true);
        
        //double CutoffPerSite = 0;
        double CutoffPerSite = 1E-5;
        System.out.println("Using Cutoff per site of " + CutoffPerSite + " to trim insignificant groups...");
        activeGroups = ce.getSignificantGroups(activeGroups, ce.numSigmaSites() * CutoffPerSite);
        System.out.println(activeGroups.length + " significant groups remaining.\n\n");
        
        double startTemp = startTemp2;
        double endTemp = endTemp2;
        double tempIncrement = tempIncre;
        
        int numPasses = numPasses2;
        System.out.println("the number of numPasses : " + numPasses);

        /*
         * chemical potential of all species
         */
        
        double chemPotPt = 0.0;
        double chemPotNi = 0.0; 
       
        
        int[][] superToDirect = new int[][] {
                {-cellSize, cellSize, cellSize},
                {cellSize, -cellSize, cellSize},
                {cellSize, cellSize, -cellSize}
        };
        
        AbstractAppliedCE appliedCE = new FastAppliedCE(ce, superToDirect);  // Applies the cluster expansion to a superstructure
        System.out.println("Supercell has " + appliedCE.numPrimCells() + " primitive cells.");
        appliedCE.activateGroups(activeGroups);
        System.out.println("the number of sites is: " + appliedCE.numSigmaSites());
        
        SuperStructure groundState2 = appliedCE.getSuperStructure();
        
        POSCAR outfile2= new POSCAR(groundState2);
        outfile2.writeFile(fileDirInner + "groundState_0.vasp");
        System.out.println("write out the initial structure before decoration!!!!!!");
        
        Metropolis metropolis = new Metropolis();
        metropolis.setOptimistic(true);
        
        POSCAR infile = new POSCAR(fileDir + initialStruct, true);
        Structure structure = new Structure(infile);
        appliedCE.decorateFromStructure(structure);
        
        SuperStructure groundState3 = appliedCE.getSuperStructure();
        
        POSCAR outfile3= new POSCAR(groundState3);
        outfile3.writeFile(fileDirInner + "groundState_1.vasp");
        System.out.println("write out the structure after decoration!!!!!!");
        
        int numPt = structure.numDefiningSitesWithSpecies(Species.platinum);
        int numNi = structure.numDefiningSitesWithSpecies(Species.nickel);
        int numVac = appliedCE.numSigmaSites() - numNi - numPt;
        int numAtoms = numPt + numNi;
        
        //DecorationCrossCanonicalManager manager = new DecorationCrossCanonicalManager(appliedCE); // Use this line if you want a canonical run
        NanoCanonicalManagerLowMemFixedShape manager = new NanoCanonicalManagerLowMemFixedShape(appliedCE);
        
        metropolis.setTemp(startTemp * kb); // Whatever you want
        
        //structMetropolis.setNumIterations(numPasses * appliedCE.numSigmaSites());
        metropolis.setNumIterations(1);
        Status.detail("Running equilibration for " + metropolis.getNumIterations() + " steps...");
        metropolis.runBasic(manager);

        //System.out.println("Initial FBeta: " + totalFBeta);
        //System.out.println("Running for real...");
        
        
        if ((endTemp - startTemp) / tempIncrement < 0) {
            tempIncrement *= -1;
        }
        int numSteps = (int) Math.floor((endTemp - startTemp) / tempIncrement);
        System.out.println("the # of step of Monte Carlo simulation is: " + numSteps + "\n\n");
        
        Structure groundState = null;
        
        
        try{
            
            //FileWriter fw = new FileWriter(fileDirInner + "compositionProfile.txt", false);
            FileWriter fw = new FileWriter(fileDir + "/muOH_KMC=" + muOH_Inner_KMC + "-nKMCItes=" + numKMCIterations + "-avgCurrent.txt", false);
            BufferedWriter bw = new BufferedWriter(fw); 
            
            bw.write("temp numPt numNi numPtSurface numPt_6.67<=GCN numPt_EdgeVertex ratio_6.67<=GCN ratio_MA avgE avgE2 cV muOH_Inner_KMC avgCurrent avgAdsCurrent maxCurrent avgOHOccupancy avgGCNFaceOHOccupancy avgEdgeOHOccupancy avgEads avgGCNFaceEads avgEdgeEads avgEads2 stdDev " + "\r\n");
            bw.flush();
            
            for (int stepNum = 0; stepNum <= numSteps; stepNum++) {
                
                double temp = startTemp + stepNum * tempIncrement;
                metropolis.setTemp(kb * temp);
                
                                
                //TODO
                NanoSiteConcentration_OHAdsDes_KMC_Recorder_SplitGCN recorder = new NanoSiteConcentration_OHAdsDes_KMC_Recorder_SplitGCN(manager, appliedCE, muOH_Inner_KMC, numKMCIterations, equilibraRatio_KMC, OH_Spec); 
                //NanoSiteConcentration_OHAdsDes_KMC_Recorder_SplitGCN_KMCTime recorder = new NanoSiteConcentration_OHAdsDes_KMC_Recorder_SplitGCN_KMCTime(manager, appliedCE, muOH_Inner_KMC, numKMCIterations, equilibraRatio_KMC, OH_Spec); 

                // just for symmetrical Monte Carlo simulations
               // Equilibration            
                //structMetropolis.setNumIterations(numPasses * appliedCE.numSigmaSites());
                //structMetropolis.runBasic(manager);

                // Run for real
                //structMetropolis.setNumIterations(numPasses * appliedCE.numSigmaSites());         
                metropolis.setNumIterations(1);         
                Status.detail("Running equilibration for " + metropolis.getNumIterations() + " steps!!!");
                metropolis.run(manager, recorder);
                        
                Status.detail("Temperature: " + metropolis.getTemp());
                Status.detail("Trigger ratio: " + recorder.getTriggerRatio());
                Status.detail("Average energy: " + recorder.getAverageValue());
                

             
                /*
                 * print out the ground state for every temperature step
                 * 
                 */
                SuperStructure groundState4 = appliedCE.getSuperStructure();            
                POSCAR outfile4= new POSCAR(groundState4);                        
                outfile4.writeFile(fileDirInner + "snapShot." + stepNum + "-temp=" + temp + ".vasp");
                //outfile4.writeVICSOutFile(fileDirInner + "snapShot." + stepNum  + "-temp=" + temp + ".out"); 

                System.out.println("\nOuter metropolis run to get the cleanslab under ChemPot(Pt-Ni): ");
                System.out.println("Temperature: " + metropolis.getTemp());
                System.out.println("# of iterations: " + metropolis.getNumIterations());
                System.out.println("Trigger ratio: " + recorder.getTriggerRatio());
                System.out.println("Average energy: " + recorder.getAverageValue());
                
                System.out.println("Average current: " + recorder.getAverageCurrent());
                System.out.println("Average current from occupied sites: " + recorder.getAverageCurrentOccupied());
                System.out.println("Average current from unoccupied sites: " + recorder.getAverageCurrentUnOccupied());
                System.out.println("Average Ads current: " + recorder.getAverageAdsCurrent());
                System.out.println("Max current: " + recorder.getMaxAvgCurrent());
                System.out.println("Average OH Occupancy: " + recorder.getAverageOHOccupancy());
                System.out.println("Average OH Binding Energy: " + recorder.getAverageOHBindingEnergy());
                System.out.println("Average OH Binding Energy Sq: " + recorder.getAverageOHBindingEnergySq());
                double stdDev = Math.sqrt(recorder.getAverageOHBindingEnergySq() - (recorder.getAverageOHBindingEnergy() *  recorder.getAverageOHBindingEnergy()));
                System.out.println("Average OH Binding Energy Std Dev: " + stdDev);
                
                System.out.println("Average Face(GCN=7.5) OH Occupancy: " + recorder.getAverageFaceOHOccupancy());
                System.out.println("Average Face(GCN=7.5) OH Binding Energy: " + recorder.getAverageFaceOHBindingEnergy());
                
                System.out.println("Average Edge OH Occupancy: " + recorder.getAverageEdgeOHOccupancy());
                System.out.println("Average Edge OH Binding Energy: " + recorder.getAverageEdgeOHBindingEnergy());
                
                double avgE = recorder.getAverageValue();
                double avgE2 = recorder.getAverageSquaredValue();
                double sigSq = avgE2 - avgE * avgE;
                double cV = (avgE2 - avgE*avgE) / (kb * temp * temp);
                cV /= appliedCE.numSigmaSites();

                double maxCurrent = recorder.getMaxAvgCurrent();
                double avgCurrent = recorder.getAverageCurrent();
                double avgCurrentOccupied = recorder.getAverageCurrentOccupied();
                double avgCurrentUnOccupied = recorder.getAverageCurrentUnOccupied();                
                double avgAdsCurrent = recorder.getAverageAdsCurrent();
                double avgOHOccupancy = recorder.getAverageOHOccupancy();
                double avgFaceOHOccupancy = recorder.getAverageFaceOHOccupancy();
                double avgEdgeOHOccupancy = recorder.getAverageEdgeOHOccupancy();
                double avgEads =  recorder.getAverageOHBindingEnergy();
                double avgFaceEads =  recorder.getAverageFaceOHBindingEnergy();
                double avgEdgeEads =  recorder.getAverageEdgeOHBindingEnergy();
                double avgEads2 = recorder.getAverageOHBindingEnergySq();
                
                double avgKMCTime = recorder.getOutKMCTime();
                
                int totalSurfacePt = recorder.getNumOHSites();
                int totalSurfacePt_111 = recorder.getNumOHFaceSites();
                int totalSurfacePt_EdgeVertex = recorder.getNumOHEdgeSites();
                
                Structure sturctureMaxAvgCurrent = recorder.getStructureMaxTempAvgCurrent();
                
                POSCAR outfileMaxAvgCurrent= new POSCAR(sturctureMaxAvgCurrent);                                        outfileMaxAvgCurrent.writeFile(fileDirInner + "snapshot." + stepNum + "-temp=" + temp + "-maxAvgCurrent.vasp");
                
                
                System.out.println("\n\n****************************************************\n");
                System.out.println("the KMC time: " +  avgKMCTime + "\n");
                System.out.println("****************************************************\n\n");
                
                
                //System.out.println("Temp: " + temp + "\tConcentration: " + concentration + "\tE: " + avgE + "\tcv: " + cV+ "\tsigSq: " + sigSq);
                Status.detail("Temp: " + temp + "\tE: " + avgE + "\tcv: " + cV+ "\tsigSq: " + sigSq);
                
                Structure snapShot = appliedCE.getStructure(); 
              
                //System.out.println(stepNum);
                System.out.println("the num of steps is: " + stepNum + "\n");
                
                
                //String stringNum = temp + "   " + numPt + "   " + numNi + "   " + numOTem + "   " + compositionPtTemp_bulk + "   " + compositionPtTemp + "   " + latticeP_Temp_bulk + "   " + latticeP_Temp + "   " + strain_Temp_bulk + "   " + strain_Temp + "   " + shiftAdsEnergy + "  " + avgE + "   " + avgE2 + "   " + Cv_Slab + "   " + avgCurrent + "   " + avgAdsCurrent + "   " + maxCurrent + "   " + avgOOccupancy + "    " + avgEads + "    " + avgEads2 + "   " + stdDev + "   " + compositionNiSplitTemp[0] + "   " + compositionNiSplitTemp[1] + "   " + compositionNiSplitTemp[2] + "   " + compositionNiSplitTemp[3] + "   " + NiComp_567_Freeze[0] + "   " + NiComp_567_Freeze[1] + "   " + NiComp_567_Freeze[2] + "\r\n";
                
                String stringNum = temp + "   " + numPt + "   " + numNi + "   " + totalSurfacePt + "   " + totalSurfacePt_111 + "   " + totalSurfacePt_EdgeVertex + "   " + (((double)totalSurfacePt_111) / totalSurfacePt) + "   " + (((double)totalSurfacePt) / numPt) + "   " + avgE + "   " + avgE2 + "   " + cV + "   " + muOH_Inner_KMC + "   " + avgCurrent + "   " + avgAdsCurrent + "   " + maxCurrent + "   " + avgOHOccupancy + "    " + avgFaceOHOccupancy + "    " + avgEdgeOHOccupancy + "    " + avgEads + "    " + avgFaceEads + "    " + avgEdgeEads + "    " + avgEads2 + "   " + stdDev + "\r\n";
                
                System.out.println("Temp and Cv are: " + stringNum);
                bw.write(stringNum);    
                bw.flush();

                
                returnArr[0] = temp;
                returnArr[1] = numPt; 
                returnArr[2] = numNi; 
                returnArr[3] = avgE;
                returnArr[4] = avgE2;
                returnArr[5] = cV;
                returnArr[6] = muOH_Inner_KMC;
                returnArr[7] = avgCurrent;
                returnArr[8] = avgAdsCurrent; 
                returnArr[9] = maxCurrent ;
                returnArr[10] = avgOHOccupancy;
                returnArr[11] = avgFaceOHOccupancy;
                returnArr[12] = avgEdgeOHOccupancy;
                returnArr[13] = avgEads;
                returnArr[14] = avgFaceEads;
                returnArr[15] = avgEdgeEads;
                returnArr[16] = avgEads2;
                returnArr[17] = stdDev;

                returnArr[18] = totalSurfacePt;
                returnArr[19] = totalSurfacePt_111;
                returnArr[20] = totalSurfacePt_EdgeVertex;
                returnArr[21] = ((double)totalSurfacePt_111) / totalSurfacePt;
                returnArr[22] = ((double)totalSurfacePt) / numPt;

            } 
            
            bw.flush();
            bw.close();
            fw.close();

        }
            
        catch (IOException e) {
            e.printStackTrace();
        }
            

        
        groundState = appliedCE.getSuperStructure().getCompactStructure();
        int numPtF = groundState.numDefiningSitesWithSpecies(Species.platinum);
        int numNiF = groundState.numDefiningSitesWithSpecies(Species.nickel);
        POSCAR outfile5= new POSCAR(groundState);
        outfile5.setDescription("fixedShape");
        outfile5.writeFile(fileDirInner  + "groundState." + numPtF +"."+ numNiF + ".vasp");
        //outfile5.writeVICSOutFile(fileDirInner  + "groundState." + numPtF +"."+ numNiF + ".out");

        
        return returnArr;
        
      } 
      
    
    
    
    
    
    public static double[] runGCMonteCarlo_KMC_Current_eachSite(ClusterExpansion ce, String fileDir_outer, String fileDir_inner, String initialStruct, int numPasses2, int startTemp2, int endTemp2, int tempIncre, int cellSize, double muOH_Inner_KMC, double numKMCIterations, double equilibraRatio_KMC, Species OH_Spec) {
        
        double[] returnArr = new double[23];

        
        double kb = 8.6173423E-5;
        //double stepsFor4 = 10;
        
        String fileDir = ORR_PREDICTION_DIR + fileDir_outer + fileDir_inner + "/";

        
        File dirFile  = new  File(fileDir);
        if ( ! (dirFile.exists())  &&   ! (dirFile.isDirectory())) {
                boolean  creadok  =  dirFile.mkdirs();
        }
        
        
        String fileDirInner = fileDir + "muOH_KMC=" + muOH_Inner_KMC + "-numKMCItes=" + numKMCIterations + "-KMCEquRatio=" + equilibraRatio_KMC + "/";
        File dirFileInner  = new  File(fileDirInner);
        if ( ! (dirFileInner.exists())  &&   ! (dirFileInner.isDirectory())) {
                boolean  creadok  =  dirFileInner.mkdirs();
        }
        
        ClusterGroup[] activeGroups = ce.getAllGroups(true);
        
        //double CutoffPerSite = 0;
        double CutoffPerSite = 1E-5;
        System.out.println("Using Cutoff per site of " + CutoffPerSite + " to trim insignificant groups...");
        activeGroups = ce.getSignificantGroups(activeGroups, ce.numSigmaSites() * CutoffPerSite);
        System.out.println(activeGroups.length + " significant groups remaining.\n\n");
        
        double startTemp = startTemp2;
        double endTemp = endTemp2;
        double tempIncrement = tempIncre;
        
        int numPasses = numPasses2;
        System.out.println("the number of numPasses : " + numPasses);

        /*
         * chemical potential of all species
         */
        
        double chemPotPt = 0.0;
        double chemPotNi = 0.0; 
       
        
        int[][] superToDirect = new int[][] {
                {-cellSize, cellSize, cellSize},
                {cellSize, -cellSize, cellSize},
                {cellSize, cellSize, -cellSize}
        };
        
        AbstractAppliedCE appliedCE = new FastAppliedCE(ce, superToDirect);  // Applies the cluster expansion to a superstructure
        System.out.println("Supercell has " + appliedCE.numPrimCells() + " primitive cells.");
        appliedCE.activateGroups(activeGroups);
        System.out.println("the number of sites is: " + appliedCE.numSigmaSites());
        
        SuperStructure groundState2 = appliedCE.getSuperStructure();
        
        POSCAR outfile2= new POSCAR(groundState2);
        outfile2.writeFile(fileDirInner + "groundState_0.vasp");
        //outfile2.writeVICSOutFile(fileDirInner + "groundState_0.out");
        System.out.println("write out the initial structure before decoration!!!!!!");
        
        Metropolis metropolis = new Metropolis();
        metropolis.setOptimistic(true);
        
        POSCAR infile = new POSCAR(fileDir + initialStruct, true);
        Structure structure = new Structure(infile);
        appliedCE.decorateFromStructure(structure);
        
        SuperStructure groundState3 = appliedCE.getSuperStructure();
        
        POSCAR outfile3= new POSCAR(groundState3);
        outfile3.writeFile(fileDirInner + "groundState_1.vasp");
        System.out.println("write out the structure after decoration!!!!!!");

        int numPt = structure.numDefiningSitesWithSpecies(Species.platinum);
        int numNi = structure.numDefiningSitesWithSpecies(Species.nickel);
        int numVac = appliedCE.numSigmaSites() - numNi - numPt;
        int numAtoms = numPt + numNi;
        

        NanoCanonicalManagerLowMemFixedShape manager = new NanoCanonicalManagerLowMemFixedShape(appliedCE);
        
        metropolis.setTemp(startTemp * kb); // Whatever you want
        
        //structMetropolis.setNumIterations(numPasses * appliedCE.numSigmaSites());
        metropolis.setNumIterations(1);
        Status.detail("Running equilibration for " + metropolis.getNumIterations() + " steps...");
        metropolis.runBasic(manager);

        //System.out.println("Initial FBeta: " + totalFBeta);
        //System.out.println("Running for real...");
        
        
        if ((endTemp - startTemp) / tempIncrement < 0) {
            tempIncrement *= -1;
        }
        int numSteps = (int) Math.floor((endTemp - startTemp) / tempIncrement);
        System.out.println("the # of step of Monte Carlo simulation is: " + numSteps + "\n\n");
        
        Structure groundState = null;
        
        
        try{
            
            //FileWriter fw = new FileWriter(fileDirInner + "compositionProfile.txt", false);
            FileWriter fw = new FileWriter(fileDir + "/muOH_KMC=" + muOH_Inner_KMC + "-nKMCItes=" + numKMCIterations + "-avgCurrent.txt", false);
            BufferedWriter bw = new BufferedWriter(fw); 
            
            bw.write("temp numPt numNi numPtSurface numPt_111 numPt_EdgeVertex ratio_111 ratio_MA avgE avgE2 cV muOH_Inner_KMC avgCurrent avgAdsCurrent maxCurrent avgOHOccupancy avgFaceOHOccupancy avgEdgeOHOccupancy avgEads avgFaceEads avgEdgeEads avgEads2 stdDev " + "\r\n");
            bw.flush();
            
            
            
            FileWriter fw_eachSite = new FileWriter(fileDir + "/muOH_KMC=" + muOH_Inner_KMC + "-nKMCItes=" + numKMCIterations + "-avgEads-eachSite.txt", false);
            BufferedWriter bw_eachSite = new BufferedWriter(fw_eachSite); 
            
            bw_eachSite.write("siteNum surfaceSiteIndex surfaceSiteSigmaIndex avgEads CN avgEads2 stdDev " + "\r\n");
            bw_eachSite.flush();
            
            for (int stepNum = 0; stepNum <= numSteps; stepNum++) {
                
                double temp = startTemp + stepNum * tempIncrement;
                metropolis.setTemp(kb * temp);

                //TODO
                //NanoSiteConcentration_OHAdsDes_KMC_Recorder recorder = new NanoSiteConcentration_OHAdsDes_KMC_Recorder(manager, appliedCE, muOH_Inner_KMC, numKMCIterations, equilibraRatio_KMC, OH_Spec); 
                NanoSiteConcentration_OHAdsDes_KMC_Recorder_allSites recorder = new NanoSiteConcentration_OHAdsDes_KMC_Recorder_allSites(manager, appliedCE, structure, muOH_Inner_KMC, numKMCIterations, equilibraRatio_KMC, OH_Spec); 

                // just for symmetrical Monte Carlo simulations
               // Equilibration            
                //structMetropolis.setNumIterations(numPasses * appliedCE.numSigmaSites());
                //structMetropolis.runBasic(manager);

                // Run for real
                //structMetropolis.setNumIterations(numPasses * appliedCE.numSigmaSites());         
                metropolis.setNumIterations(1);         
                Status.detail("Running equilibration for " + metropolis.getNumIterations() + " steps!!!");
                metropolis.run(manager, recorder);
                        
                Status.detail("Temperature: " + metropolis.getTemp());
                Status.detail("Trigger ratio: " + recorder.getTriggerRatio());
                Status.detail("Average energy: " + recorder.getAverageValue());
                

             
                /*
                 * print out the ground state for every temperature step
                 * 
                 */
                SuperStructure groundState4 = appliedCE.getSuperStructure();            
                POSCAR outfile4= new POSCAR(groundState4);                        
                outfile4.writeFile(fileDirInner + "snapShot." + stepNum + "-temp=" + temp + ".vasp");
                //outfile4.writeVICSOutFile(fileDirInner + "snapShot." + stepNum  + "-temp=" + temp + ".out"); 

                System.out.println("\nOuter metropolis run to get the cleanslab under ChemPot(Pt-Ni): ");
                System.out.println("Temperature: " + metropolis.getTemp());
                System.out.println("# of iterations: " + metropolis.getNumIterations());
                System.out.println("Trigger ratio: " + recorder.getTriggerRatio());
                System.out.println("Average energy: " + recorder.getAverageValue());
                
                System.out.println("Average current: " + recorder.getAverageCurrent());
                System.out.println("Average current from occupied sites: " + recorder.getAverageCurrentOccupied());
                System.out.println("Average current from unoccupied sites: " + recorder.getAverageCurrentUnOccupied());
                System.out.println("Average Ads current: " + recorder.getAverageAdsCurrent());
                System.out.println("Max current: " + recorder.getMaxAvgCurrent());
                System.out.println("Average OH Occupancy: " + recorder.getAverageOHOccupancy());
                System.out.println("Average OH Binding Energy: " + recorder.getAverageOHBindingEnergy());
                System.out.println("Average OH Binding Energy Sq: " + recorder.getAverageOHBindingEnergySq());
                double stdDev = Math.sqrt(recorder.getAverageOHBindingEnergySq() - (recorder.getAverageOHBindingEnergy() *  recorder.getAverageOHBindingEnergy()));
                System.out.println("Average OH Binding Energy Std Dev: " + stdDev);
                
                System.out.println("Average Face OH Occupancy: " + recorder.getAverageFaceOHOccupancy());
                System.out.println("Average Face OH Binding Energy: " + recorder.getAverageFaceOHBindingEnergy());
                
                System.out.println("Average Edge OH Occupancy: " + recorder.getAverageEdgeOHOccupancy());
                System.out.println("Average Edge OH Binding Energy: " + recorder.getAverageEdgeOHBindingEnergy());

                
                double avgE = recorder.getAverageValue();
                double avgE2 = recorder.getAverageSquaredValue();
                double sigSq = avgE2 - avgE * avgE;
                double cV = (avgE2 - avgE*avgE) / (kb * temp * temp);
                cV /= appliedCE.numSigmaSites();

                double maxCurrent = recorder.getMaxAvgCurrent();
                double avgCurrent = recorder.getAverageCurrent();
                double avgCurrentOccupied = recorder.getAverageCurrentOccupied();
                double avgCurrentUnOccupied = recorder.getAverageCurrentUnOccupied();                
                double avgAdsCurrent = recorder.getAverageAdsCurrent();
                double avgOHOccupancy = recorder.getAverageOHOccupancy();
                double avgFaceOHOccupancy = recorder.getAverageFaceOHOccupancy();
                double avgEdgeOHOccupancy = recorder.getAverageEdgeOHOccupancy();
                double avgEads =  recorder.getAverageOHBindingEnergy();
                double avgFaceEads =  recorder.getAverageFaceOHBindingEnergy();
                double avgEdgeEads =  recorder.getAverageEdgeOHBindingEnergy();
                double avgEads2 = recorder.getAverageOHBindingEnergySq();
                
                double[] avgEadsAllSites = recorder.getAverageOHBindingEnergyEachSite();
                int[] allSurfaceSitesIndices = recorder.getSurfaceOHSitesIndices();
                int[] allSurfaceSitesSigmaIndices = recorder.getSurfaceOHSitesSigmaIndices();

                int[] allSurfaceSitesCN = recorder.getSurfaceOHSitesCN();

                for(int siteNum=0; siteNum< avgEadsAllSites.length; siteNum++) {
                    String inputStr = siteNum + "   " + allSurfaceSitesIndices[siteNum]  + "   " +   allSurfaceSitesSigmaIndices[siteNum]  + "   " +   avgEadsAllSites[siteNum] + "   " + allSurfaceSitesCN[siteNum] + "\r\n";
                    bw_eachSite.write(inputStr);    
                    bw_eachSite.flush();
                }
                
                int totalSurfacePt = recorder.getNumOHSites();
                int totalSurfacePt_111 = recorder.getNumOHFaceSites();
                int totalSurfacePt_EdgeVertex = recorder.getNumOHEdgeSites();
                
                Structure sturctureMaxAvgCurrent = recorder.getStructureMaxTempAvgCurrent();
                
                POSCAR outfileMaxAvgCurrent= new POSCAR(sturctureMaxAvgCurrent);                                        outfileMaxAvgCurrent.writeFile(fileDirInner + "snapshot." + stepNum + "-temp=" + temp + "-maxAvgCurrent.vasp");
                
                
                //System.out.println("Temp: " + temp + "\tConcentration: " + concentration + "\tE: " + avgE + "\tcv: " + cV+ "\tsigSq: " + sigSq);
                Status.detail("Temp: " + temp + "\tE: " + avgE + "\tcv: " + cV+ "\tsigSq: " + sigSq);
                
                Structure snapShot = appliedCE.getStructure(); 
              
                //System.out.println(stepNum);
                System.out.println("the num of steps is: " + stepNum + "\n");
                
                
                //String stringNum = temp + "   " + numPt + "   " + numNi + "   " + numOTem + "   " + compositionPtTemp_bulk + "   " + compositionPtTemp + "   " + latticeP_Temp_bulk + "   " + latticeP_Temp + "   " + strain_Temp_bulk + "   " + strain_Temp + "   " + shiftAdsEnergy + "  " + avgE + "   " + avgE2 + "   " + Cv_Slab + "   " + avgCurrent + "   " + avgAdsCurrent + "   " + maxCurrent + "   " + avgOOccupancy + "    " + avgEads + "    " + avgEads2 + "   " + stdDev + "   " + compositionNiSplitTemp[0] + "   " + compositionNiSplitTemp[1] + "   " + compositionNiSplitTemp[2] + "   " + compositionNiSplitTemp[3] + "   " + NiComp_567_Freeze[0] + "   " + NiComp_567_Freeze[1] + "   " + NiComp_567_Freeze[2] + "\r\n";
                
                String stringNum = temp + "   " + numPt + "   " + numNi + "   " + totalSurfacePt + "   " + totalSurfacePt_111 + "   " + totalSurfacePt_EdgeVertex + "   " + (((double)totalSurfacePt_111) / totalSurfacePt) + "   " + (((double)totalSurfacePt) / numPt) + "   " + avgE + "   " + avgE2 + "   " + cV + "   " + muOH_Inner_KMC + "   " + avgCurrent + "   " + avgAdsCurrent + "   " + maxCurrent + "   " + avgOHOccupancy + "    " + avgFaceOHOccupancy + "    " + avgEdgeOHOccupancy + "    " + avgEads + "    " + avgFaceEads + "    " + avgEdgeEads + "    " + avgEads2 + "   " + stdDev + "\r\n";
                
                System.out.println("Temp and Cv are: " + stringNum);
                bw.write(stringNum);    
                bw.flush();

                
                returnArr[0] = temp;
                returnArr[1] = numPt; 
                returnArr[2] = numNi; 
                returnArr[3] = avgE;
                returnArr[4] = avgE2;
                returnArr[5] = cV;
                returnArr[6] = muOH_Inner_KMC;
                returnArr[7] = avgCurrent;
                returnArr[8] = avgAdsCurrent; 
                returnArr[9] = maxCurrent ;
                returnArr[10] = avgOHOccupancy;
                returnArr[11] = avgFaceOHOccupancy;
                returnArr[12] = avgEdgeOHOccupancy;
                returnArr[13] = avgEads;
                returnArr[14] = avgFaceEads;
                returnArr[15] = avgEdgeEads;
                returnArr[16] = avgEads2;
                returnArr[17] = stdDev;

                returnArr[18] = totalSurfacePt;
                returnArr[19] = totalSurfacePt_111;
                returnArr[20] = totalSurfacePt_EdgeVertex;
                returnArr[21] = ((double)totalSurfacePt_111) / totalSurfacePt;
                returnArr[22] = ((double)totalSurfacePt) / numPt;

            } 
            
            bw.flush();
            bw.close();
            fw.close();

        }
            
        catch (IOException e) {
            e.printStackTrace();
        }
            

        
        groundState = appliedCE.getSuperStructure().getCompactStructure();
        int numPtF = groundState.numDefiningSitesWithSpecies(Species.platinum);
        int numNiF = groundState.numDefiningSitesWithSpecies(Species.nickel);
        POSCAR outfile5= new POSCAR(groundState);
        outfile5.setDescription("fixedShape");
        outfile5.writeFile(fileDirInner  + "groundState." + numPtF +"."+ numNiF + ".vasp");
        //outfile5.writeVICSOutFile(fileDirInner  + "groundState." + numPtF +"."+ numNiF + ".out");

        
        return returnArr;
        
      } 
      
    
    
    public static double[] runGCMonteCarlo_KMC_Current_slab(ClusterExpansion ce, String fileDir_outer, String fileDir_inner, String initialStruct, int numPasses2, int startTemp2, int endTemp2, int tempIncre, int cellSize, double muOH_Inner_KMC, double numKMCIterations, double equilibraRatio_KMC, Species OH_Spec) {
        
        double[] returnArr = new double[19];

        
        double kb = 8.6173423E-5;
        //double stepsFor4 = 10;
        
        //String fileDir = ORR_PREDICTION_DIR + "_variedOCoverage/" + initialStruct + "/";
        String fileDir = ORR_PREDICTION_DIR + fileDir_outer + fileDir_inner + "/";

        
        File dirFile  = new  File(fileDir);
        if ( ! (dirFile.exists())  &&   ! (dirFile.isDirectory())) {
                boolean  creadok  =  dirFile.mkdirs();
        }
        
        
        String fileDirInner = fileDir + "muOH_KMC=" + muOH_Inner_KMC + "-numKMCItes=" + numKMCIterations + "-KMCEquRatio=" + equilibraRatio_KMC + "/";
        File dirFileInner  = new  File(fileDirInner);
        if ( ! (dirFileInner.exists())  &&   ! (dirFileInner.isDirectory())) {
                boolean  creadok  =  dirFileInner.mkdirs();
        }
        
        ClusterGroup[] activeGroups = ce.getAllGroups(true);
        
        //double CutoffPerSite = 0;
        double CutoffPerSite = 1E-5;
        System.out.println("Using Cutoff per site of " + CutoffPerSite + " to trim insignificant groups...");
        activeGroups = ce.getSignificantGroups(activeGroups, ce.numSigmaSites() * CutoffPerSite);
        System.out.println(activeGroups.length + " significant groups remaining.\n\n");
        
        double startTemp = startTemp2;
        double endTemp = endTemp2;
        double tempIncrement = tempIncre;
        
        int numPasses = numPasses2;
        System.out.println("the number of numPasses : " + numPasses);

        /*
         * chemical potential of all species
         */
        
        double chemPotPt = 0.0;
        double chemPotNi = 0.0; 
       
        int x_slab = 2;
        int y_slab = 2;
        int z_slab = 6;
        
        x_slab = cellSize;
        y_slab = cellSize;
        
        /*
        //bad for "9-layer-slab-25.11-cleanslab-prim=144.vasp"
        //bad for "9-layer-slab-27.9-cleanslab-300K-prim=144.vasp"
        int[][] superToDirect = new int[][] {
                {x_slab, 0, -x_slab},
                {0, -y_slab, y_slab},
                {z_slab, z_slab, z_slab}
        }; 
        */
        
        
        
        int[][] superToDirect = new int[][] {
            {0, -y_slab, y_slab},
            {x_slab, 0, -x_slab},
            {-z_slab, -z_slab, -z_slab}
        }; 
        
    
        /*
        int[][] superToDirect = new int[][] {
                {-cellSize, cellSize, cellSize},
                {cellSize, -cellSize, cellSize},
                {cellSize, cellSize, -cellSize}
        };
        */
        
        
        AbstractAppliedCE appliedCE = new FastAppliedCE(ce, superToDirect);  // Applies the cluster expansion to a superstructure
        //AbstractAppliedCE appliedCE = new MakeMonteAppliedCE(ce, superToDirect);  // Applies the cluster expansion to a superstructure
        System.out.println("Supercell has " + appliedCE.numPrimCells() + " primitive cells.");
        appliedCE.activateGroups(activeGroups);
        System.out.println("the number of sites is: " + appliedCE.numSigmaSites());
        
        SuperStructure groundState2 = appliedCE.getSuperStructure();
        
        POSCAR outfile2= new POSCAR(groundState2);
        outfile2.writeFile(fileDirInner + "groundState_0.vasp");
        //outfile2.writeVICSOutFile(fileDirInner + "groundState_0.out");
        System.out.println("write out the initial structure before decoration!!!!!!");
        
        Metropolis metropolis = new Metropolis();
        metropolis.setOptimistic(true);
        
        //appliedCE.decorateRandomly(new int[] {0, 1, 2}, new int[] {numNi, numPt, numVac});

        
        //POSCAR infile = new POSCAR(fileDir + initialStruct + ".vasp", true);
        POSCAR infile = new POSCAR(fileDir + initialStruct, true);
        Structure structure = new Structure(infile);
        appliedCE.decorateFromStructure(structure);
        
        SuperStructure groundState3 = appliedCE.getSuperStructure();
        
        POSCAR outfile3= new POSCAR(groundState3);
        outfile3.writeFile(fileDirInner + "groundState_1.vasp");
        //outfile3.writeVICSOutFile(fileDirInner + "groundState_1.out");
        System.out.println("write out the structure after decoration!!!!!!");
        


        
        int numPt = structure.numDefiningSitesWithSpecies(Species.platinum);
        int numNi = structure.numDefiningSitesWithSpecies(Species.nickel);
        int numVac = appliedCE.numSigmaSites() - numNi - numPt;
        int numAtoms = numPt + numNi;
        
        NanoCanonicalManagerLowMemFixedShape manager = new NanoCanonicalManagerLowMemFixedShape(appliedCE);
        
        metropolis.setTemp(startTemp * kb); // Whatever you want
        
        //structMetropolis.setNumIterations(numPasses * appliedCE.numSigmaSites());
        metropolis.setNumIterations(1);
        Status.detail("Running equilibration for " + metropolis.getNumIterations() + " steps...");
        metropolis.runBasic(manager);

        //System.out.println("Initial FBeta: " + totalFBeta);
        //System.out.println("Running for real...");
        
        
        if ((endTemp - startTemp) / tempIncrement < 0) {
            tempIncrement *= -1;
        }
        int numSteps = (int) Math.floor((endTemp - startTemp) / tempIncrement);
        System.out.println("the # of step of Monte Carlo simulation is: " + numSteps + "\n\n");
        
        Structure groundState = null;
        
        
        try{
            
            //FileWriter fw = new FileWriter(fileDirInner + "compositionProfile.txt", false);
            FileWriter fw = new FileWriter(fileDir + "/muOH_KMC=" + muOH_Inner_KMC + "-nKMCItes=" + numKMCIterations + "-avgCurrent.txt", false);
            BufferedWriter bw = new BufferedWriter(fw); 
            
            bw.write("temp numPt numNi numPtSurface numPt_111 numPt_EdgeVertex ratio_111 ratio_MA avgE avgE2 cV muOH_Inner_KMC avgCurrent avgAdsCurrent maxCurrent avgOHOccupancy avgEads avgEads2 stdDev " + "\r\n");
            bw.flush();
            
            for (int stepNum = 0; stepNum <= numSteps; stepNum++) {
                
                double temp = startTemp + stepNum * tempIncrement;
                metropolis.setTemp(kb * temp);
                

                //TODO
                //NanoSiteConcentration_OHAdsDes_KMC_Recorder recorder = new NanoSiteConcentration_OHAdsDes_KMC_Recorder(manager, appliedCE, muOH_Inner_KMC, numKMCIterations, equilibraRatio_KMC, OH_Spec); 
                NanoSiteConcentration_OHAdsDes_KMC_Recorder_SplitGCN recorder = new NanoSiteConcentration_OHAdsDes_KMC_Recorder_SplitGCN(manager, appliedCE, muOH_Inner_KMC, numKMCIterations, equilibraRatio_KMC, OH_Spec); 


                // Run for real
                //structMetropolis.setNumIterations(numPasses * appliedCE.numSigmaSites());         
                metropolis.setNumIterations(1);         
                Status.detail("Running equilibration for " + metropolis.getNumIterations() + " steps!!!");
                metropolis.run(manager, recorder);
                        
                Status.detail("Temperature: " + metropolis.getTemp());
                Status.detail("Trigger ratio: " + recorder.getTriggerRatio());
                Status.detail("Average energy: " + recorder.getAverageValue());
                

             
                /*
                 * print out the ground state for every temperature step
                 * 
                 */
                SuperStructure groundState4 = appliedCE.getSuperStructure();            
                POSCAR outfile4= new POSCAR(groundState4);                        
                outfile4.writeFile(fileDirInner + "snapShot." + stepNum + "-temp=" + temp + ".vasp");
                //outfile4.writeVICSOutFile(fileDirInner + "snapShot." + stepNum  + "-temp=" + temp + ".out"); 

                System.out.println("\nOuter metropolis run to get the cleanslab under ChemPot(Pt-Ni): ");
                System.out.println("Temperature: " + metropolis.getTemp());
                System.out.println("# of iterations: " + metropolis.getNumIterations());
                System.out.println("Trigger ratio: " + recorder.getTriggerRatio());
                System.out.println("Average energy: " + recorder.getAverageValue());
                
                System.out.println("Average current: " + recorder.getAverageCurrent());
                System.out.println("Average current from occupied sites: " + recorder.getAverageCurrentOccupied());
                System.out.println("Average current from unoccupied sites: " + recorder.getAverageCurrentUnOccupied());
                System.out.println("Average Ads current: " + recorder.getAverageAdsCurrent());
                System.out.println("Max current: " + recorder.getMaxAvgCurrent());
                System.out.println("Average OH Occupancy: " + recorder.getAverageOHOccupancy());
                System.out.println("Average OH Binding Energy: " + recorder.getAverageOHBindingEnergy());
                System.out.println("Average OH Binding Energy Sq: " + recorder.getAverageOHBindingEnergySq());
                double stdDev = Math.sqrt(recorder.getAverageOHBindingEnergySq() - (recorder.getAverageOHBindingEnergy() *  recorder.getAverageOHBindingEnergy()));
                System.out.println("Average OH Binding Energy Std Dev: " + stdDev);
                
                
                double avgE = recorder.getAverageValue();
                double avgE2 = recorder.getAverageSquaredValue();
                double sigSq = avgE2 - avgE * avgE;
                double cV = (avgE2 - avgE*avgE) / (kb * temp * temp);
                cV /= appliedCE.numSigmaSites();

                double maxCurrent = recorder.getMaxAvgCurrent();
                double avgCurrent = recorder.getAverageCurrent();
                double avgCurrentOccupied = recorder.getAverageCurrentOccupied();
                double avgCurrentUnOccupied = recorder.getAverageCurrentUnOccupied();                
                double avgAdsCurrent = recorder.getAverageAdsCurrent();
                double avgOHOccupancy = recorder.getAverageOHOccupancy();
                double avgEads =  recorder.getAverageOHBindingEnergy();
                double avgEads2 = recorder.getAverageOHBindingEnergySq();
                
                int totalSurfacePt = recorder.getNumOHSites();
                int totalSurfacePt_111 = recorder.getNumOHFaceSites();
                int totalSurfacePt_EdgeVertex = recorder.getNumOHEdgeSites();
                
                Structure sturctureMaxAvgCurrent = recorder.getStructureMaxTempAvgCurrent();
                
                POSCAR outfileMaxAvgCurrent= new POSCAR(sturctureMaxAvgCurrent);                                        outfileMaxAvgCurrent.writeFile(fileDirInner + "snapshot." + stepNum + "-temp=" + temp + "-maxAvgCurrent.vasp");
                
                
                //System.out.println("Temp: " + temp + "\tConcentration: " + concentration + "\tE: " + avgE + "\tcv: " + cV+ "\tsigSq: " + sigSq);
                Status.detail("Temp: " + temp + "\tE: " + avgE + "\tcv: " + cV+ "\tsigSq: " + sigSq);
                
                Structure snapShot = appliedCE.getStructure(); 
              
                //System.out.println(stepNum);
                System.out.println("the num of steps is: " + stepNum + "\n");
                
                
                //String stringNum = temp + "   " + numPt + "   " + numNi + "   " + numOTem + "   " + compositionPtTemp_bulk + "   " + compositionPtTemp + "   " + latticeP_Temp_bulk + "   " + latticeP_Temp + "   " + strain_Temp_bulk + "   " + strain_Temp + "   " + shiftAdsEnergy + "  " + avgE + "   " + avgE2 + "   " + Cv_Slab + "   " + avgCurrent + "   " + avgAdsCurrent + "   " + maxCurrent + "   " + avgOOccupancy + "    " + avgEads + "    " + avgEads2 + "   " + stdDev + "   " + compositionNiSplitTemp[0] + "   " + compositionNiSplitTemp[1] + "   " + compositionNiSplitTemp[2] + "   " + compositionNiSplitTemp[3] + "   " + NiComp_567_Freeze[0] + "   " + NiComp_567_Freeze[1] + "   " + NiComp_567_Freeze[2] + "\r\n";
                
                String stringNum = temp + "   " + numPt + "   " + numNi + "   " + totalSurfacePt + "   " + totalSurfacePt_111 + "   " + totalSurfacePt_EdgeVertex + "   " + (((double)totalSurfacePt_111) / totalSurfacePt) + "   " + (((double)totalSurfacePt) / numPt) + "   " + avgE + "   " + avgE2 + "   " + cV + "   " + muOH_Inner_KMC + "   " + avgCurrent + "   " + avgAdsCurrent + "   " + maxCurrent + "   " + avgOHOccupancy + "    " + avgEads + "    " + avgEads2 + "   " + stdDev + "\r\n";
                
                System.out.println("Temp and Cv are: " + stringNum);
                bw.write(stringNum);    
                bw.flush();

                
                returnArr[0] = temp;
                returnArr[1] = numPt; 
                returnArr[2] = numNi; 
                returnArr[3] = avgE;
                returnArr[4] = avgE2;
                returnArr[5] = cV;
                returnArr[6] = muOH_Inner_KMC;
                returnArr[7] = avgCurrent;
                returnArr[8] = avgAdsCurrent; 
                returnArr[9] = maxCurrent ;
                returnArr[10] = avgOHOccupancy;
                returnArr[11] = avgEads;
                returnArr[12] = avgEads2;
                returnArr[13] = stdDev;

                returnArr[14] = totalSurfacePt;
                returnArr[15] = totalSurfacePt_111;
                returnArr[16] = totalSurfacePt_EdgeVertex;
                returnArr[17] = ((double)totalSurfacePt_111) / totalSurfacePt;
                returnArr[18] = ((double)totalSurfacePt) / numPt;

            } 
            
            bw.flush();
            bw.close();
            fw.close();

        }
            
        catch (IOException e) {
            e.printStackTrace();
        }
            

        
        groundState = appliedCE.getSuperStructure().getCompactStructure();
        int numPtF = groundState.numDefiningSitesWithSpecies(Species.platinum);
        int numNiF = groundState.numDefiningSitesWithSpecies(Species.nickel);
        POSCAR outfile5= new POSCAR(groundState);
        outfile5.setDescription("fixedShape");
        outfile5.writeFile(fileDirInner  + "groundState." + numPtF +"."+ numNiF + ".vasp");
        //outfile5.writeVICSOutFile(fileDirInner  + "groundState." + numPtF +"."+ numNiF + ".out");

        
        return returnArr;
        
      } 
      
    

    /**
     * 
     * method recorderCompositionEachLayer needs to get rid of 1st line, which stores the information of temperature.
     *  return composition of Pt, Ni and Mo in order of 1st, 2nd, 3rd, 4th, 5th and core compositions
     */
    public static double[] recorderCompositionEachLayer(String fileDir, String structName, String fileNameCu, String fileNameNi, String fileNamePt) {
        
        //ClusterExpansion ce = NanoFitClusterExpansion_PtNiOH.buildCE();
        ClusterExpansion ce = NanoFitClusterExpansion_PtNiOH.buildCE_dummy();
        PRIM infile = new PRIM(fileDir + "/" + structName);
        SuperStructure superStructure = ce.getBaseStructure().getSuperStructure(infile);
        
        double[] cuValues = new double[superStructure.numDefiningSites()];
        double[] niValues = new double[superStructure.numDefiningSites()];
        double[] ptValues = new double[superStructure.numDefiningSites()];


        double[] vacancyValues = new double[superStructure.numDefiningSites()];
        try {

          LineNumberReader cuReader = new LineNumberReader(new FileReader(fileDir + "/" + fileNameCu));
          LineNumberReader niReader = new LineNumberReader(new FileReader(fileDir + "/" + fileNameNi));
          LineNumberReader ptReader = new LineNumberReader(new FileReader(fileDir + "/" + fileNamePt));
          //LineNumberReader vacancyReader = new LineNumberReader(new FileReader(fileDir + "/" + fileNameVacancy));

          String cuLine = cuReader.readLine();
          String niLine = niReader.readLine();
          String ptLine = ptReader.readLine();
          //String vacancyLine = vacancyReader.readLine();
          
          /*
           * get rid of 1st line, which tells the info of temperature
           */
          cuLine = cuReader.readLine();
          niLine = niReader.readLine();
          ptLine = ptReader.readLine();
          for (int sigmaIndex = 0; sigmaIndex < niValues.length; sigmaIndex++) {
            

            StringTokenizer cuTokenizer = new StringTokenizer(cuLine);
            StringTokenizer niTokenizer = new StringTokenizer(niLine);
            StringTokenizer ptTokenizer = new StringTokenizer(ptLine);
            //StringTokenizer vacancyTokenizer = new StringTokenizer(vacancyLine);
            
            cuTokenizer.nextToken();
            niTokenizer.nextToken();
            ptTokenizer.nextToken();
            //vacancyTokenizer.nextToken();
            
            cuValues[sigmaIndex] += Double.parseDouble(cuTokenizer.nextToken());
            niValues[sigmaIndex] += Double.parseDouble(niTokenizer.nextToken());
            ptValues[sigmaIndex] += Double.parseDouble(ptTokenizer.nextToken());
            //vacancyValues[sigmaIndex] += Double.parseDouble(vacancyTokenizer.nextToken());
            
            cuLine = cuReader.readLine();
            niLine = niReader.readLine();
            ptLine = ptReader.readLine();
            //vacancyLine = vacancyReader.readLine();
          }
          
          cuReader.close();
          niReader.close();
          ptReader.close();
          //vacancyReader.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
        
        //niValues = symmetrizeValues(superStructure, niValues);
        //ptValues = symmetrizeValues(superStructure, ptValues);
        //vacancyValues = symmetrizeValues(superStructure, vacancyValues);
        
        //Arrays.fill(moValues, 0);
        
        //TODO, add codes to decorate all sites in vacancy, to distinguish from the atoms within the core of nanoparticles.
        
        int numSitesVacancy = 0;
        int numSitesLayer1 = 0;
        int numSitesLayer2 = 0;
        int numSitesLayer3 = 0;
        int numSitesLayer4 = 0;
        int numSitesLayer5 = 0;
        int numSitesCore = 0;
        boolean[] vacancySites = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {
              vacancySites[sigmaIndex] = true;
              numSitesVacancy ++;
          }

        }
        
        
        boolean[] surfaceSites = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          //if (vacancySites[sigmaIndex]) {continue;}
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (neighbors[neighborNum].getSpecies() == Species.vacancy) {
              surfaceSites[sigmaIndex] = true;
              numSitesLayer1++;
              break;
            }
          }
        }
        
        boolean[] surfaceSites2 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites[neighbors[neighborNum].getIndex()]) {
              surfaceSites2[sigmaIndex] = true;
              numSitesLayer2++;
              break;
            }
          }
        }
        
        boolean[] surfaceSites3 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites2[neighbors[neighborNum].getIndex()]) {
              surfaceSites3[sigmaIndex] = true;
              numSitesLayer3++;
              break;
            }
          }
        }
        
        boolean[] surfaceSites4 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          if (surfaceSites3[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites3[neighbors[neighborNum].getIndex()]) {
              surfaceSites4[sigmaIndex] = true;
              numSitesLayer4++;
              break;
            }
          }
        }
        
        
        boolean[] surfaceSites5 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          if (surfaceSites3[sigmaIndex]) {continue;}
          if (surfaceSites4[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites4[neighbors[neighborNum].getIndex()]) {
              surfaceSites5[sigmaIndex] = true;
              numSitesLayer5++;
              break;
            }
          }
        }
        
        /*
         * check the sites wihin the core(here, is deeper than 5th layer)
         */
        boolean[] coreSites = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          if (surfaceSites3[sigmaIndex]) {continue;}
          if (surfaceSites4[sigmaIndex]) {continue;}
          if (surfaceSites5[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          coreSites[sigmaIndex] = true;
          numSitesCore ++;
        }
        
        
        /*
         * calculate the composition for each layer
         */
        
        int numSitesVacancyTemp = 0;
        int numSitesL1Temp = 0;
        int numSitesL2Temp = 0;
        int numSitesL3Temp = 0;
        int numSitesL4Temp = 0;
        int numSitesL5Temp = 0;
        int numSitesCoreTemp = 0;
        
        double compositionPtL1 = 0;
        double compositionPtL2 = 0;
        double compositionPtL3 = 0;
        double compositionPtL4 = 0;
        double compositionPtL5 = 0;
        double compositionPtCore = 0;
        
        double compositionNiL1 = 0;
        double compositionNiL2 = 0;
        double compositionNiL3 = 0;
        double compositionNiL4 = 0;
        double compositionNiL5 = 0;
        double compositionNiCore = 0;
        
        double compositionCuL1 = 0;
        double compositionCuL2 = 0;
        double compositionCuL3 = 0;
        double compositionCuL4 = 0;
        double compositionCuL5 = 0;
        double compositionCuCore = 0;
        
        for (int siteNum = 0; siteNum < infile.numDefiningSites(); siteNum++) {
            Coordinates coords = infile.getSiteCoords(siteNum);
            int sigmaIndex = superStructure.getSiteIndex(coords);
            
            if(vacancySites[sigmaIndex]){
                numSitesVacancyTemp ++;
                continue;
            }
            
            if(surfaceSites[sigmaIndex]){
                numSitesL1Temp ++;
                compositionCuL1 += cuValues[sigmaIndex];
                compositionNiL1 += niValues[sigmaIndex];
                compositionPtL1 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites2[sigmaIndex]){
                numSitesL2Temp ++;
                compositionCuL2 += cuValues[sigmaIndex];
                compositionNiL2 += niValues[sigmaIndex];
                compositionPtL2 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites3[sigmaIndex]){
                numSitesL3Temp ++;
                compositionCuL3 += cuValues[sigmaIndex];
                compositionNiL3 += niValues[sigmaIndex];
                compositionPtL3 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites4[sigmaIndex]){
                numSitesL4Temp ++;
                compositionCuL4 += cuValues[sigmaIndex];
                compositionNiL4 += niValues[sigmaIndex];
                compositionPtL4 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites5[sigmaIndex]){
                numSitesL5Temp ++;
                compositionCuL5 += cuValues[sigmaIndex];
                compositionNiL5 += niValues[sigmaIndex];
                compositionPtL5 += ptValues[sigmaIndex];
                continue;
            }
            
            if(coreSites[sigmaIndex]){
                numSitesCoreTemp ++;
                compositionCuCore += cuValues[sigmaIndex];
                compositionNiCore += niValues[sigmaIndex];
                compositionPtCore += ptValues[sigmaIndex];
                continue;
            }
            
            System.out.println("this sigmaIndex is not considered when recorder the position of each site: " + sigmaIndex);
                     
        } // end of for loop
        
        
        if(numSitesL1Temp != numSitesLayer1){
            System.out.println("# of 1st layer does not agree!");
        }
        if(numSitesL2Temp != numSitesLayer2){
            System.out.println("# of 2nd layer does not agree!");
        }
        if(numSitesL3Temp != numSitesLayer3){
            System.out.println("# of 3rd layer does not agree!");
        }
        if(numSitesL4Temp != numSitesLayer4){
            System.out.println("# of 4th does not agree!");
        }
        if(numSitesL5Temp != numSitesLayer5){
            System.out.println("# of 5th does not agree!");
        }
        if(numSitesCoreTemp != numSitesCore){
            System.out.println("# of core sites does not agree!");
        }
        
        compositionCuL1 = compositionCuL1/ numSitesLayer1;
        compositionNiL1 = compositionNiL1/ numSitesLayer1;
        compositionPtL1 = compositionPtL1/ numSitesLayer1;
        
        compositionCuL2 = compositionCuL2/ numSitesLayer2;
        compositionNiL2 = compositionNiL2/ numSitesLayer2;
        compositionPtL2 = compositionPtL2/ numSitesLayer2;
        
        compositionCuL3 = compositionCuL3/ numSitesLayer3;
        compositionNiL3 = compositionNiL3/ numSitesLayer3;
        compositionPtL3 = compositionPtL3/ numSitesLayer3;
        
        compositionCuL4 = compositionCuL4/ numSitesLayer4;
        compositionNiL4 = compositionNiL4/ numSitesLayer4;
        compositionPtL4 = compositionPtL4/ numSitesLayer4;
        
        compositionCuL5 = compositionCuL5/ numSitesLayer5;
        compositionNiL5 = compositionNiL5/ numSitesLayer5;
        compositionPtL5 = compositionPtL5/ numSitesLayer5;
        
        compositionCuCore = compositionCuCore/ numSitesCore;
        compositionNiCore = compositionNiCore/ numSitesCore;
        compositionPtCore = compositionPtCore/ numSitesCore;
        
        int numAllAtoms = numSitesLayer1 + numSitesLayer2 + numSitesLayer3 + numSitesLayer4 + numSitesLayer5 + numSitesCore;
        
        System.out.println("# of sites for each layer are: " + numAllAtoms + "  " + numSitesLayer1 + "  " + numSitesLayer2 + "  " + numSitesLayer3 + "  " + numSitesLayer4 + "  " + numSitesLayer5 + "   " + numSitesCore + "    " + numSitesVacancy);
        
        System.out.println("Cu% for each layer are: " + compositionCuL1 + "  " + compositionCuL2 + "  " + compositionCuL3 + "  " + compositionCuL4 + "  " + compositionCuL5 + "   " + compositionCuCore);
        System.out.println("Ni% for each layer are: " + compositionNiL1 + "  " + compositionNiL2 + "  " + compositionNiL3 + "  " + compositionNiL4 + "  " + compositionNiL5 + "   " + compositionNiCore);
        System.out.println("Pt% for each layer are: " + compositionPtL1 + "  " + compositionPtL2 + "  " + compositionPtL3 + "  " + compositionPtL4 + "  " + compositionPtL5 + "   " + compositionPtCore);

        double[] compositionProfiles = new double[26];

        compositionProfiles[0] = compositionPtL1;
        compositionProfiles[1] = compositionPtL2;
        compositionProfiles[2] = compositionPtL3;
        compositionProfiles[3] = compositionPtL4;
        compositionProfiles[4] = compositionPtL5;
        compositionProfiles[5] = compositionPtCore;
        
        compositionProfiles[6] = compositionNiL1;
        compositionProfiles[7] = compositionNiL2;
        compositionProfiles[8] = compositionNiL3;
        compositionProfiles[9] = compositionNiL4;
        compositionProfiles[10] = compositionNiL5;
        compositionProfiles[11] = compositionNiCore;
        
        compositionProfiles[12] = compositionCuL1;
        compositionProfiles[13] = compositionCuL2;
        compositionProfiles[14] = compositionCuL3;
        compositionProfiles[15] = compositionCuL4;
        compositionProfiles[16] = compositionCuL5;
        compositionProfiles[17] = compositionCuCore;
        
        compositionProfiles[18] = numAllAtoms;
        compositionProfiles[19] = numSitesLayer1;
        compositionProfiles[20] = numSitesLayer2;
        compositionProfiles[21] = numSitesLayer3;
        compositionProfiles[22] = numSitesLayer4;
        compositionProfiles[23] = numSitesLayer5;
        compositionProfiles[24] = numSitesCore;
        compositionProfiles[25] = numSitesVacancy;
        
        return compositionProfiles;
        
        
      }
    
    
    

}


