/**
 * 
 */
package PtNiOH_JCAS_2023;


/**
 * 
 */


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Arrays;
import java.util.StringTokenizer;

//import binary.NanoFitClusterExpansion;
import matsci.Element;
import matsci.Species;
import matsci.engine.monte.metropolis.Metropolis;
import matsci.io.app.log.Status;
import matsci.io.clusterexpansion.PRIM;
import matsci.io.clusterexpansion.StructureListFile;
import matsci.io.vasp.POSCAR;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.model.linear.L2FullRLSFitter;
import matsci.model.reg.RegularizerSelector;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.structure.decorate.function.ce.AbstractAppliedCE;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.FastAppliedCE;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ILinearCERegGenerator;
import matsci.structure.decorate.function.ce.reg.PowExpRegGenerator;
import matsci.structure.decorate.function.ce.structures.StructureList;
import matsci.structure.function.ce.structures.ClustStructureData;
import matsci.structure.superstructure.SuperStructure;
import matsci.structure.symmetry.StructureMapper;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;


/**
 * @author Liang
 *
 */
public class NanoFitClusterExpansion_PtNiOH {

    
    public static String ROOT = "ce/PtNiOH/";
    public static String STRUCT_DIR = ROOT + "/structures/";
    public static String CONTCAR_DIR = ROOT + "/contcars/";
    public static String CLUST_DIR = ROOT + "/clusters/";
    public static String CLUST_DIR_PTNIOH = ROOT + "/clusters_PtNiOH/";
    public static String GROUND_STATE_DIR = ROOT + "/groundStates/";
    
    public static String DFT_CE_ERROR_DIR = ROOT + "/DFT_CE_error/";
    
    //public static String  = ROOT + "/groundStates/";


    /*
     * RPBE-PREC=Accurate, getKPoints
     */
    public static double PtEnergyBulkAccurate = -5.4101415;
    public static double NiEnergyBulkAccurate = -4.9485515;
    //public static double MoEnergyBulkAccurate = -9.9257503;
    //public static double CuEnergyBulkAccurate = -3.371883;
    public static double OHEnergyCorrectAccurate = 10.66793045;
    //public static double O2EnergyAccurate = -8.28;
    
    
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
        Status.setLogLevelDetail();
        //Status.setLogLevelBasic();
        
        /*
        //fit CE for PtNiSVac (particles with *OH)
        ClusterExpansion ce = buildCE_PtNiOH();
        addKnownStructures_PtNiOH_CalculateOHCoverage(ce, 1.0);      
        fitCE(ce);
        */
        
        
        //fit CE for PtNiVac (clean particles without *OH)
        ClusterExpansion ce = buildCE_PtNiVac();
        addKnownStructures_PtNiVac_cleanNP(ce);      
        fitCE(ce);
        
        
        //PREC=Accurate
        //checkNanoparticleRelaxation_driver(0.75, 1.0, ROOT + "/Pt-Ni-nanoparticles-CONTCAR-finish/gs-finished-PREC=Normal-CONTCAR-files/", ROOT + "/Pt-Ni-nanoparticles-CONTCAR-finish/gs-finished-PREC=Accurate-CONTCAR-files/", ROOT + "/Pt-Ni-nanoparticles-CONTCAR-finish/", "structList-gs-finished-PREC=Normal.txt");

        
    }
    
    
    
    
    public static ClusterExpansion buildCE() {
        
        PRIM prim = new PRIM(ROOT + "/PRIM-PtNi.vasp", true);
        ClusterExpansion ce = new ClusterExpansion(prim);
        
        System.out.println("\n!!!!!!Here, I reduce cutoof for pair-cluster from 10 A to 7 A.\n");
        
        /*
         * (10, 6, 5, 4, 4, 0) are the parameters for Pt3Ni-Mo Nano Lett paper
         */
        double pairCutoff = 7; 
        double tripleCutoff = 6;
        double quadCutoff = 5;
        double quintCutoff = 4;// include
        double hexCutoff = 4; //include
        double seCuCutoff = 0;
   
        /*
         * 2nd set: (6,6,5,4,4,0)
         */
        
        
        
        /*
         * 3rd set
         */
        /*
        pairCutoff = 6;
        tripleCutoff = 6;
        quadCutoff = 4;
        quintCutoff = 4;
        hexCutoff = 4;
        seCuCutoff = 0;
        */
        
        System.out.println("Pair Cutoff: " + pairCutoff);
        System.out.println("Triple Cutoff: " + tripleCutoff);
        System.out.println("Quad Cutoff: " + quadCutoff);
        System.out.println("Quint Cutoff: " + quintCutoff);
        System.out.println("Hex Cutoff: " + hexCutoff);
        System.out.println("SeCu Cutoff: " + seCuCutoff);
        ce.appendClusters(1, 1);
        ce.appendClusters(2, pairCutoff);
        ce.appendClusters(3, tripleCutoff);
        ce.appendClusters(4, quadCutoff);
        ce.appendClusters(5, quintCutoff);
        ce.appendClusters(6, hexCutoff);
        ce.appendClusters(7, seCuCutoff);
        
        Species gold = Species.get("Au");  // first element in PRIM file
        Species palladium = Species.get("Pd"); // second element in PRIM file
        Species[] speciesInOrder = new Species[] {gold, palladium};
        int numFunctionGroups = 0;
        for (int clustNum = 0; clustNum < ce.numGroups(); clustNum++) {
          ClusterGroup group = ce.getClusterGroup(clustNum);
          POSCAR outfile = new POSCAR(group.getSampleStructure(gold, palladium), speciesInOrder);
          outfile.writeFile(CLUST_DIR + clustNum + ".vasp");
          //outfile.writeVICSOutFile(CLUST_DIR + clustNum + ".out");
          numFunctionGroups += group.numFunctionGroups();
        }
        System.out.println(ce.numGroups() + " cluster groups generated");
        System.out.println(numFunctionGroups + " function groups generated");
        return ce;

      }

      
      
      
    
    /**
     * 
     * buildCE method
     */
    public static ClusterExpansion buildCE_PtNiOH() {
      
      PRIM prim = new PRIM(ROOT + "/PRIM-PtNiS.vasp", true);
      ClusterExpansion ce = new ClusterExpansion(prim);
      
      /*
       * (10, 6, 5, 4, 4, 0) are the parameters for Pt3Ni-Mo Nano Lett paper
       */
      double pairCutoff = 7; 
      double tripleCutoff = 6;
      double quadCutoff = 5;
      double quintCutoff = 4;// include
      double hexCutoff = 4; //include
      double seCuCutoff = 0;
 
      /*
       * 2nd set: (6,6,5,4,4,0)
       */
      /*
      pairCutoff = 6;
      tripleCutoff = 6;
      quadCutoff = 5;
      quintCutoff = 4;
      hexCutoff = 4;
      seCuCutoff = 0;
      */
      
      /*
       * 3rd set: (6,6,4,4,4,0)
       */
      /*
      pairCutoff = 6;
      tripleCutoff = 6;
      quadCutoff = 4;
      quintCutoff = 4;
      hexCutoff = 4;
      seCuCutoff = 0;
      */
      
      
      /*
       * 4th set: (5,4,0,0,0,0)
       */
      /*
      pairCutoff = 5;
      tripleCutoff = 4;
      quadCutoff = 0;
      quintCutoff = 0;
      hexCutoff = 0;
      seCuCutoff = 0;
      */
      
      System.out.println("Pair Cutoff: " + pairCutoff);
      System.out.println("Triple Cutoff: " + tripleCutoff);
      System.out.println("Quad Cutoff: " + quadCutoff);
      System.out.println("Quint Cutoff: " + quintCutoff);
      System.out.println("Hex Cutoff: " + hexCutoff);
      System.out.println("SeCu Cutoff: " + seCuCutoff);
      ce.appendClusters(1, 1);
      ce.appendClusters(2, pairCutoff);
      ce.appendClusters(3, tripleCutoff);
      ce.appendClusters(4, quadCutoff);
      ce.appendClusters(5, quintCutoff);
      ce.appendClusters(6, hexCutoff);
      ce.appendClusters(7, seCuCutoff);
      
      Species gold = Species.get("Au");  // first element in PRIM file
      Species palladium = Species.get("Pd"); // second element in PRIM file
      Species[] speciesInOrder = new Species[] {gold, palladium};
      int numFunctionGroups = 0;
      for (int clustNum = 0; clustNum < ce.numGroups(); clustNum++) {
        ClusterGroup group = ce.getClusterGroup(clustNum);
        POSCAR outfile = new POSCAR(group.getSampleStructure(gold, palladium), speciesInOrder);
        outfile.writeFile(CLUST_DIR_PTNIOH + clustNum + ".vasp");
        //outfile.writeVICSOutFile(CLUST_DIR_PTNIO + clustNum + ".out");
        numFunctionGroups += group.numFunctionGroups();
      }
      System.out.println(ce.numGroups() + " cluster groups generated");
      System.out.println(numFunctionGroups + " function groups generated");
      return ce;

    }

    
    

      
      
      
    
    /**
     * 
     * buildCE method
     */
    public static ClusterExpansion buildCE_PtNiVac() {
      
      PRIM prim = new PRIM(ROOT + "/PRIM-PtNi.vasp", true);
      ClusterExpansion ce = new ClusterExpansion(prim);
      
      /*
       * (10, 6, 5, 4, 4, 0) are the parameters for Pt3Ni-Mo Nano Lett paper
       */
      double pairCutoff = 7; 
      double tripleCutoff = 6;
      double quadCutoff = 5;
      double quintCutoff = 4;// include
      double hexCutoff = 4; //include
      double seCuCutoff = 0;
 
      /*
       * 2nd set: (6,6,5,4,4,0)
       */
      /*
      pairCutoff = 6;
      tripleCutoff = 6;
      quadCutoff = 5;
      quintCutoff = 4;
      hexCutoff = 4;
      seCuCutoff = 0;
      */
      
      /*
       * 3rd set: (6,6,4,4,4,0)
       */
      /*
      pairCutoff = 6;
      tripleCutoff = 6;
      quadCutoff = 4;
      quintCutoff = 4;
      hexCutoff = 4;
      seCuCutoff = 0;
      */
      
      
      /*
       * 4th set: (5,4,0,0,0,0)
       */
      /*
      pairCutoff = 5;
      tripleCutoff = 4;
      quadCutoff = 0;
      quintCutoff = 0;
      hexCutoff = 0;
      seCuCutoff = 0;
      */
      
      System.out.println("Pair Cutoff: " + pairCutoff);
      System.out.println("Triple Cutoff: " + tripleCutoff);
      System.out.println("Quad Cutoff: " + quadCutoff);
      System.out.println("Quint Cutoff: " + quintCutoff);
      System.out.println("Hex Cutoff: " + hexCutoff);
      System.out.println("SeCu Cutoff: " + seCuCutoff);
      ce.appendClusters(1, 1);
      ce.appendClusters(2, pairCutoff);
      ce.appendClusters(3, tripleCutoff);
      ce.appendClusters(4, quadCutoff);
      ce.appendClusters(5, quintCutoff);
      ce.appendClusters(6, hexCutoff);
      ce.appendClusters(7, seCuCutoff);
      
      Species gold = Species.get("Au");  // first element in PRIM file
      Species palladium = Species.get("Pd"); // second element in PRIM file
      Species[] speciesInOrder = new Species[] {gold, palladium};
      int numFunctionGroups = 0;
      for (int clustNum = 0; clustNum < ce.numGroups(); clustNum++) {
        ClusterGroup group = ce.getClusterGroup(clustNum);
        POSCAR outfile = new POSCAR(group.getSampleStructure(gold, palladium), speciesInOrder);
        outfile.writeFile(CLUST_DIR + clustNum + ".vasp");
        //outfile.writeVICSOutFile(CLUST_DIR + clustNum + ".out");
        numFunctionGroups += group.numFunctionGroups();
      }
      System.out.println(ce.numGroups() + " cluster groups generated");
      System.out.println(numFunctionGroups + " function groups generated");
      return ce;

    }

    
    /**
     * 
     * buildCE method
     */
    public static ClusterExpansion buildCE_dummy() {
      
      PRIM prim = new PRIM(ROOT + "/PRIM-PtNi.vasp", true);
      ClusterExpansion ce = new ClusterExpansion(prim);
      
      double pairCutoff = 4; 
      double tripleCutoff = 4;
      
      ce.appendClusters(1, 1);
      ce.appendClusters(2, pairCutoff);
      //ce.appendClusters(3, tripleCutoff);
      
      return ce;

    }
    
    
    /**
     * addKnownStructures() method
     * the txt file contains free energies, while, the formation energies are calculated and added to CE
     * @param ce
     * @return
     */
    public static ClusterExpansion addKnownStructures_PtNiOH_CalculateOHCoverage(ClusterExpansion ce, double cutoffOHML) {
      
      //StructureListFile enerIn = new StructureListFile(ROOT + "energyList-trainingSet-PtNiVac.txt"); //chosen
      StructureListFile enerIn = new StructureListFile(ROOT + "energyList-trainingSet-PtNiSVac.txt"); //chosen

  
      /*
       * different x,y,z supercels, use different appliedCE object.   
       */
      
      /*
       * may have problem when bulk struct added to training set. 
       */
      int[][] superToDirect = new int[][] {
          {-8, 8, 8},
          {8, -8, 8},
          {8, 8, -8}
      };

      FastAppliedCE appliedCE = new FastAppliedCE(ce, superToDirect);
      
      int num_struct_withOH = 0;
      int num_struct_withSingleOH = 0;
      
      int num_struct_nonSurfaceOH =0;
      
      double max_OHCoverage = Double.NEGATIVE_INFINITY;
      double min_OHCoverage = Double.POSITIVE_INFINITY;
      
      int numStructLess85 = 0;
      int numStructLess100 = 0;
      
      int OHML1stBin = 0;
      int OHML2ndBin = 0;
      int OHML3rdBin = 0;
      int OHML4thBin = 0;
      int OHML5thBin = 0;
      int OHML6thBin = 0;
      int OHML7thBin = 0;
      int OHML8thBin = 0;
      int OHML9thBin = 0;
      int OHML10thBin = 0;
      int OHMLAllBin = 0;

      
      try{
          
          FileWriter fw = new FileWriter(ROOT + "/_added-energyList-PtNi-OH.txt", false);
          BufferedWriter bw = new BufferedWriter(fw); 
          
          bw.write("structIndex structName numPt numNi numOH energy FE OHcoverage" + "\r\n");
          bw.flush();
      
          //TODO
          for (int entryNum = 0; entryNum < enerIn.numEntries(); entryNum++) {
              String fileName = enerIn.getFileName(entryNum);
              //POSCAR infile = new POSCAR(STRUCT_DIR + fileName, true);
              POSCAR infile = new POSCAR(ROOT + "/structures-trainingSet-n=352/" + fileName, true);

              //System.out.println(fileName);

              Structure structure = new Structure(infile);

              int numPt = structure.numDefiningSitesWithElement(Element.platinum);
              int numNi = structure.numDefiningSitesWithElement(Element.nickel);
              
              int numP = structure.numDefiningSitesWithElement(Element.phosphorus);
              int numS = structure.numDefiningSitesWithElement(Element.sulfur);

              //int totalAtoms = numPt + numNi + numMo + numCu;
              int totalAtoms = numPt + numNi + numP + numS;
              
              if(numP!=0){
                  //System.out.println("the structure contains Ni-*OH: " + fileName);
                  continue;
              }
              
              
              if(numS!=0){
                  //System.out.println("the structure contains Ni-*OH: " + fileName);
                  //continue; //exlude NPs with *OH
              }
              
              //if (((totalAtoms) > 50) && ((totalAtoms) < 85)) {continue;}
              if (((totalAtoms) > 50) && ((totalAtoms) < 100)) {continue;}
              
              //if (((totalAtoms) > 1) && ((totalAtoms) < 100)) {continue;}
              
              
              //TODO
              /*
               * calculate the *OH coverage for each training structure
               */
              int numSurfaceS = 0; //only CN<=9, considered as surface site
              int numSurfaceSites = 0;
              for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
                  Structure.Site site = structure.getDefiningSite(siteNum);
                  Structure.Site[] neighbors = structure.getNearbySites(site.getCoords(), 3, false);
                              
                  //if((neighbors.length < 12) && (site.getSpecies()==Species.nickel)){
                  if(neighbors.length <= 9){
                      numSurfaceSites++;
                  }
                  if((neighbors.length <= 9) && (site.getSpecies()==Species.sulfur)){
                      numSurfaceS++;
                  }
              }
              
              double SCoverage_temp = ((double)numSurfaceS) / numSurfaceSites;

              if(SCoverage_temp > cutoffOHML) {
                  continue;
              }
              
              if(max_OHCoverage < SCoverage_temp) {
                  max_OHCoverage = SCoverage_temp; 
              }
              if(min_OHCoverage > SCoverage_temp) {
                  min_OHCoverage = SCoverage_temp; 
              }
              
              
              if((SCoverage_temp>0.0)&&(SCoverage_temp<=0.1)) {
                  OHML1stBin++;
                  OHMLAllBin++;
              }
              if((SCoverage_temp>0.1)&&(SCoverage_temp<=0.2)) {
                  OHML2ndBin++;
                  OHMLAllBin++;
              }
              if((SCoverage_temp>0.2)&&(SCoverage_temp<=0.3)) {
                  OHML3rdBin++;
                  OHMLAllBin++;
              }
              if((SCoverage_temp>0.3)&&(SCoverage_temp<=0.4)) {
                  OHML4thBin++;
                  OHMLAllBin++;
              }
              if((SCoverage_temp>0.4)&&(SCoverage_temp<=0.5)) {
                  OHML5thBin++;
                  OHMLAllBin++;
              }
              if((SCoverage_temp>0.5)&&(SCoverage_temp<=0.6)) {
                  OHML6thBin++;
                  OHMLAllBin++;
              }
              if((SCoverage_temp>0.6)&&(SCoverage_temp<=0.7)) {
                  OHML7thBin++;
                  OHMLAllBin++;
              }
              if((SCoverage_temp>0.7)&&(max_OHCoverage<=0.8)) {
                  OHML8thBin++;
                  OHMLAllBin++;
              }
              if((SCoverage_temp>0.8)&&(SCoverage_temp<=0.9)) {
                  OHML9thBin++;
                  OHMLAllBin++;
              }
              if((SCoverage_temp>0.9)&&(SCoverage_temp<=1.0)) {
                  OHML10thBin++;
                  OHMLAllBin++;
              }
              
              
              
              if(numSurfaceS != numS){
                  num_struct_nonSurfaceOH++;
                  System.out.println("struct=" + fileName + ", has surface Pt-OH sites with CN>9");
                  continue;
              }
              
              if(numS != 0){
                  num_struct_withOH++;
              }
              
              //exclude NPs with only one *OH
              if(numS == 1){
                  num_struct_withSingleOH++;
                  continue;
              }

              double delta = Math.abs(structure.getDefiningVolume() - appliedCE.getSuperStructure().getDefiningVolume());
              
              //the supercell of this structure is the same as the implicit one in the appliedCE
              if (delta < 0.1) {
                  //appliedNanoparticleCE.decorateFromStructure(structure);
                  //appliedCE = appliedNanoparticleCE;
                  
                  appliedCE.decorateFromStructure(structure);

              } 
              //the supercell of this structure is different from the implicit one in the appliedCE
              else {
                  System.out.println("!!! Attention: " + fileName + " has different shape of super cell.");
                  appliedCE = new FastAppliedCE(ce, ce.getBaseStructure().getSuperStructure(infile));
              }
              
              //SuperStructure superStructure = appliedCE.getSuperStructure();
              
              
              //appliedCE.decorateFromStructure(new Structure(infile));
              appliedCE.getSuperStructure().setDescription(fileName);
              
              
              double value = enerIn.getEnergy(entryNum); 
              
              if(value==Double.NaN){
                  continue;
              }

              
              // formation energy: formation E = E(PtxNiy)-xE(Pt)-yE(Ni)
              value -= numPt * PtEnergyBulkAccurate; // From bulk calCulations
              value -= numNi * NiEnergyBulkAccurate; // From bulk calCulations
              
              //value -= numCu * CuEnergyBulkAccurate; // From bulk calCulations
              //value -= numMo * MoEnergyBulkAccurate; // From bulk calCulations

              //TODO
              // Sulfur stands for Pt-OH: "value += numS * OHEnergyCorrect;" used to correct the *OH from the group of "Pt-OH", which is referenced to 0.5*E(H2)-E(H2O)
              //                          "value -= numS * PtEnergyBulk;"    used to account for the Pt from the group of "Pt-OH", , which is referenced to E(Pt-bulk)
              value -= numS * PtEnergyBulkAccurate; // From bulk calCulations
              value -= numP * NiEnergyBulkAccurate; // From bulk calCulations
              
              //TODO
              //correction to *OH term, referenced to 0.5*E(H2)-E(H2O)
              value += numP * OHEnergyCorrectAccurate;
              value += numS * OHEnergyCorrectAccurate;

              
              value /= appliedCE.numPrimCells();
              
              //Status.detail(fileName + "      " + enerIn.getEnergy(entryNum));

              Status.detail("Adding structure " + (entryNum + 1) + "/" + enerIn.numEntries() + ", totalAtoms=" + totalAtoms + ", " + fileName + ". numPt: " +  numPt +", numNi: " + numNi + ", numS: " + numS + ", value / site: " + value + ", appliedCE.numPrimCells(): " + appliedCE.numPrimCells()+ ", OHCoverage: " + SCoverage_temp);
              //Status.detail("Adding structure " + (entryNum + 1) + "/" + enerIn.numEntries() + ", " + fileName + ". numPt: " +  numPt +", numNi: " + numNi + ", numMo: " + numMo + ", value: " + value);
              
              if(totalAtoms < 85) {
                  System.out.println("added structure with totalAtoms<85: " + fileName + ", totalAtoms=" + totalAtoms);
                  numStructLess85++;
              }
              
              if(totalAtoms < 100) {
                  //System.out.println("added structure with totalAtoms<85: " + fileName + ", totalAtoms=" + totalAtoms);
                  numStructLess100++;
              }
              
              bw.write((entryNum + 1) + "   " + fileName + "   " + numPt + "   " + numNi + "   " + num_struct_withOH + "   " + enerIn.getEnergy(entryNum) + "   " + value + "   " + SCoverage_temp + "\r\n");
              bw.flush();
              
              appliedCE.activateAllGroups();
              ce.getStructureList().addCorrelations(appliedCE, value);
          }
          
          bw.flush();
          bw.close();
          fw.close();

      }
      
      catch (IOException e) {
      e.printStackTrace();
      }

      
      Status.basic("Added " + ce.getStructureList().numKnownStructures() + " structures.\n\n");

      Status.basic("The # structures with non-surface Pt-OH (CN>9) on the surface is: " + num_struct_nonSurfaceOH + "\n\n");

      Status.basic("The # structures with Pt-OH and Pt-SingleOH on the surface is: " + num_struct_withOH + ", " + num_struct_withSingleOH + "\n\n");

      Status.basic("The, cutoff, max and min OHCoverage for added training set: cutoff=" + cutoffOHML + ", max=" + max_OHCoverage + ", min=" + min_OHCoverage + "\n\n");

      System.out.println("# of structs with atoms < 85: " +  numStructLess85);
      System.out.println("# of structs with atoms < 100: " +  numStructLess100);

      System.out.println("\n\n******************************************************");
      System.out.println("the analysis of *OH coverages:");

      System.out.println("# of structs with *OH coverage < 0.1 ML: " +  OHML1stBin);
      System.out.println("# of structs with *OH coverage < 0.2 ML: " +  OHML2ndBin);
      System.out.println("# of structs with *OH coverage < 0.3 ML: " +  OHML3rdBin);
      System.out.println("# of structs with *OH coverage < 0.4 ML: " +  OHML4thBin);
      System.out.println("# of structs with *OH coverage < 0.5 ML: " +  OHML5thBin);
      System.out.println("# of structs with *OH coverage < 0.6 ML: " +  OHML6thBin);
      System.out.println("# of structs with *OH coverage < 0.7 ML: " +  OHML7thBin);
      System.out.println("# of structs with *OH coverage < 0.8 ML: " +  OHML8thBin);
      System.out.println("# of structs with *OH coverage < 0.9 ML: " +  OHML9thBin);
      System.out.println("# of structs with *OH coverage < 1.0 ML: " +  OHML10thBin);
      System.out.println("\n# of structs with *OH coverage > 0 ML: " +  OHMLAllBin);
      System.out.println("******************************************************\n\n");
      
      return ce;
    }
    


    
    
    /**
     * addKnownStructures() method
     * the txt file contains free energies, while, the formation energies are calculated and added to CE
     * @param ce
     * @return
     */
    public static ClusterExpansion addKnownStructures_PtNiVac_cleanNP(ClusterExpansion ce) {
      
      StructureListFile enerIn = new StructureListFile(ROOT + "energyList-trainingSet-PtNiVac.txt"); //chosen
      //StructureListFile enerIn = new StructureListFile(ROOT + "energyList-trainingSet-PtNiSVac.txt"); //chosen

  
      /*
       * different x,y,z supercels, use different appliedCE object.   
       */
      
      /*
       * may have problem when bulk struct added to training set. 
       */
      int[][] superToDirect = new int[][] {
          {-8, 8, 8},
          {8, -8, 8},
          {8, 8, -8}
      };

      FastAppliedCE appliedCE = new FastAppliedCE(ce, superToDirect);

      
      try{
          
          FileWriter fw = new FileWriter(ROOT + "/_added-energyList-PtNi-cleanNP.txt", false);
          BufferedWriter bw = new BufferedWriter(fw); 
          
          bw.write("structIndex structName numPt numNi numOH energy FE" + "\r\n");
          bw.flush();
      
          //TODO
          for (int entryNum = 0; entryNum < enerIn.numEntries(); entryNum++) {
              String fileName = enerIn.getFileName(entryNum);
              //POSCAR infile = new POSCAR(STRUCT_DIR + fileName, true);
              POSCAR infile = new POSCAR(ROOT + "/structures-trainingSet-n=352/" + fileName, true);

              //System.out.println(fileName);

              Structure structure = new Structure(infile);

              int numPt = structure.numDefiningSitesWithElement(Element.platinum);
              int numNi = structure.numDefiningSitesWithElement(Element.nickel);
              
              int numP = structure.numDefiningSitesWithElement(Element.phosphorus);
              int numS = structure.numDefiningSitesWithElement(Element.sulfur);

              //int totalAtoms = numPt + numNi + numMo + numCu;
              int totalAtoms = numPt + numNi + numP + numS;

              
              if(numP!=0){
                  //System.out.println("the structure contains Ni-*OH: " + fileName);
                  continue;
              }
              
              
              if(numS!=0){
                  //System.out.println("the structure contains Ni-*OH: " + fileName);
                  continue; //exlude NPs with *OH
              }
              
              //if (((totalAtoms) > 50) && ((totalAtoms) < 85)) {continue;}
              if (((totalAtoms) > 50) && ((totalAtoms) < 100)) {continue;}
              //if (((totalAtoms) > 1) && ((totalAtoms) < 100)) {continue;}
              

              double delta = Math.abs(structure.getDefiningVolume() - appliedCE.getSuperStructure().getDefiningVolume());
              
              //the supercell of this structure is the same as the implicit one in the appliedCE
              if (delta < 0.1) {
                  //appliedNanoparticleCE.decorateFromStructure(structure);
                  //appliedCE = appliedNanoparticleCE;
                  
                  appliedCE.decorateFromStructure(structure);

              } 
              //the supercell of this structure is different from the implicit one in the appliedCE
              else {
                  System.out.println("!!! Attention: " + fileName + " has different shape of super cell.");
                  appliedCE = new FastAppliedCE(ce, ce.getBaseStructure().getSuperStructure(infile));
              }
              
              //SuperStructure superStructure = appliedCE.getSuperStructure();
              
              
              //appliedCE.decorateFromStructure(new Structure(infile));
              appliedCE.getSuperStructure().setDescription(fileName);
              
              
              double value = enerIn.getEnergy(entryNum); 
              
              if(value==Double.NaN){
                  continue;
              }

              
              // formation energy: formation E = E(PtxNiy)-xE(Pt)-yE(Ni)
              value -= numPt * PtEnergyBulkAccurate; // From bulk calCulations
              value -= numNi * NiEnergyBulkAccurate; // From bulk calCulations             

              //TODO
              // Sulfur stands for Pt-OH: "value += numS * OHEnergyCorrect;" used to correct the *OH from the group of "Pt-OH", which is referenced to 0.5*E(H2)-E(H2O)
              //                          "value -= numS * PtEnergyBulk;"    used to account for the Pt from the group of "Pt-OH", , which is referenced to E(Pt-bulk)
              value -= numS * PtEnergyBulkAccurate; // From bulk calCulations
              value -= numP * NiEnergyBulkAccurate; // From bulk calCulations
              
              //TODO
              //correction to *OH term, referenced to 0.5*E(H2)-E(H2O)
              value += numP * OHEnergyCorrectAccurate;
              value += numS * OHEnergyCorrectAccurate;

              
              value /= appliedCE.numPrimCells();
              
              //Status.detail(fileName + "      " + enerIn.getEnergy(entryNum));

              Status.detail("Adding structure " + (entryNum + 1) + "/" + enerIn.numEntries() + ", totalAtoms=" + totalAtoms + ", " + fileName + ". numPt: " +  numPt +", numNi: " + numNi + ", numS: " + numS + ", value / site: " + value + ", appliedCE.numPrimCells(): " + appliedCE.numPrimCells());
              //Status.detail("Adding structure " + (entryNum + 1) + "/" + enerIn.numEntries() + ", " + fileName + ". numPt: " +  numPt +", numNi: " + numNi + ", numMo: " + numMo + ", value: " + value);
              
              bw.write((entryNum + 1) + "   " + fileName + "   " + numPt + "   " + numNi + "   " + enerIn.getEnergy(entryNum) + "   " + value + "\r\n");
              bw.flush();
              
              appliedCE.activateAllGroups();
              ce.getStructureList().addCorrelations(appliedCE, value);
          }
          
          bw.flush();
          bw.close();
          fw.close();

      }
      
      catch (IOException e) {
      e.printStackTrace();
      }

      
      Status.basic("Added " + ce.getStructureList().numKnownStructures() + " structures.\n\n");
      
      return ce;
    }
    



    public static ClusterExpansion fitCE(ClusterExpansion ce) {
        
        /**
         * Build the cluster expansion and generate NN clusters with up to 4 sites.
         */ 
        //ClusterExpansion ce = buildCE();

        /**
         * Read in correlations from a file called "energies.txt"  
         * This should just be a list of fileNames followed by energies (per unit cell)
         * Each fileName should correspond to a POSCAR in the "nanowire/2nm/ce/poscars" folder
         */    
        // Order the species in this array the same way they are ordered in the POSCAR files.
        //addKnownStructures(ce);
        //addKnownStructuresWithoutMo(ce);
        //addKnownStructuresRuleOutStructWithHighFormationE(ce, FECutoff);
        
        
        ClusterGroup[] activeGroups = ce.getAllGroups(true);
        
        System.out.println(activeGroups.length + " cluster groups.");
        
        //ILinearCERegGenerator generator = new ExpExpRegGenerator(activeGroups);
        
        //ILinearCERegGenerator generator = new ExpPowRegGenerator(activeGroups);
        
        ILinearCERegGenerator generator = new PowExpRegGenerator(activeGroups);
        //ILinearCERegGenerator generator = new PowPowRegGenerator(activeGroups);
        generator.setMultiplicityExponent(0);

        double[] initState = new double[generator.numParameters()];
        Arrays.fill(initState, 1);
        //initState = new double[] {0.8823124822899626, 0.16174455365217855, 0.11868777414268145, 0.0922548990460885};
        //initState = new double[] {0.6580633972197518, 0.14685427063104411, 0.11907807350546687, 0.1319838515075602};
        //initState = new double[] {1E-8, 1E-8, 10, 10};
        
        //20191121
        initState = new double[] {1.0000014835039548E-8, 9.41387009584011E-9, 4.286326852575267, 2.9862877358694506};
        
        //20200122, 20200123
        // the set for trainingSet including Pt(111) slabs
        //initState = new double[] {1.0138695063838821E-8, 9.417254064670763E-9, 0.6590492647968678, 6.077005051205868};

        //CE: 20200124_n=354
        // the set for trainingSet including Pt(111) slabs
        //initState = new double[] {9.978191734636064E-9, 9.407220917090453E-9, 4.084263541427147, 2.974329932793443};

        //CE: 20200124_n=363
        // trainingSet excluding Pt(111) slabs
        //initState = new double[] {1.0138695063838821E-8, 9.417254064670763E-9, 0.6590492647968678, 6.077005051205868};

        //CE: 20200213_n=383
        // trainingSet excluding Pt(111) slabs
        //initState = new double[] {1.0138496562698203E-8, 9.318735467371137E-9, 0.6516398871070076, 5.036628490864908};
        //CE: 20200213_n=425-singleOH
        // trainingSet excluding Pt(111) slabs
        //initState = new double[] {1.0138690077370717E-8, 9.317896093307552E-9, 0.6512046578222509, 5.033281982901187};
        
        
        System.out.println();
        System.out.println("Initial Parameters: ");
        for (int paramNum = 0; paramNum < initState.length; paramNum++) {
          System.out.println(initState[paramNum] + ", ");
        }
        System.out.println();
        System.out.println();
        
        double[] values = ce.getStructureList().getValues(true);
        double[][] correlations = ce.getStructureList().getMultCorrelations(activeGroups, true);
        double[] regularizer = generator.getRegularizer(initState, null);
        
        L2FullRLSFitter fitter = new L2FullRLSFitter(correlations, values);
        fitter = (L2FullRLSFitter) fitter.setRegularizer(generator, initState);
            
        System.out.println("************ Before oCuimizing score *********");
        System.out.println("GCV: " + fitter.getGeneralizedCVScore() + ", ");
        System.out.println("LOO CV: " + fitter.getLKOCVScore(1) + ", ");
        System.out.println();
        
        fitter.useLKOCVScore(1);
        //fitter.useGCVScore();
        //fitter.useUnweightedLOOCVScore();
        fitter.setNoSingularGuarantee(false);
        fitter.allowIncrementalUpdates(false);
        
        RegularizerSelector selector = new RegularizerSelector(fitter, generator, initState);
        //selector.setMaxAllowedGrad(5E-3);
        //selector.setMaxAllowedGrad(1E-5);
        selector.setMaxAllowedGrad(1E-8);
        selector.findMinPolakRibiere();
        fitter = (L2FullRLSFitter) selector.getFitter();
        
        System.out.println("************ After oCuimizing score *********");
        System.out.println("Score: " + fitter.scoreFit() + ", ");
        System.out.println("GCV: " + fitter.getGeneralizedCVScore() + ", ");
        System.out.println("LOO CV: " + fitter.getLKOCVScore(1) + ", ");
        System.out.println("Unweighted LOO CV: " + fitter.getUnweightedLOOCVScore() + ", ");
        System.out.println("RMS Error: " + fitter.getRMSError() + ", ");
        System.out.println();
        double[] finalState = selector.getBoundedParameters(null);
        System.out.println("Final Parameters: ");
        for (int paramNum = 0; paramNum < finalState.length; paramNum++) {
          System.out.println(finalState[paramNum] + ", ");
        }
        System.out.println();
        double[] eci = fitter.getCoefficients();
        System.out.println("\nECI: ");
        for (int varNum = 0; varNum < eci.length; varNum++) {
          System.out.print(eci[varNum] + ", ");
        }
        System.out.println();
        System.out.println();

        ce.setECI(eci, activeGroups, false);
        
        int numFunctionGroups = 0;
        for (int clustNum = 0; clustNum < ce.numGroups(); clustNum++) {
          ClusterGroup group = ce.getClusterGroup(clustNum);
          for (int functNum = 0; functNum < group.numFunctionGroups(); functNum++) {
            String description = "Group number " + clustNum + ", Funct group number: " + functNum + ", Sites per cluster: " + group.numSitesPerCluster() + ", Multiplicity: " + group.getMultiplicity() + ", Max distance: " + group.getMaxDistance() + ", ECI: " + group.getECI(functNum) + ", Correlation coefficient: " + group.getCorrelationCoefficient(functNum);        
            Status.detail(description);
            numFunctionGroups++;
          }
        }
        Status.detail("Num total function groups: " + numFunctionGroups);
        
        Status.detail("");
        Status.detail("Structure information: ");

        double[] cvErrors = fitter.getCVErrors(false);
        StructureList list = ce.getStructureList();
        Status.detail("the # of structures: " + list.numKnownStructures());
        double totalSqCVErrorPerAtom = 0;
        double totalAbsCVErrorPerAtom = 0;
        int numNonEmpty = 0;
        for (int structNum = 0; structNum < list.numKnownStructures(); structNum++) {
          ClustStructureData structData = list.getKnownStructureData(structNum);
          double value = structData.getValue();
          double predValue = list.predictValue(activeGroups, structNum);
          SuperStructure structure = structData.newSuperStructure(ce);
          double ratio = (double) structure.numDefiningSites() / (structure.numDefiningSites() - structure.numDefiningSitesWithSpecies(Species.vacancy));
          double cvErrorPerAtom = cvErrors[structNum] * ratio;
          if (!Double.isInfinite(ratio)) {
            totalSqCVErrorPerAtom += cvErrorPerAtom * cvErrorPerAtom;
            totalAbsCVErrorPerAtom += Math.abs(cvErrorPerAtom);
            numNonEmpty++;
          }
          double weight = fitter.getWeight(structNum);
          Status.detail(structData.getDescription() + ", Known Value: " + value + ", Predicted value: " + predValue + ", CV value: " + (value - cvErrors[structNum]) + ", CV Error: " + cvErrors[structNum] + ", CV Error per atom: " + cvErrorPerAtom + ", Weight: " + weight);
        }
        
        
        System.out.println("\n\n\nlist out structures with absolute value of CV Error per atom larger than 0.005 eV.\n");
        int numStructLargeError = 0;
        for (int structNum = 0; structNum < list.numKnownStructures(); structNum++) {
            ClustStructureData structData = list.getKnownStructureData(structNum);
            double value = structData.getValue();
            double predValue = list.predictValue(activeGroups, structNum);
            SuperStructure structure = structData.newSuperStructure(ce);
            double ratio = (double) structure.numDefiningSites() / (structure.numDefiningSites() - structure.numDefiningSitesWithSpecies(Species.vacancy));
            double cvErrorPerAtom = cvErrors[structNum] * ratio;
            if (!Double.isInfinite(ratio)) {
              //totalSqCVErrorPerAtom += cvErrorPerAtom * cvErrorPerAtom;
              //totalAbsCVErrorPerAtom += Math.abs(cvErrorPerAtom);
              //numNonEmpty++;
            }
            double weight = fitter.getWeight(structNum);
            if(Math.abs(cvErrorPerAtom) >= 0.005){
                numStructLargeError++;
                Status.detail(structData.getDescription() + ", Known Value: " + value + ", Predicted value: " + predValue + ", CV value: " + (value - cvErrors[structNum]) + ", CV Error: " + cvErrors[structNum] + ", CV Error per atom: " + cvErrorPerAtom + ", Weight: " + weight);
            }
        }
        
        System.out.println("\nThe # of structures with CV Error per atom larger than 0.005 eV is: " + numStructLargeError );
        
        
        System.out.println("\n\n\nlist out structures with absolute value of CV Error per atom larger than 0.010 eV.\n");
        int numStructLargeError_10 = 0;
        for (int structNum = 0; structNum < list.numKnownStructures(); structNum++) {
            ClustStructureData structData = list.getKnownStructureData(structNum);
            double value = structData.getValue();
            double predValue = list.predictValue(activeGroups, structNum);
            SuperStructure structure = structData.newSuperStructure(ce);
            double ratio = (double) structure.numDefiningSites() / (structure.numDefiningSites() - structure.numDefiningSitesWithSpecies(Species.vacancy));
            double cvErrorPerAtom = cvErrors[structNum] * ratio;
            if (!Double.isInfinite(ratio)) {
              //totalSqCVErrorPerAtom += cvErrorPerAtom * cvErrorPerAtom;
              //totalAbsCVErrorPerAtom += Math.abs(cvErrorPerAtom);
              //numNonEmpty++;
            }
            double weight = fitter.getWeight(structNum);
            if(Math.abs(cvErrorPerAtom) >= 0.010){
                numStructLargeError_10++;
                Status.detail(structData.getDescription() + ", Known Value: " + value + ", Predicted value: " + predValue + ", CV value: " + (value - cvErrors[structNum]) + ", CV Error: " + cvErrors[structNum] + ", CV Error per atom: " + cvErrorPerAtom + ", Weight: " + weight);
            }
        }
        
        System.out.println("\nThe # of structures with CV Error per atom larger than 0.010 eV is: " + numStructLargeError_10);
        
        
        System.out.println("\n\n\nlist out structures with absolute value of CV Error per atom larger than 0.020 eV.\n");
        int numStructLargeError_20 = 0;
        for (int structNum = 0; structNum < list.numKnownStructures(); structNum++) {
            ClustStructureData structData = list.getKnownStructureData(structNum);
            double value = structData.getValue();
            double predValue = list.predictValue(activeGroups, structNum);
            SuperStructure structure = structData.newSuperStructure(ce);
            double ratio = (double) structure.numDefiningSites() / (structure.numDefiningSites() - structure.numDefiningSitesWithSpecies(Species.vacancy));
            double cvErrorPerAtom = cvErrors[structNum] * ratio;
            if (!Double.isInfinite(ratio)) {
              //totalSqCVErrorPerAtom += cvErrorPerAtom * cvErrorPerAtom;
              //totalAbsCVErrorPerAtom += Math.abs(cvErrorPerAtom);
              //numNonEmpty++;
            }
            double weight = fitter.getWeight(structNum);
            if(Math.abs(cvErrorPerAtom) >= 0.020){
                numStructLargeError_20++;
                Status.detail(structData.getDescription() + ", Known Value: " + value + ", Predicted value: " + predValue + ", CV value: " + (value - cvErrors[structNum]) + ", CV Error: " + cvErrors[structNum] + ", CV Error per atom: " + cvErrorPerAtom + ", Weight: " + weight);
            }
        }
        
        System.out.println("\nThe # of structures with CV Error per atom larger than 0.020 eV is: " + numStructLargeError_20);
        
        
        System.out.println("\n\n");
        Status.basic("RMS CV score per atom: " + Math.sqrt(totalSqCVErrorPerAtom / numNonEmpty));
        Status.basic("Mean Abs CV score per atom: " + totalAbsCVErrorPerAtom / numNonEmpty);
        Status.basic("LOOCV score: " + fitter.getLOOCVScore());
        Status.basic("Final score: " + fitter.scoreFit());
        Status.basic("RMS Error: " + fitter.getRMSError());
        
        Status.basic("");
        
        return ce;
      }
       

    
    /**
     * 
     * using the struct-list from 2020-03-03-n=354
     *   
     *   add Pt29Ni7
     *   
     * extend the Pt-Ni(111) slabs with numPrims=144
     * 
     * @return
     */
    public static ClusterExpansion getPreFittedCE20201101_PRIM_PtNiOH_PtNi111_355_maxOH056ML(){

        ClusterExpansion ce = buildCE_PtNiOH();
        ClusterGroup[] activeGroups = ce.getAllGroups(true);

        
        double[] eci = new double[]{
                0.45957597667761546, 0.1918274610367381, 0.4171663190223076, 0.16229562735764608, -5.531794269776277E-6, 1.6625370038819004E-4, -4.225460612740195E-4, -8.803664857229762E-4, 3.132542867470469E-4, 6.613657463941814E-4, -8.397137809624351E-5, 3.5117931330691E-4, -2.719322827177935E-4, -6.557553812604808E-4, 1.2163107107602059E-4, 7.735190573299865E-4, -6.127731055756824E-4, 6.701734521223825E-4, 8.527000812403821E-4, -5.823638736148799E-4, -2.6191656021705423E-4, -3.549076585248694E-4, -4.267564584057998E-4, 0.0011772342736624131, -5.551440287448091E-4, -6.493817024313103E-4, 0.0018613441274230448, -9.31743483063229E-4, 0.0010659468621937382, 8.279201905116978E-4, 0.001374481254357218, -0.0017852246984532668, 8.08610553312617E-4, -2.1155925082312795E-4, 0.001550946697057189, -0.0012044775183846605, 8.490406571130989E-4, -0.0011366702551435463, -2.1826772094592044E-4, -2.3955519822080676E-4, 0.005202664256901034, 0.00863584130734567, 0.004335744448083519, -0.010511947374133149, 0.005297134806210593, 0.005170219513380997, 7.332150503994367E-5, -1.1999127566383308E-4, -5.868440861115225E-5, 5.7575223924586695E-6, -2.924963583091696E-5, 9.258723685367154E-5, 4.162643584408978E-5, -1.552062580858259E-4, 2.3776401343185302E-4, 1.2766277299511904E-4, -3.676189940853285E-5, 2.271586748533918E-5, -8.649126267118619E-5, -7.436781174998959E-5, -1.784277211677298E-5, -1.3310926977044683E-4, 2.692737934226776E-4, -4.1250681212569307E-4, 1.0296283394473581E-4, -4.4768925494904064E-5, 2.526574241526892E-5, -1.6378015139166476E-4, 7.442422597519875E-5, 6.494070504666134E-6, -9.342383249894062E-5, 8.719596631226673E-5, 8.569827159855738E-6, 6.965064593708634E-5, -2.8665489007638782E-5, 7.151857086929879E-5, 1.758657494101319E-5, -2.1816006234160346E-5, -2.580789925500694E-4, 5.255539267463275E-5, -9.493727440377164E-5, 1.6448086232457811E-4, -2.5004685226045528E-5, -4.102866620962146E-5, -4.0627990362404826E-5, -1.4595966026623728E-4, 7.280488558358768E-5, 1.4443014622937504E-4, -1.28175875185372E-4, 1.2605010942378432E-4, 1.4250896687788197E-5, 1.2110626373085985E-5, -3.9162991852589944E-5, -6.870057087609801E-5, 3.224732117485278E-5, 2.7617860370767605E-4, -4.0339074170242506E-5, 1.527778981253744E-4, -2.301268323072845E-4, 5.360936839857988E-5, -1.3367740521707763E-4, 1.661529902664933E-4, -2.4008472876251453E-5, 2.0674626220444593E-4, -3.2755455185945817E-4, -2.2569833429094874E-4, -8.782545309957005E-5, -9.26402419623338E-7, 1.1366520723266396E-4, -1.5813176313534023E-5, -3.9378062504960966E-5, 4.8906331894890296E-5, -6.546587018232345E-5, 1.293336392844532E-4, -2.2714166747574366E-5, 4.302920102168691E-5, -7.129912208241659E-5, 1.0220451374758554E-5, -6.830068287278228E-5, -6.167339922461337E-6, -1.3296527518561342E-4, 1.1887914390684685E-4, 3.140047925130087E-5, 7.250479594163176E-5, -1.0200597396316019E-4, 7.459155133863094E-5, -4.8722184234560074E-5, -3.5032095607665176E-5, -2.01877578670947E-6, 1.853126918133041E-4, -1.1664977823809433E-5, -1.029979606385177E-4, -2.4900576425309837E-5, -1.457143338591644E-4, 2.154934038136562E-4, -1.5625198083659645E-4, 7.906471578712207E-5, -1.6065037187847943E-4, 1.0224841670852434E-4, -3.0290285440335955E-4, 8.045369126703414E-5, -4.632394751724546E-6, 1.2394806270804042E-4, 8.194674118530788E-5, -1.3368168882073637E-4, -1.9761410149143956E-4, 3.917449939159854E-5, -1.5354161666693593E-4, 1.720771517058171E-4, 1.0254765854646013E-4, -2.630542167724793E-5, -1.9631984522533149E-4, 6.351643444523804E-5, -9.452993510785746E-5, -4.9928195890947425E-5, 2.9352070221660585E-4, -9.506215924859991E-5, -4.028582374339005E-5, -1.7457600904682862E-4, -4.452435008626813E-5, 1.7831053821504707E-4, -3.8206436640378236E-4, 3.3814120042947784E-4, -2.0688158199720034E-4, 5.9109473806470084E-5, -1.0770492966404163E-4, -1.746311289316747E-4, 1.5089455428750807E-5, 8.115221693211759E-5, -1.1930220664642106E-5, -2.2149350779574476E-5, 6.145158459051615E-5, 2.3466735195663987E-5, 2.6722739725557737E-5, -7.119040764420447E-5, 4.824939950235632E-5, 1.9926938584525968E-4, 3.134437837863397E-5, -3.690135083493141E-5, -7.897142328724743E-5, -6.3292202287131E-5, -9.268822030671482E-5, -1.0437173815433173E-4, 2.279431260994589E-4, -3.860733547057178E-5, 1.3302283278924208E-4, -3.377861075885677E-4, -2.0045714098478355E-4, 1.6683284966475756E-4, -4.3027312929776445E-5, -2.7157313744101917E-5, 2.1793996289571458E-5, 2.797163627152215E-5, -1.729208867541824E-5, 8.834346324587772E-5, -5.39348365374533E-5, -3.9325999115085363E-5, 6.580677860183769E-5, -6.716708376673436E-5, 3.7812427514234215E-5, -3.1668214867216734E-5, -1.564888686344829E-5, -1.6474931249968732E-5, -1.6244551493036028E-5, 8.657249550158178E-5, 6.654853421059767E-7, 4.448372948089247E-5, 5.007914896917789E-6, -1.673122578418802E-4, -2.702827725034211E-5, -6.2371966412631E-5, 1.2147127597987849E-4, -8.849901168672431E-5, 1.250436549196916E-4, -1.3281216743146128E-4, 8.801026889431222E-5, 4.386910693752978E-5, 1.2927013252670956E-4, -3.977679494865296E-5, 2.068776902078265E-5, 2.8218268819688562E-5, 7.146235892838062E-5, -8.343492118137105E-6, -2.4436932588347334E-5, 4.613962664712758E-5, -1.6975150673031797E-5, -1.3912680301708625E-4, 1.276002293417117E-6, -5.075183649152224E-5, 7.754431953300924E-5, 1.3281892206074236E-4, -5.829883503587728E-5, 5.808156731650575E-5, -8.08329384930015E-5, -2.449190414748829E-4, -1.987392417611292E-5, -2.149742757464735E-4, 2.9210047250646413E-4, -1.388614900430065E-4, 2.443446338157506E-4, -1.4832794145051026E-4, 1.2132203970495643E-4, -8.391927457650564E-5, 1.2472642899654025E-4, -4.981371824341679E-5, 2.233932993971799E-4, -3.6318444450382504E-5, 4.67804099111693E-4, -1.219945644561325E-4, -8.958888549334687E-5, -1.6958689893300724E-4, -2.1009334523513048E-4, 2.689180830794699E-4, 8.721881044967961E-5, 3.315117912650902E-5, -1.6401231643043107E-5, -1.68776398117408E-4, 4.8016617217774664E-4, -1.7850931082294473E-4, 1.9564663485634485E-4, -3.2806562930634336E-4, -1.1066437697461766E-4, 1.3056473195703882E-4, -9.448277598155138E-5, 3.1323670764296077E-4, 2.3989990415435763E-4, -2.5378530596298766E-4, 5.428920023418894E-5, -5.80706662109612E-5, 1.4860781528838924E-4, 2.0109091961596683E-5, -8.14361521820114E-5, -1.794337199124312E-5, -7.853378962636266E-5, 1.5381299793506555E-4, -2.188487164845568E-5, 6.437055046775689E-5, -1.151166950902221E-4, -2.7556165460028794E-5, 8.612242828973798E-6, -1.7369372366720357E-4, 4.3721897845428685E-5, -1.8439943531801427E-4, 2.979216875009635E-4, -1.4631750554144635E-5, 8.586833040730222E-5, 4.728318322466153E-5, 6.270625500973663E-5, -2.1282057167860448E-4, 3.714797917705776E-5, -1.1230828879340584E-4, -4.133052679815058E-5, -3.9291585964548476E-5, 4.2254643877046765E-5, -9.735504035161173E-5, 1.597198186017335E-4, 8.002253458895309E-5, 7.980361912380597E-5, 9.265899780376216E-5, -2.264872423276892E-5, 4.5438217364134254E-5, 2.0987739405095834E-5, -1.5262803938628777E-4, 2.6373873147646307E-5, 1.2137670214533956E-4, -1.2374128930163085E-4, 1.9924181252124642E-4, -2.103181853893698E-4, -8.107545309697661E-5, -8.842899516032447E-5, 1.0644916434283296E-4, 3.0162423165568152E-5, -1.0994518408142672E-4, -2.949018959703259E-5, 2.2890033046319065E-5, 2.5380399743714724E-5, 7.343744998587775E-5, 1.4416360711434848E-5, -1.895811112398638E-4, 1.0824328145462095E-4, 9.181436257698597E-5, 1.1542362741373549E-4, -3.723264469542957E-4, 8.745349347973313E-5, 2.0574948200529708E-4, 4.5132523873316066E-5, -2.649618688200724E-4, 2.0670386174632916E-4, 4.674769855670421E-5, -2.243794343070957E-4, 1.0468817190319147E-4, -1.470255461871483E-4, -1.58066104882081E-4, 1.14015037219058E-4, -3.695868623075759E-4, 2.7641348209162333E-4, 2.2250908470428587E-4, -1.1444977320571156E-4, -3.724851541363047E-4, 7.375685652817598E-5, 2.2367522043049547E-4, -7.95553321824643E-5, 5.9986369706206666E-5, -1.5229322632030078E-4, -3.933581231080212E-4, -7.358124895343607E-5, -1.7924521323654363E-4, 1.7346605698243325E-4, 1.0528146546614266E-4, 2.1464349176140854E-4, -2.040444868961147E-5, -1.227287912476444E-4, -1.0921125598664801E-4, -1.3660662961576648E-4, 2.580498367552018E-4, -1.3830272605338395E-4, 3.4018815296269714E-4, -3.3518927454890147E-4, 3.250440791278059E-4, -1.4724331757887035E-4, -6.301431350196497E-5, -8.241742341967779E-5, -4.046758092642868E-5, -1.1164905107049648E-4, -1.1581785207688674E-5, 1.6482247430570099E-4, -1.1428860344305522E-4, 9.483360502681327E-6, -2.1634892468489712E-4, -5.4927530975943956E-5, 1.1178768964667041E-4, 9.240565106552641E-5, 5.3295291770324966E-5, 5.005202142231713E-5, 1.9575126401893744E-4, -5.1521600502041874E-5, 1.1138670361089719E-4, -2.2459576666747708E-4, -9.372639200598546E-5, 8.870849270718479E-5, -1.0629061076075598E-4, -7.410868750562071E-5, -1.5422375228051066E-5, -1.1096104876091349E-4, 2.406741309756238E-4, -3.75934910113888E-4, -2.548359245002416E-4, 1.3102476690044938E-4, 1.151648080479576E-5, -1.3838542365127702E-4, -1.9554208080455092E-4, 8.831532574480423E-5, 8.128926386367376E-5, -3.297040021105291E-4, 5.795752532163556E-5, -1.0976402688178048E-4, 1.3776322952391588E-4, 1.3543203840668734E-4, -2.0828656308144412E-5, -2.1619379634152196E-4, -4.688010257687346E-4, 6.166840051261978E-5, -1.8212913295057227E-4, 2.9398774573419564E-4, -4.432867885411013E-5, -1.842694490497398E-4, 1.5222055585213884E-4, 2.919097591696303E-4, -1.1407351613320401E-4, -1.750051933946356E-4, 1.6631102842984088E-4, 1.194116310313559E-4, -1.053087376080182E-4, -3.0731883371051295E-4, 4.7793531350722286E-4, -1.0637583466688739E-4, -2.2399029241344532E-4, -4.503805124217779E-4, 2.498536091303748E-4, -2.2983326266332038E-4, -1.1165808424959151E-4, 5.592497417485466E-4, -4.63687271370077E-5, -2.0236297497370668E-4, 4.706804517532752E-4, 7.983485939914645E-5, 2.2118606815412466E-4, -3.7982631493713864E-5, 9.527727805212077E-5, -2.1941397535665125E-5, -3.8305064083769044E-4, -1.010009773451748E-4, 6.331598588208125E-4, -2.3679487168010854E-4, -4.078136835170155E-4, 2.1667612293187893E-6, -5.279844613987369E-4, -1.3606379059864402E-4, 2.158153059609603E-4, -0.0010274746063181645, -2.7400355161174596E-4, -8.84385874836283E-5, 0.0019423714780954036, -0.001192909661578815, -0.0015791061233875157, -4.383262911329624E-6, 2.5141468073319804E-6, 6.592330584169205E-7, 4.581042227826002E-8, 3.7631225283238427E-6, -1.627493777947212E-5, 1.2766092920869668E-5, -1.5505174996999498E-5, 2.567051532338671E-5, -1.4276322348476975E-5, 2.978399935738089E-6, 4.205672319664911E-6, 1.711431023641044E-5, -2.3473256864915714E-5, 4.500929571572967E-5, -3.872514252833709E-6, -3.2564711853889754E-7, -1.9272926230820694E-5, 1.0428710351649555E-5, -2.7698764324245826E-5, 1.2680715997980598E-5, 6.001478792801114E-6, 1.8828136550372277E-5, -3.370472648961951E-5, 3.7134144007229617E-6, -2.672372748485874E-5, 2.938758132530763E-5, 4.056074545431354E-6, 1.8064966526454935E-5, -1.122685663292088E-5, 3.503508337717614E-5, 2.095684191848169E-6, -9.149331309873176E-6, -2.591080961874985E-5, 5.6322388122171155E-6, 1.5493797189950712E-5, 9.87520461866329E-6, -2.6859131870656032E-5, -1.0702909272925908E-5, -1.1025865741110796E-5, -3.3809954361635496E-6, 2.511152740967813E-5, -1.1558789748304025E-5, -3.189459659254811E-5, 1.0636052655607547E-5, 6.272617570667674E-6, -3.3451641391008327E-6, 6.95190120371305E-6, 5.5935863097012276E-6, -8.954306120181337E-6, 2.2954932066894922E-5, 2.124216893308338E-5, 6.130365463468784E-6, -5.281233974438503E-6, -5.422237998439793E-6, 1.8979802132642716E-5, 6.976736160507085E-5, 1.8524443894533233E-5, 1.3719297517321606E-5, 1.5676847713272764E-5, 1.2350487487859936E-5, -5.41421685336606E-6, 3.700591747194938E-5, -1.1155388793776156E-5, 9.227118407699307E-6, -2.1746238653075104E-5, 1.676603347163476E-5, 4.029753338551139E-5, -2.188662150338298E-5, 3.845590646219774E-5, -1.549287948141755E-5, -2.6646005226898357E-5, -5.41790640237305E-5, -2.9035787737520242E-6, 1.4271868166530744E-5, 2.3806549615497125E-5, -1.9364585438785686E-5, -1.5418869327931154E-5, -2.2743583688711224E-5, 1.672389019475364E-5, 7.364336712873017E-6, 2.295378769809031E-5, -1.5771435788983196E-5, -8.763728592345321E-6, -1.8262639875077792E-5, 1.5045969773324079E-5, -2.317612740086881E-5, -4.118323780901345E-6, -3.4517750744080883E-6, -3.0058102980853434E-5, -4.264594036728687E-6, 5.259818868684962E-6, 5.816256069018558E-5, 7.172638140082665E-6, -3.642460619973284E-5, 3.204219135596908E-5, -3.4511058243930304E-5, 2.6169731896913694E-5, 1.832852003441171E-5, 4.3984752349699526E-5, 1.1306694129065847E-5, -2.0409092014945997E-5, -1.281385255903181E-5, 1.604539047539163E-5, 2.2002728724859017E-5, 1.0784021137117349E-5, -2.5635488147347232E-5, -7.535834845751857E-6, 5.82131039297281E-6, 6.715106539209982E-6, -6.708622638212132E-5, 4.246399138996444E-5, -2.6011521817535598E-5, -4.6867812765213446E-6, -2.194705344456553E-5, 3.592692909513341E-5, -1.4728225880588416E-5, -1.753020811668166E-6, -2.09602638171188E-5, -2.191526595032628E-6, -1.577943912067019E-5, 3.5987747706249014E-5, -1.540841084733479E-5, -7.408573699929462E-6, 5.548459913487965E-6, 7.265462339305464E-6, 2.7624390261777015E-5, 1.5994706016655156E-6, -1.2089127115006522E-5, 1.585411401828692E-5, -3.151756587564295E-5, 5.9833828238456464E-5, 5.035942519557974E-6, -1.8568772953925314E-5, -7.1107492394824725E-6, 1.2815391110475012E-6, 1.2734763184825643E-5, -2.352059464089313E-6, 3.7252454685932014E-7, 9.761622212568392E-6, -6.31903417262929E-6, 1.1725304016644894E-5, 8.902937658081635E-6, 7.1339230947922694E-6, 4.03332237285642E-6, -7.55103284158764E-6, 8.943202904522854E-6, 1.2195422725130482E-5, 2.760148297745402E-6, 1.8123091673282452E-5, 3.57237319590201E-5, -4.0645111527389064E-5, -5.773235618658767E-6, -4.318167079262971E-5, 5.2561757298239594E-5, -3.9417165568523876E-5, 2.5634551182005276E-6, -1.6344241682757254E-5, -3.867517338581862E-5, -1.0322312718934624E-5, 1.6582911576734193E-5, 1.8986699955951135E-5, 3.0632677589584865E-6, -4.28463584328223E-6, 2.0087207552551906E-5, 1.8112037653416537E-5, -3.969793256425989E-6, -4.85401014751986E-6, -3.4505320816695525E-6, -2.5478823733488486E-5, 2.6482714224661328E-5, 2.6691790318964344E-5, -3.3356647274287754E-6, 1.5612025424198146E-6, -1.9703105060203217E-5, 7.925174995613774E-6, 2.4504875104554855E-6, -2.5587410787768396E-5, 4.6259185512805386E-5, -5.0548597197982775E-6, -2.3860824822210565E-5, -8.098921057102539E-6, -3.230848607182606E-6, -7.966551445808651E-6, 8.950963982846483E-6, 2.4040460165233033E-5, 3.683138491129736E-6, -2.527091253118688E-5, 1.5850683754384362E-5, 4.3682876725446114E-5, -2.047413132312266E-5, 3.928932627804256E-5, -1.062764250463251E-5, 3.755319115480841E-5, -9.263393143364844E-6, 1.8147081113423203E-5, -2.3402393717874984E-5, -1.0429614793169882E-6, -6.376056329530878E-6, 1.7482241734815412E-5, -9.231790805633495E-6, 2.198675970530637E-5, 5.567941927751281E-6, 9.64075185938696E-6, -1.5404144555277223E-5, -4.2010437253315165E-5, 3.297821098786967E-5, 2.7799433928119466E-5, -1.440684713532517E-5, 1.7942110103092183E-6, -8.047751306983877E-7, 2.3294925818677857E-5, -2.678267978061094E-5, 6.9595099810756245E-6, -1.1440032156954656E-6, 2.026422263268733E-5, -1.472584941682913E-5, 4.254070005943293E-6, 1.1311215814668884E-5, 4.605713420371949E-5, -3.900887938261262E-5, 2.1509296916258077E-5, -5.414497641553388E-5, -3.072797529615821E-6, -1.9838078726379903E-5, 2.55573205840941E-5, 1.4866468063298504E-5, -2.656123625197884E-6, -4.447337260221398E-6, 1.4921737752574243E-5, 5.856520903516245E-6, -6.196035605477239E-6, 1.3119630528533595E-5, -1.374072210139673E-5, -1.9976951791302592E-5, -1.6777435988035854E-5, 2.910151544076474E-5, -9.316428569396656E-6, -1.4806628641148733E-5, 4.299124876693981E-6, 4.891044792392899E-5, -5.1450345303993925E-6, -6.532412920649995E-5, 1.568867194520052E-5, 2.8003778638664256E-5, 5.3948333238957105E-6, 2.4267336068393183E-5, 6.356683244184542E-6, 1.1524933425406447E-5, 1.5895280323025923E-5, -1.072103321021469E-5, -7.198357445963488E-7, 3.2599522376647246E-5, -1.6847612026786357E-5, -1.4390778701873466E-6, 7.897965008713057E-6, -1.6881238964527802E-5, 1.5190259198437882E-5, -2.756064541111942E-6, 3.116139884116494E-5, -1.1152100464474282E-5, 6.821919087596592E-6, -3.682302611813159E-5, 2.6980616598353013E-5, 4.246503409332672E-6, -6.03673370870464E-6, 2.6880327694085476E-5, -8.781833613379778E-6, -1.2413416782170995E-5, -1.3698019648128315E-5, 2.15503837382905E-5, 3.338682395678241E-6, -9.514147060239384E-6, -2.2223172732705347E-5, 6.204345487090295E-6, -2.7362002053450143E-5, -3.5249596762671154E-5, 4.771444022427649E-5, -1.3523688651686551E-5, 3.665527517686287E-5, -2.2739532868905793E-5, 1.8560504436417426E-5, 2.119028504082816E-5, -2.6166689933669867E-6, 1.959652685520224E-5, 4.1634461515352764E-5, -7.901074733906045E-5, 3.90973750682822E-5, -7.730482633852272E-5, 6.157221363323792E-5, -4.767903880413066E-5, -3.4415972953568476E-5, 2.379343630430702E-6, -5.4233799270147944E-6, -6.202484927976612E-6, 1.5774542359270398E-5, -2.5957240196364056E-5, 7.975221982386483E-6, 7.172431878752113E-6, 2.245223992861999E-5, 1.5255505563666738E-5, -1.664151870712019E-5, 4.491377777304494E-5, 4.219986598375562E-5, -5.231113184176904E-5, -1.458824022608317E-5, -4.15714524183954E-5, 4.9013042217505736E-5, 3.581902086940048E-5, -2.0306875700334747E-5, 3.0500488415763924E-6, -2.581750502745684E-5, -6.2546692564982E-6, 4.095672014708311E-5, 1.2423844495921882E-5, 6.737147100933157E-5, -7.266805302785263E-5, -1.7372984744939545E-6, 2.8203463274730834E-5, 7.64401121185589E-6, -3.797932442991036E-5, -1.8615244013885904E-5, 2.2389972898896203E-5, 1.9599634711563174E-6, -1.7871386423843044E-6, -3.0971827927992086E-6, 4.598666296577172E-5, 1.189652913348329E-5, -1.7539812034875427E-5, 1.6287420477749233E-5, 6.463249799271724E-6, -1.2879399913073695E-6, -2.248261873458976E-6, 1.0066563021502814E-5, -2.4554538892990692E-5, 1.8055813094886453E-5, 4.827421048920094E-7, 7.872066631533748E-6, -1.0324220284049795E-6, -6.601010339584318E-6, -2.1598325428383574E-6, -7.200661206285862E-6, 7.141191283346345E-6, -7.353743820581282E-6, 1.2269641922966253E-5, 1.1284152916835508E-5, -2.7262846418142346E-5, -4.639450892684193E-6, 7.072361586639986E-6, -6.1301128907934276E-6, -1.2639805415740095E-5, -2.4742294310418092E-5, 2.820569107387093E-5, 2.648122872380295E-5, -5.832963782345744E-5, 5.152396308556448E-6, 4.643909431603356E-5, 1.351719571081073E-6, 1.6258733842958466E-5, 1.0162688285770909E-5, -1.519867657113447E-5, 3.0057145860618506E-5, -4.0912206586000846E-5, 5.885474437297651E-6, 1.4980804651393309E-5, 5.675642707652211E-7, -3.85096713264833E-5, 6.702211767184061E-6, -9.871956142005144E-6, 1.9578705436272176E-5, 3.7460899269036797E-6, 1.5424980038387243E-5, 9.753869706750315E-6, 8.43292550446449E-6, -1.0916170750370442E-5, -1.5030179130473011E-5, -1.5728687826392308E-5, -1.8262648000926391E-6, 1.4768847328670029E-5, 3.6881090703566512E-6, -3.905886616772849E-6, -4.436708641550845E-6, 5.29173133824978E-6, -1.0817649706965563E-5, 2.0109898276113574E-6, 2.152476291057348E-5, -3.4094617365230324E-5, -1.3953832859619195E-5, 4.432446712669365E-6, -7.420118090077654E-6, -1.4845854604703652E-5, -1.882160424210287E-5, 2.9064454485959993E-5, 2.0363167043485213E-5, 2.280442508401651E-6, -2.6992483922178576E-5, -2.2989041108558133E-6, 8.981617781501656E-6, 1.4992597689433538E-5, 7.895752449645863E-7, 4.060721036741359E-6, 3.5603066431422203E-6, 2.1457873610073017E-5, -7.986811919890628E-6, 2.403641997799868E-5, 1.69750697309064E-5, -1.3111746901961127E-5, -2.217366745783532E-5, 7.718452324331266E-6, -4.083548177030998E-6, -1.5104976233655678E-5, -4.8248978709028835E-5, 3.2522340268088985E-5, 2.7321679391863207E-5, 1.1821895440126094E-5, -1.3253582442398744E-5, 8.091815967343986E-6, 1.7721195499513566E-5, 2.9067451492348795E-6, -9.81849108238505E-6, 1.262317652381815E-5, 2.649204710462103E-5, -1.535874165372236E-5, -1.1596913137939667E-5, -1.555121787967452E-5, 3.29332429764009E-6, -2.6930506591280292E-5, 1.9294677198840565E-5, -1.7493410016711423E-5, -6.3224027213848975E-6, -9.55435503718964E-6, 2.5142746851172348E-5, 9.118804927957854E-6, -9.07490191737103E-6, -1.7181120929258588E-5, 4.1888835307995085E-6, 8.938325091062971E-6, -2.3518739271588784E-5, 1.950492536227958E-5, 6.681865669458953E-6, 1.8974404222314094E-5, -4.024435366873955E-5, -9.51640723657627E-7, -2.503473871386195E-6, -2.745560087339416E-5, 3.6838705032431127E-6, -1.8509229778761714E-5, 1.4388392041752088E-5, -1.3802976810670586E-5, 1.6716312977263362E-5, -6.645233903154117E-6, 1.2914726255848692E-5, 1.6669303493490402E-6, -9.809781884799688E-6, -3.573703131291839E-7, 1.239201709320642E-5, -9.718545887553399E-8, 8.292642362049208E-6, 4.157333591243463E-6, 2.401932951589901E-5, -3.647213116139122E-5, 4.362145162719851E-5, 3.832251917128559E-6, -6.359397697933199E-5, 2.975615925101353E-5, -9.773808635474583E-6, 6.638260180839265E-5, -5.962585831425474E-5, -1.5157186502919029E-5, 9.99298147146509E-6, 5.097676009605131E-5, 9.706034781802118E-6, -1.3880859564899316E-5, -1.178881347028949E-6, 8.146414779896464E-6, 1.5510183853502347E-5, -6.271821206688007E-6, 1.4023085347336889E-6, -6.6085566931520035E-6, 8.446582272026392E-6, -2.7432442791773277E-6, -6.2199944990290424E-6, 4.0645836431976484E-6, 1.5372124027816854E-5, -3.687795571581914E-6, -2.607129877658072E-5, -2.8434633316713033E-6, -6.615697508551639E-6, 1.6116067263353978E-5, 9.992158667205143E-6, -1.7775478706649663E-6, 1.964378753724515E-5, 2.845041031222307E-5, -2.430361553111199E-5, 2.706269149796469E-6, 2.098625729503549E-5, 1.264107254717461E-5, -2.001024894721486E-5, 8.428973300985188E-6, -3.532163957066753E-5, 1.0693756911005458E-5, -1.4292018240521349E-5, 3.614209337572522E-5, -6.674152713787278E-5, -2.6144434166767265E-5, 6.72129820420947E-5, -5.9996266323754214E-5, 7.84400968442938E-6, -2.742559726327321E-5, 4.073498149896754E-5, 1.6729697320708495E-5, -2.848341288686618E-5, 2.847471121516384E-5, 1.808668947618581E-5, -1.770433744257378E-5, 2.8076996845333364E-5, 8.265816490815581E-6, -2.0970287204482782E-5, -4.309955674991429E-6, -1.824614548708481E-5, -7.1318229985946856E-6, -2.6714361066241056E-5, 9.679211694539733E-6, 2.9488154353092753E-5, 1.5728898682346383E-5, 4.46137832422794E-6, -1.4936475566851965E-5, -1.547230574775924E-5, -4.8432579547762875E-6, 2.7444599397054926E-5, -9.862942959865974E-6, -2.0830472290811646E-5, 3.0199743011690978E-5, -6.731310388817996E-5, -1.6389754073692006E-5, 2.5580715045183473E-5, 1.9612627483463316E-6, -1.0153913336307052E-5, 2.3304089354968444E-5, -1.5464503835840534E-6, -8.70223421836966E-6, -5.264548112884448E-6, 3.683258742236405E-5, -1.1132502714691992E-7, 1.7051754595759382E-5, -1.5431352102206815E-5, 9.265974026466091E-6, 1.55597805393003E-5, 3.445716437740806E-5, -1.846383705954897E-6, -7.231846315746202E-6, 1.4097577389623117E-5, -6.823467183391513E-6, -1.7407686372422973E-5, 4.416196272811324E-5, -5.418669149256765E-6, -1.1793942724539614E-5, 3.467144849695128E-5, -4.877972604823286E-6, -6.452353621588737E-5, 1.0533907091267116E-4, 2.862793410624069E-5, 3.0338342388766082E-5, -7.291375980678723E-5, -2.0098930206363945E-5, 3.5038959939746086E-6, 2.606993334370477E-5, 9.811135661501108E-6, 2.1608476855204954E-5, -2.9650967728127656E-5, 9.544972479362117E-5, -1.6194890007444914E-5, 2.7774659102217407E-5, 3.937607023138409E-6, -1.0916840570975077E-5, -6.284007094868539E-7, 7.657721000856022E-6, -2.7015231406136736E-5, 9.758785661601E-6, 4.279962783462768E-6, -4.31541503452918E-6, 1.0300862843729532E-6, 1.2618419630830138E-5, -2.1051469268459553E-6, 5.5267686730251615E-6, -6.1067007963424235E-6, 1.0313029504567132E-5, 8.710940953210903E-6, -9.401643540517805E-6, 2.6147784312827052E-5, 2.6852553286490415E-5, 1.2344857581148661E-5, -3.2391279005089535E-5, -6.924428211865831E-6, 2.1898145227891358E-5, -1.1580366486184222E-5, -5.858226313118389E-6, 2.5074563479214146E-6, -2.3274368965290064E-5, 3.668552036216504E-5, 4.287576542864741E-6, 2.0906062356571844E-5, 6.436492195609103E-7, 2.1632724078103146E-6, -2.493269190203233E-5, 1.3700542656940333E-5, 1.2111844358746566E-5, -2.1439911158988722E-5, 1.1850526085112502E-5, -5.56408376769851E-6, 1.1099582897150208E-5, 1.0018700809013846E-5, -1.9220590891446716E-5, -3.8658715006809383E-7, 3.31806252540618E-5, -2.7197891670179062E-6, 1.310623647317755E-5, -3.971191963096437E-5, 1.22908405853902E-5, -2.283226465009931E-6, -4.934148707135992E-6, -1.3538920643191773E-5, 7.470433416492835E-6, 1.0154317238978046E-6, 1.0937734367991582E-5, 1.0228838780657599E-5, -3.196983746323937E-5, 4.223310223111875E-5, -3.1096036566334204E-5, 1.5081084581063792E-5, -1.808059250253284E-6, 1.6164417058470214E-6, 1.4244818418662872E-5, 1.7715940521325455E-6, -1.4730402966087002E-5, 5.990956295739172E-6, 1.0308241672921608E-6, 1.975354473277427E-6, 1.0887267045856707E-5, 3.63708765332785E-5, -4.695550365590271E-5, 1.994556158034612E-5, -1.681406307084155E-6, 2.909561762644374E-5, 4.47356547635374E-6, -1.519496393433676E-5, -9.039720507984024E-6, 2.450649110653639E-5, 9.855932766458277E-6, 2.981843486336082E-5, -3.087398717003655E-5, -1.3034586651545902E-5, 2.969940550822032E-5, -1.0306819797405652E-5, -8.601111541518227E-6, -1.1993054874652909E-5, 2.7447031918630984E-5, 1.2587250578850116E-5, -2.783483927395596E-5, 1.1410559374458809E-5, -6.434033825632114E-6, 2.7089536458961405E-5, 2.2140428904226074E-5, -1.1446975090424391E-5, 1.1996353361907075E-5, -1.8053562935264455E-5, -1.5706989670390115E-5, 2.387844362174068E-5, 3.568323245514097E-5, 4.379779521040294E-6, -1.6889823980433547E-5, 1.8141126324536282E-5, -3.774863618568065E-6, 5.228174353482225E-6, 5.2323278454290936E-5, 4.632399612257346E-6, 1.055688662127748E-5, 7.0288588828327624E-6, -3.1008831625094864E-5, 4.692495344032501E-5, -4.249814579128722E-5, -4.772424167238747E-5, 3.846181991640565E-6, -2.81818266287346E-5, -1.6234593067090074E-6, 5.74977758623867E-5, -2.1272919057170017E-5, -9.616579421869789E-5, 6.389512646836205E-5, -8.856323238168014E-6, -3.457326415961409E-5, -6.533599762009019E-5, 1.4462213608887982E-5, 2.278789882890801E-5, -1.338542979832059E-4, 4.9580272068880686E-5, 2.50562566698467E-6, 1.5405263255767953E-5, 3.168064392496464E-5, -7.671128243272408E-6, 6.935451135763613E-5, 2.8441282317145933E-5, 2.490362334595397E-6, -1.4301982717869927E-5, 2.9729298606194824E-5, -1.0813274850104488E-6, -2.5507231399217426E-5, 1.9335877998520666E-5, -2.020624139034851E-5, 3.335788919664462E-5, 3.587408218431014E-6, -7.082530491355106E-6, -1.682356439950284E-7, -2.676855421639298E-6, -3.7936108711411005E-5, 3.672257722162455E-5, -4.417045512054461E-6, -1.8275293135256883E-5, 5.525238427093787E-5, -1.823563272521821E-5, -7.054843250356652E-5, 7.529873665368782E-5, -2.9587267195495736E-5, -4.94321368125403E-6, -5.6051124577365604E-5, 5.9491144682363045E-5, -4.14839347979237E-5, 2.0866045901233796E-5, 7.452514425391045E-5, 2.5491117648539718E-5, 1.978934528925171E-5, 1.0679340260765944E-5, -1.5416570241270238E-4, 5.708077329587035E-5, -8.666796948581955E-5, 1.0350874497382654E-4, -1.261796964503242E-4, 1.821789297057092E-4, 6.489110718719432E-7, -2.32940045738812E-6, 2.2155150993727064E-6, 5.728612992247961E-7, -1.5406751732793074E-6, 9.239972860381661E-7, -4.5186475867704034E-7, -2.1012921967762648E-7, 9.09271639101384E-7, 1.2410733671463685E-6, -2.563969162569458E-6, 4.609504272570332E-7, 7.550623019536711E-7, -3.154106687510623E-6, 2.6296791142198337E-6, -1.4694792374281794E-7, -1.53464976188384E-6, 9.877065347876348E-7, 2.5038823021687165E-6, -3.995053117757742E-6, -5.552232533234859E-7, 2.8738915531938044E-6, -1.0484906786520278E-6, -2.4941826652046786E-6, 2.094804557093593E-6, 6.182812271082318E-6, -8.419845664991418E-6, 2.77338407127318E-6, 7.592182110208645E-7, -1.9408214993712125E-6, -2.8522821982448736E-6, 3.7788035632035704E-6, -2.9068963148983894E-7, 4.978209545277606E-6, 3.0876610685324203E-6, -5.722331979309491E-6, 6.434882744866936E-7, 5.261860435002479E-7, -3.663604840559774E-6, 5.441811574549742E-6, -5.695205788686824E-7, -4.057450452043415E-6, 7.770747330846172E-7, 3.574479836144888E-6, -3.3481504180363393E-7, 4.922592689711327E-7, -5.073357860632316E-6, 1.936072380457121E-6, -6.489868575413685E-7, 4.193273675987111E-6, -1.7589911567193268E-6, 8.868713668173817E-7, 1.6649445953198939E-6, -2.9663056634193107E-7, 5.029218227081908E-6, -2.544200999577274E-6, 3.731073849934396E-7, -1.6800003151282378E-6, -8.892665639197931E-6, 3.2830131842655035E-6, 2.7456120415055994E-6, 1.222488995558668E-6, -4.310447067024667E-6, -3.6265465083684665E-8, 8.937946855061123E-8, -1.1938009290851279E-7, -1.8506182834142898E-8, 8.271932647033916E-8, -2.768569943169931E-8, -5.28826797129451E-8, 3.210680284376681E-7, 2.4251648523731554E-9, -1.4885346973656296E-7, 6.027715039757905E-8, -1.5654479855887296E-7, -6.062481099296503E-8, 4.0502330017508654E-8, -8.384530051627711E-8, 7.25526607371198E-8, -1.5418931164496818E-7, -1.4542549276027883E-7, 2.6121997830585E-7, -9.647344098083795E-8, 2.241667530462229E-9, 1.274568991219813E-7, -1.4166012489824333E-7, 1.7863708156092768E-7, 7.847412649508962E-8, 3.104832382436577E-8, 6.247970149272138E-8, 6.652238238316488E-8, 2.9006640936775884E-9, 4.6094661327609664E-8, -1.6581661999254665E-8, 1.2510870729161566E-7, -1.118126183401271E-7, -2.263335083713247E-8, 1.3163332055622387E-8, -2.1278039924870205E-7, 3.2834557456035574E-8, -1.28834120225102E-7, 1.183201290492438E-7, -1.8220628481315254E-7, 7.14911998918029E-8, 2.6629754025092194E-7, -4.6409180041655327E-7, -9.041785407514853E-9, -1.5438535257752397E-7, 8.650970390408917E-8, -4.60611892097517E-8, 2.4028388918610156E-8, 1.0720539223986245E-8, -9.859212050916379E-8, 3.2104593287194264E-8, 9.280767615632807E-8, 8.884488734289819E-8, 6.501109581225073E-9, 2.2052372473588266E-7, -3.793448716694209E-8, 
        };
        
        System.out.println("\n" + "!!!!!!the ECIs were fitted @ 2020/11/01_355, including all 4 Pt(111), and 3 Pt29Ni7(111), the cutoff *OH coverage is 1.0 ML!" + "\n");
        System.out.println("\n" + "!!!!!! the cutoff # of NPs training structures is 100\n");
        System.out.println("\n" + "!!!!!! the maximum *OH coverages of training structures is 0.56ML\n");

        System.out.println("the # of ECI are: " + eci.length);
        
        ce.setECI(eci, activeGroups, false);
        return ce;
    }
    
    

    
    /**
     * 
     * LOOCV = 2.2 meV/atom
     * 
     * 
     * 
     * @return
     */
    
    public static ClusterExpansion getPreFittedCE20210121_PRIM_PtNiVac_201_Accu(){

        ClusterExpansion ce = buildCE_PtNiVac();
        ClusterGroup[] activeGroups = ce.getAllGroups(true);

        
        double[] eci = new double[]{
                0.22910046396939887, -0.3365867106125904, 0.17945685921249865, -0.0012486651384458272, 0.0018456272925309477, -2.5264895670362394E-4, -5.474399925013184E-4, 2.133393712251486E-4, 7.802557029486477E-4, -7.055236075454931E-4, -4.056006781654013E-4, 2.3946111609067858E-5, -8.533299068226525E-4, 0.002904184390767404, -0.004958374519997356, -0.0013386973998840207, 1.9931409160580229E-4, -7.345309696215052E-4, -6.52637693385129E-4, -0.003287683696879859, 0.0020555671063623784, -0.01348150620599596, 0.014155772727732661, 0.003493862994048552, 8.703414228906155E-5, 1.2937025696765176E-4, -2.645503449977683E-4, 3.0258811612297876E-4, -2.530458880434152E-4, 3.2518240540181306E-4, 1.3816508090275367E-4, -1.4824707667295846E-4, -6.450008755822965E-5, 1.3959558569743517E-4, 1.267902309550292E-4, -3.212115751028378E-4, 1.1377061013693557E-4, -1.575927181787502E-5, 2.3676712775211437E-4, -1.638827395315454E-4, 5.231372056732526E-4, 2.787074949585312E-5, -4.4928903258921633E-4, -1.739262559155236E-4, 1.2331939960848561E-4, -3.0753979897633877E-4, -3.4415729346562625E-5, -7.596483423566057E-6, -5.646572321693166E-5, 3.7794030686362935E-4, -1.5451646665405995E-4, 4.793742923875008E-5, 1.8496170618836796E-4, -2.487037668809323E-5, -4.2852160584014635E-4, 4.425174433935787E-5, -1.5746285486052517E-5, -5.2109291111660655E-5, 7.718187450569062E-4, -4.668924165806077E-4, -4.391147684602685E-6, 1.9058690995188378E-4, 1.3081523931612144E-4, -1.6960138951462343E-4, -2.9506531726559027E-4, 4.690140127168868E-4, -3.312628596988286E-4, -3.5926056747405746E-5, 5.542942616850127E-5, -5.241142409025381E-5, 8.634986381114837E-5, 1.1874534802730138E-5, 1.3457380551530748E-4, -7.873047649021485E-5, -5.4972198830620725E-5, 1.2291139375159976E-4, 3.5019702723404066E-4, -1.4755449400865792E-4, -1.7607959019956673E-4, -5.4293708013084863E-5, 1.3859025263837296E-4, 2.686233738777898E-4, 6.465604692215801E-4, -6.459781645600862E-4, 4.822316649384824E-5, 1.3545857136980062E-4, -3.3262003118689817E-4, 3.5585203786910264E-4, 3.3485091419713925E-4, -1.0694307604360503E-4, -6.098270985657045E-6, -6.938479447819209E-4, 4.2622463025415724E-4, 4.0936926608666196E-4, 2.74074673084811E-5, -2.0007802383151078E-4, -2.847227506526961E-4, 1.4968683743792565E-5, 2.6577530536537036E-4, 1.1131718635027169E-4, 1.1248274691834525E-4, 4.354900762403328E-5, 4.1792815159327453E-4, 2.3469250430916723E-4, -4.512739710018157E-5, 2.684010150705426E-4, 5.056955864305904E-5, -4.2470147721826706E-5, -7.227924682922976E-5, -1.7993571333106044E-4, -1.8734324413663904E-4, 3.919337402648955E-4, 1.2636242844229085E-4, -2.574209292547603E-5, -5.5819800386382526E-5, -4.4162155155593255E-4, 2.029062509598242E-4, 3.2608089640820837E-4, -6.230900600635858E-5, 1.0047198893218178E-4, 5.384624845857907E-4, -5.593652283530217E-4, -3.834612039404321E-4, 5.105983549766342E-4, 9.734534823184032E-5, 3.404699706716129E-4, 1.3651241604492343E-4, -1.3326392305547212E-4, -1.7264338313993178E-4, 3.299773843558609E-4, -1.4448622432188668E-4, -2.860411983786691E-4, 4.5396721878248E-5, -2.3284625382334628E-4, 1.2188259582881578E-4, -1.1928906734644338E-4, 1.7934970954257322E-4, 4.295312796791001E-5, -5.594354352751827E-4, -0.0012476528995144706, 1.4755675860172795E-4, 1.75850147707744E-4, -1.4714101141779373E-4, 7.547496692695095E-4, -7.33039686554672E-4, -8.735704044714764E-4, 4.2077272020045464E-4, -2.876795971699306E-4, 1.2881216755683805E-4, -0.0012885011354715659, 0.0030416301706552426, -3.417025665194296E-4, -8.836767767938115E-6, 1.6717633780413265E-6, 1.8448965342187165E-5, -2.9686567776425173E-5, -1.876447602280212E-5, 7.083059387044417E-5, 5.112217735615604E-6, -3.993690295560858E-5, -5.54613065187478E-7, -4.872416045709302E-5, -2.1824206329629312E-5, -1.3373454853519333E-6, -7.595853995888489E-6, 1.7560658040096603E-5, -6.887585521821835E-5, -1.3249702647442536E-4, 1.3447519682872455E-6, -8.161439136472105E-6, -1.7900830914413014E-5, -1.8498891359328867E-4, -2.699837579600334E-5, -9.329186801258012E-5, 2.7064340932998776E-6, 4.111007442384688E-5, 3.49703794581878E-7, -6.447993883892749E-5, 9.140085609143814E-5, 4.1237621248552125E-5, 1.1605533771534901E-6, 7.291348205738235E-5, -7.705852999417254E-5, -3.3140818357995597E-6, -2.2039985729237774E-5, 1.4923544626291893E-5, -7.075868612463733E-5, 8.943431894521702E-5, -7.675052097153479E-5, 1.4661986108213329E-5, -6.883468317178416E-5, -8.938708329175264E-6, -2.9267325885048252E-5, -1.2837864981815258E-5, 7.285919131156142E-5, -1.1791128800247306E-5, -4.640823951673366E-6, 5.552437397755112E-5, 1.2519452277623996E-5, 1.2181562807322987E-5, -1.667071131714352E-5, -2.111167566616498E-5, 4.649277724706833E-5, 6.204442013378175E-5, -3.77324793778348E-5, 1.5751623120064707E-5, 7.338835524070408E-5, -6.584232501284052E-5, 2.4818559254700945E-5, 1.212872068693762E-6, -4.203645513577983E-5, 9.509778207974606E-5, 4.559408153259438E-5, 2.1886514969056317E-5, -2.3950311533241036E-4, 1.0055159327105939E-4, 4.06829478169805E-5, 6.721356027420246E-5, 3.9257949356688755E-5, -7.220172397362067E-6, -6.732686534870866E-6, -2.05723187499732E-5, 2.3883093708689722E-5, -2.9987644170590766E-5, 1.9937572463101915E-5, -9.521963293195393E-6, -4.5302953738786617E-7, 7.903071038420286E-5, -6.69926344046883E-5, -4.269830749555189E-7, 1.618493758363869E-5, -3.51276292822096E-5, -3.298519471075701E-5, 9.833885408309169E-6, 1.1443999997465997E-5, 1.5106570282719784E-5, -1.0953066838170322E-5, 4.064048616744982E-5, 1.963197017774694E-5, -6.049037240438837E-5, -3.3251028311341536E-5, -1.9819640711707235E-5, -8.833211807548742E-6, -1.5518300983140493E-5, 2.1754936471087222E-5, 4.93959478355171E-5, 5.952584835327993E-7, 6.6413034932617104E-6, 2.978504370903536E-5, 1.1495623065827507E-4, -1.4779109994858802E-4, -1.2845388064032016E-5, 7.226658413897928E-5, 6.8226840207487305E-6, -6.854195538389014E-6, 1.996963168767867E-5, 2.9729273814484852E-5, -7.460990782566263E-5, 6.443844006651142E-5, -3.694643395010225E-5, -4.357488860859388E-5, 2.2079702137789008E-5, -3.196498440799652E-5, -1.6476416957398156E-5, 3.848399987021244E-5, -5.2686521555266996E-5, 9.347107258549743E-6, -1.5322268836876927E-5, 2.6812585630069802E-5, -6.855391763673164E-5, -1.0149504754527311E-4, 5.662628907283321E-6, -1.246505154260391E-4, -4.966840859198479E-5, 1.1979616025920622E-4, 4.5696661555829016E-7, -3.1510666540890517E-6, 2.0486045745091736E-6, -6.8981537834351785E-6, -4.006211583964218E-6, 1.6690724521058198E-5, 2.0422703420586727E-5, -1.6648434549510654E-5, -5.482276937448572E-6, 3.1396187912771615E-5, 5.783000424800212E-6, -6.616274371920453E-7, 4.2380271762674875E-5, -4.5492620702248555E-5, 4.859368998663234E-5, -1.1596053502161579E-5, 3.790745299166512E-5, -2.6826531832678025E-5, 3.0192441032969542E-5, -6.760082421854944E-5, 4.4366147897864096E-5, 4.682487171003727E-6, 5.799794439047333E-5, -2.6228894881895684E-5, -2.5963757894568957E-5, 2.447173714238129E-5, -5.621790470870108E-6, 9.641032983145783E-5, -2.0414602976588463E-4, 8.164987762980026E-5, 1.8747100326657086E-5, 9.25021902214206E-5, -2.3153362311103577E-5, -4.702610012657842E-6, -6.24813576995852E-7, -5.247907004624129E-6, -1.2704786633977632E-4, 3.9267105429843026E-5, -1.0017433686421444E-4, -6.81996556998061E-6, -8.571845674766945E-7, 1.3161841298190997E-4, 5.754884395085466E-5, 2.16353448409016E-4, 6.920622497067248E-8, -5.960493757919171E-6, 3.1323313023544434E-6, -2.0604967890223245E-6, 9.881625215065208E-6, -4.582842258273303E-6, 1.0316239732244355E-5, -1.1171636294226307E-5, 6.802261921553274E-6, 3.4350075787024058E-6, 3.136492263510233E-6, 1.0715458314888111E-5, -5.558771887284899E-9, 2.1340495159107294E-7, -8.916007004773383E-8, -2.1940031669462276E-7, 2.2071439699742998E-7, 4.563257912431368E-8, 1.0530708323408522E-7, -7.349275077395134E-9, -3.9461521626413254E-7, -2.075392866269718E-7, 
        };
        
        System.out.println("\n" + "!!!!!!the ECIs were fitted @ 2021/01/21_201 for PRIM-PtNiVac, PREC=Accurate, LOOCV=2.2!" + "\n");

        System.out.println("the # of ECI are: " + eci.length);
        
        ce.setECI(eci, activeGroups, false);
        return ce;
    }
    
    

    public static SuperStructure centerStructure(SuperStructure structure) {
        
        if (structure.numPeriodicDimensions() == 0) {
          return structure;
        }
        
        if (structure.numDefiningSites() == structure.numDefiningSitesWithSpecies(Species.vacancy)) {
          return structure;
        }
        
        Structure primStructure = structure.getParentStructure();
        LinearBasis primBasis = primStructure.getDirectBasis();
        
        Coordinates centerCoords = null;
        for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
          Structure.Site site = structure.getDefiningSite(siteNum);
          if (site.getSpecies() != Species.vacancy) {
            centerCoords = site.getCoords();
            break;
          }
        }
        
        double centerShift = Double.POSITIVE_INFINITY;
        BravaisLattice superLattice = structure.getDefiningLattice();
        while (centerShift > CartesianBasis.getPrecision()) {
          double[] centerCoordArray = new double[3];
          int numSites = 0;
          for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
            if (structure.getSiteSpecies(siteNum) == Species.vacancy) {continue;}
            Structure.Site site = structure.getDefiningSite(siteNum);
            Coordinates nearbyCoords = superLattice.getNearestImage(centerCoords, site.getCoords());
            centerCoordArray = MSMath.arrayAdd(centerCoordArray, nearbyCoords.getCartesianArray());
            numSites++;
          }
          for (int dimNum = 0; dimNum < centerCoordArray.length; dimNum++) {
            centerCoordArray[dimNum] /= numSites;
          }
          Coordinates newCenterCoords = new Coordinates(centerCoordArray, CartesianBasis.getInstance());
          centerShift = newCenterCoords.distanceFrom(centerCoords);
          centerCoords = newCenterCoords;
        }
        
        double[] cellCenter = new double[] {0.5, 0.5, 0.5};
        Coordinates cellCenterCoords = new Coordinates(cellCenter, structure.getDirectBasis());
        Vector translation = new Vector(centerCoords, cellCenterCoords);
        double[] primTranslationArray = translation.getDirectionArray(primBasis);
        for (int dimNum = 0; dimNum < primTranslationArray.length; dimNum++) {
          primTranslationArray[dimNum] = Math.round(primTranslationArray[dimNum]);
        }
        Vector primTranslation = new Vector(primTranslationArray, primBasis);
        
        StructureBuilder builder = new StructureBuilder(structure);
        for (int siteNum = 0; siteNum < builder.numDefiningSites(); siteNum++) {
          builder.setSiteCoordinates(siteNum, builder.getSiteCoords(siteNum).translateBy(primTranslation));
        }
        return primStructure.getSuperStructure(builder);
        
      }
 

    
    
    
    
    public static SuperStructure centerStructure_Nanowires(SuperStructure structure) {
        
        if (structure.numPeriodicDimensions() == 0) {
          return structure;
        }
        
        if (structure.numDefiningSites() == structure.numDefiningSitesWithSpecies(Species.vacancy)) {
          return structure;
        }
        
        Structure primStructure = structure.getParentStructure();
        LinearBasis primBasis = primStructure.getDirectBasis();
        
        Coordinates centerCoords = null;
        for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
          Structure.Site site = structure.getDefiningSite(siteNum);
          if (site.getSpecies() != Species.vacancy) {
            centerCoords = site.getCoords();
            break;
          }
        }
        
        double centerShift = Double.POSITIVE_INFINITY;
        BravaisLattice superLattice = structure.getDefiningLattice();
        while (centerShift > CartesianBasis.getPrecision()) {
          double[] centerCoordArray = new double[3];
          int numSites = 0;
          for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
            if (structure.getSiteSpecies(siteNum) == Species.vacancy) {continue;}
            Structure.Site site = structure.getDefiningSite(siteNum);
            Coordinates nearbyCoords = superLattice.getNearestImage(centerCoords, site.getCoords());
            centerCoordArray = MSMath.arrayAdd(centerCoordArray, nearbyCoords.getCartesianArray());
            numSites++;
          }
          for (int dimNum = 0; dimNum < centerCoordArray.length; dimNum++) {
            centerCoordArray[dimNum] /= numSites;
          }
          Coordinates newCenterCoords = new Coordinates(centerCoordArray, CartesianBasis.getInstance());
          centerShift = newCenterCoords.distanceFrom(centerCoords);
          centerCoords = newCenterCoords;
        }
        
        
        double[] cellCenter = new double[] {0.5, 0.5, 0.5};
        Coordinates cellCenterCoords = new Coordinates(cellCenter, structure.getDirectBasis());
        Vector translation = new Vector(centerCoords, cellCenterCoords);
        double[] primTranslationArray = translation.getDirectionArray(primBasis);
        
        /*
        System.out.println("\nprint out primTranslationArray: ");
        for(int i=0; i< primTranslationArray.length; i++){
            System.out.print(primTranslationArray[i] + ",  ");
        }*/
        
        
        for (int dimNum = 0; dimNum < primTranslationArray.length; dimNum++) {
          primTranslationArray[dimNum] = Math.round(primTranslationArray[dimNum]);
        }
        
        
        // x-axis is the nanowire growth direction, no need to center
        primTranslationArray[0] = 0;
        
        /*
        System.out.println("\nprint out primTranslationArray: ");
        for(int i=0; i< primTranslationArray.length; i++){
            System.out.print(primTranslationArray[i] + ",  ");
        }
        System.out.println("\n");
        */
        
        Vector primTranslation = new Vector(primTranslationArray, primBasis);
        
        StructureBuilder builder = new StructureBuilder(structure);
        for (int siteNum = 0; siteNum < builder.numDefiningSites(); siteNum++) {
          builder.setSiteCoordinates(siteNum, builder.getSiteCoords(siteNum).translateBy(primTranslation));
        }
        return primStructure.getSuperStructure(builder);
        
      }
 

    
    public static void checkNanoparticleRelaxation_driver(double relaxRatio, double cellRatio, String poscarDir, String contcarDir, String structListDir, String structList){
        
        
        StructureListFile enerIn = new StructureListFile(structListDir + structList);

        for (int entryNum = 0; entryNum < enerIn.numEntries(); entryNum++) {
            String fileName = enerIn.getFileName(entryNum);
            
            //POSCAR infile = new POSCAR(STRUCT_DIR + fileName, true);

            //Structure structure = new Structure(infile);

            /*
            int numPt = structure.numDefiningSitesWithElement(Element.platinum);
            int numNi = structure.numDefiningSitesWithElement(Element.nickel);
            int numMo = structure.numDefiningSitesWithElement(Element.molybdenum);

            int numH = structure.numDefiningSitesWithElement(Element.hydrogen);
            */
            
            //simpleRelaxationCleanNP_new(poscarDir, contcarDir, fileName);
            
            simpleRelaxationOHDecorated_new(poscarDir, contcarDir, fileName, relaxRatio, cellRatio);

        }
        
    }
    
    
    
    public static boolean simpleRelaxationCleanNP_new(String poscarDir, String contcarDir, String fileName) {
        
        
        String contcarPath = contcarDir + "/" + fileName.substring(0, fileName.length()-5) + "/" + "CONTCAR";
        
        if (! new File(contcarPath).exists()) {
          Status.detail("No CONTCAR found for " + fileName);
          return true;
        }
        POSCAR poscar = new POSCAR(poscarDir + "/" + fileName);
        POSCAR contcar = new POSCAR(contcarPath);
        poscar.setVectorPeriodicity(0, false);
        poscar.setVectorPeriodicity(1, false);
        poscar.setVectorPeriodicity(2, false);
        contcar.setVectorPeriodicity(0, false);
        contcar.setVectorPeriodicity(1, false);
        contcar.setVectorPeriodicity(2, false);
        Structure poscarStruct = new Structure(poscar);
        Structure contcarStruct = new Structure(contcar);

        double oldStatus = Status.getLogLevel();
        Status.setLogLevelError();
        StructureMapper mapper = new StructureMapper(poscarStruct, contcarStruct);
        mapper.setMaxAllowedRMSError(1);
        mapper.setMaxAllowedOffsetDifferenceScale(0.75);
        boolean returnValue = mapper.mapExists();
        Status.setLogLevel(oldStatus);
        if (!returnValue) {
            System.out.println("Not Accepted: " + fileName);
        }
        else{
            System.out.println("Accepted: " + fileName);
        }
        return returnValue;
    }
    
    
    public static boolean simpleRelaxationOHDecorated_new(String poscarDir, String contcarDir, String fileName, double relaxRatio, double cellRatio) {
        
        //System.out.println("struct: " + fileName);
        
        //String contcarPath = contcarDir + "/" + fileName.substring(0, fileName.length()-5) + "/" + "CONTCAR";
        String contcarPath = contcarDir + "/" + fileName + "/" + "CONTCAR";
        
        //String poscarPath = poscarDir + "/" + fileName + "/" + "POSCAR";
        String poscarPath = poscarDir + "/" + fileName + "/" + "CONTCAR-decorates.vasp";

        
        if (! new File(contcarPath).exists()) {
          Status.detail("No CONTCAR found for " + fileName);
          return true;
        }
        //POSCAR poscar = new POSCAR(poscarDir + "/" + fileName);
        POSCAR poscar = new POSCAR(poscarPath);
        //poscar.setScaleFactor(cellRatio);
        
        POSCAR contcar = new POSCAR(contcarPath);
        poscar.setVectorPeriodicity(0, false);
        poscar.setVectorPeriodicity(1, false);
        poscar.setVectorPeriodicity(2, false);
        contcar.setVectorPeriodicity(0, false);
        contcar.setVectorPeriodicity(1, false);
        contcar.setVectorPeriodicity(2, false);
        Structure poscarStruct = new Structure(poscar);
        Structure contcarStruct = new Structure(contcar);

        int numPt = poscarStruct.numDefiningSitesWithElement(Element.platinum);
        int numNi = poscarStruct.numDefiningSitesWithElement(Element.nickel);
        int numMo = poscarStruct.numDefiningSitesWithElement(Element.molybdenum);

        int numO = poscarStruct.numDefiningSitesWithElement(Element.oxygen);
        int numH = poscarStruct.numDefiningSitesWithElement(Element.hydrogen);
        int initial_totalAtoms = poscarStruct.numDefiningSites();
        
        //System.out.println("numPt, numNi, numO, numH: " + numPt + ", " + numNi + ", " + numO + ", " + numH + ", " + poscarStruct.numDefiningSites());
        
        if(numH!=0){
            //TODO
            // remove all hydorgen atoms for both CONTCAR & POSCAR structures.
            StructureBuilder poscarBuilder = new StructureBuilder(poscarStruct); // builder used to add atoms to the "structure"
            StructureBuilder contcarBuilder = new StructureBuilder(contcarStruct); // builder used to add atoms to the "structure"
            
            //System.out.println("# of DefiningSites for poscar, contcar: " + poscarStruct.numDefiningSites() + ",    " + poscarStruct.numDefiningSites());
            int num_removedH = 0;
            
            poscarBuilder.removeAllSites();
            contcarBuilder.removeAllSites();
            
            for (int siteNum = 0; siteNum < initial_totalAtoms; siteNum++) {
                
                Structure.Site site = poscarStruct.getDefiningSite(siteNum);
                Coordinates coords = site.getCoords();
                Species spec = site.getSpecies();

                Structure.Site site_contcar = contcarStruct.getDefiningSite(siteNum);
                Coordinates coords_contcar = site_contcar.getCoords();
                
                if(spec!=Species.hydrogen){
                    poscarBuilder.addSite(coords, spec);
                    contcarBuilder.addSite(coords_contcar, spec);
                }
                else{
                    num_removedH++;
                }
            }
            
            poscarStruct = new Structure(poscarBuilder);
            contcarStruct = new Structure(contcarBuilder);
            
            POSCAR outfilePOS= new POSCAR(poscarStruct);
            outfilePOS.writeFile(poscarDir + "/" + fileName  + "/POSCAR-H-Removed.vasp");
            
            POSCAR outfileCONT= new POSCAR(contcarStruct);
            outfileCONT.writeFile(contcarDir + "/" + fileName  + "/CONTCAR-H-Removed.vasp");

            //System.out.println("after using builder class to remove H atoms: ");
            //System.out.println("# of DefiningSites for poscar, contcar: " + poscarStruct.numDefiningSites() + ",    " + poscarStruct.numDefiningSites());

            /*
            for (int siteNum = 0; siteNum < initial_totalAtoms; siteNum++) {
                
                Structure.Site site = poscarStruct.getDefiningSite(siteNum);

                if(site.getSpecies()==Species.hydrogen){ // if the surface site is sulfur, which stands for Pt-OH, then replace the S-atom with Pt-OH
                    Structure.Site siteCONTCAR = contcarStruct.getDefiningSite(siteNum);

                    poscarStruct.removeDefiningSite(siteNum);
                    contcarStruct.removeDefiningSite(siteNum);
                    siteNum--;
                    
                    initial_totalAtoms--;
                    
                    
                    //site.setSpecies(Species.vacancy);
                    //siteCONTCAR.setSpecies(Species.vacancy);
                    
                    
                    num_removedH++;
                }
            }
            */
            
            
            
            if(num_removedH!=numH){
                System.out.println("ERROR: the numH != num_removedH");
                return false;
            }
            
        }
        
        //System.out.println("struct: " + fileName + ", numH after removing: " + poscarStruct.numDefiningSitesWithElement(Element.hydrogen) + ", " + poscarStruct.numDefiningSites());
        
        double oldStatus = Status.getLogLevel();
        Status.setLogLevelError();
        //StructureMapper mapper = new StructureMapper(poscarStruct, contcarStruct);
        StructureMapper mapper = new StructureMapper(contcarStruct, poscarStruct);
        mapper.setMaxAllowedRMSError(1);
        mapper.setMaxAllowedOffsetDifferenceScale(relaxRatio);
        //System.out.println("\n      m_MaxAllowedOffsetDifference=" + mapper.getMaxAllowedOffsetDifference());
        boolean returnValue = mapper.mapExists();
        Status.setLogLevel(oldStatus);
        if (!returnValue) {
            System.out.println("Not Accepted: " + fileName);
        }
        else{
            //System.out.println("Accepted: " + fileName);
        }
        return returnValue;
    }
    
    
    
    
    public static boolean simpleRelaxation(String fileName) {
        
        String contcarPath = CONTCAR_DIR + "/" + "CONTCAR-" + fileName;
        
        if (! new File(contcarPath).exists()) {
          Status.detail("No CONTCAR found for " + fileName);
          return true;
        }
        POSCAR poscar = new POSCAR(STRUCT_DIR + "/" + fileName);
        POSCAR contcar = new POSCAR(contcarPath);
        poscar.setVectorPeriodicity(0, false);
        poscar.setVectorPeriodicity(1, false);
        poscar.setVectorPeriodicity(2, false);
        contcar.setVectorPeriodicity(0, false);
        contcar.setVectorPeriodicity(1, false);
        contcar.setVectorPeriodicity(2, false);
        Structure poscarStruct = new Structure(poscar);
        Structure contcarStruct = new Structure(contcar);

        double oldStatus = Status.getLogLevel();
        Status.setLogLevelError();
        //StructureMapper mapper = new StructureMapper(poscarStruct, contcarStruct);
        StructureMapper mapper = new StructureMapper(contcarStruct, poscarStruct);
        mapper.setMaxAllowedRMSError(1);
        mapper.setMaxAllowedOffsetDifferenceScale(0.75);
        boolean returnValue = mapper.mapExists();
        Status.setLogLevel(oldStatus);
        if (!returnValue) {
            System.out.println("Not Accepted: " + fileName);
        }
        return returnValue;
    }
    
    
    
    
    
    
    
    /**
     * 
     * method recorderCompositionEachLayer needs to get rid of 1st line, which stores the information of temperature.
     *  return composition of Pt, Ni and S(*OH@Pt) in order of 1st, 2nd, 3rd, 4th, 5th and core compositions
     */
    public static double[] recorderCompositionEachLayer_PtNiOH(String fileDir, String structName, String fileNameMo, String fileNameNi, String fileNamePt) {
        
        ClusterExpansion ce = NanoFitClusterExpansion_PtNiOH.buildCE_PtNiOH();
        PRIM infile = new PRIM(fileDir + "/" + structName);
        SuperStructure superStructure = ce.getBaseStructure().getSuperStructure(infile);
        
        double[] moValues = new double[superStructure.numDefiningSites()];
        double[] niValues = new double[superStructure.numDefiningSites()];
        double[] ptValues = new double[superStructure.numDefiningSites()];


        double[] vacancyValues = new double[superStructure.numDefiningSites()];
        try {

          LineNumberReader moReader = new LineNumberReader(new FileReader(fileDir + "/" + fileNameMo));
          LineNumberReader niReader = new LineNumberReader(new FileReader(fileDir + "/" + fileNameNi));
          LineNumberReader ptReader = new LineNumberReader(new FileReader(fileDir + "/" + fileNamePt));
          //LineNumberReader vacancyReader = new LineNumberReader(new FileReader(fileDir + "/" + fileNameVacancy));

          String moLine = moReader.readLine();
          String niLine = niReader.readLine();
          String ptLine = ptReader.readLine();
          //String vacancyLine = vacancyReader.readLine();
          
          /*
           * get rid of 1st line, which tells the info of temperature
           */
          moLine = moReader.readLine();
          niLine = niReader.readLine();
          ptLine = ptReader.readLine();
          for (int sigmaIndex = 0; sigmaIndex < niValues.length; sigmaIndex++) {
            

            StringTokenizer moTokenizer = new StringTokenizer(moLine);
            StringTokenizer niTokenizer = new StringTokenizer(niLine);
            StringTokenizer ptTokenizer = new StringTokenizer(ptLine);
            //StringTokenizer vacancyTokenizer = new StringTokenizer(vacancyLine);
            
            moTokenizer.nextToken();
            niTokenizer.nextToken();
            ptTokenizer.nextToken();
            //vacancyTokenizer.nextToken();
            
            moValues[sigmaIndex] += Double.parseDouble(moTokenizer.nextToken());
            niValues[sigmaIndex] += Double.parseDouble(niTokenizer.nextToken());
            ptValues[sigmaIndex] += Double.parseDouble(ptTokenizer.nextToken());
            //vacancyValues[sigmaIndex] += Double.parseDouble(vacancyTokenizer.nextToken());
            
            moLine = moReader.readLine();
            niLine = niReader.readLine();
            ptLine = ptReader.readLine();
            //vacancyLine = vacancyReader.readLine();
          }
          
          moReader.close();
          niReader.close();
          ptReader.close();
          //vacancyReader.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
        
        //niValues = symmetrizeValues(superStructure, niValues);
        //ptValues = symmetrizeValues(superStructure, ptValues);
        //vacancyValues = symmetrizeValues(superStructure, vacancyValues);
        
        //Arrays.fill(moValues, 0);
        
        //TODO, add codes to decorate all sites in vacancy, to distinguish from the atoms within the core of nanoparticles.
        
        int numSitesVacancy = 0;
        int numSitesLayer1 = 0;
        int numSitesLayer2 = 0;
        int numSitesLayer3 = 0;
        int numSitesLayer4 = 0;
        int numSitesLayer5 = 0;
        int numSitesCore = 0;
        boolean[] vacancySites = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {
              vacancySites[sigmaIndex] = true;
              numSitesVacancy ++;
          }

        }
        
        
        boolean[] surfaceSites = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          //if (vacancySites[sigmaIndex]) {continue;}
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (neighbors[neighborNum].getSpecies() == Species.vacancy) {
              surfaceSites[sigmaIndex] = true;
              numSitesLayer1++;
              break;
            }
          }
        }
        
        boolean[] surfaceSites2 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites[neighbors[neighborNum].getIndex()]) {
              surfaceSites2[sigmaIndex] = true;
              numSitesLayer2++;
              break;
            }
          }
        }
        
        boolean[] surfaceSites3 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites2[neighbors[neighborNum].getIndex()]) {
              surfaceSites3[sigmaIndex] = true;
              numSitesLayer3++;
              break;
            }
          }
        }
        
        boolean[] surfaceSites4 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          if (surfaceSites3[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites3[neighbors[neighborNum].getIndex()]) {
              surfaceSites4[sigmaIndex] = true;
              numSitesLayer4++;
              break;
            }
          }
        }
        
        
        boolean[] surfaceSites5 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          if (surfaceSites3[sigmaIndex]) {continue;}
          if (surfaceSites4[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites4[neighbors[neighborNum].getIndex()]) {
              surfaceSites5[sigmaIndex] = true;
              numSitesLayer5++;
              break;
            }
          }
        }
        
        /*
         * check the sites wihin the core(here, is deeper than 5th layer)
         */
        boolean[] coreSites = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          if (surfaceSites3[sigmaIndex]) {continue;}
          if (surfaceSites4[sigmaIndex]) {continue;}
          if (surfaceSites5[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          coreSites[sigmaIndex] = true;
          numSitesCore ++;
        }
        
        
        /*
         * calculate the composition for each layer
         */
        
        int numSitesVacancyTemp = 0;
        int numSitesL1Temp = 0;
        int numSitesL2Temp = 0;
        int numSitesL3Temp = 0;
        int numSitesL4Temp = 0;
        int numSitesL5Temp = 0;
        int numSitesCoreTemp = 0;
        
        double compositionPtL1 = 0;
        double compositionPtL2 = 0;
        double compositionPtL3 = 0;
        double compositionPtL4 = 0;
        double compositionPtL5 = 0;
        double compositionPtCore = 0;
        
        double compositionNiL1 = 0;
        double compositionNiL2 = 0;
        double compositionNiL3 = 0;
        double compositionNiL4 = 0;
        double compositionNiL5 = 0;
        double compositionNiCore = 0;
        
        double compositionMoL1 = 0;
        double compositionMoL2 = 0;
        double compositionMoL3 = 0;
        double compositionMoL4 = 0;
        double compositionMoL5 = 0;
        double compositionMoCore = 0;
        
        for (int siteNum = 0; siteNum < infile.numDefiningSites(); siteNum++) {
            Coordinates coords = infile.getSiteCoords(siteNum);
            int sigmaIndex = superStructure.getSiteIndex(coords);
            
            if(vacancySites[sigmaIndex]){
                numSitesVacancyTemp ++;
                continue;
            }
            
            if(surfaceSites[sigmaIndex]){
                numSitesL1Temp ++;
                compositionMoL1 += moValues[sigmaIndex];
                compositionNiL1 += niValues[sigmaIndex];
                compositionPtL1 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites2[sigmaIndex]){
                numSitesL2Temp ++;
                compositionMoL2 += moValues[sigmaIndex];
                compositionNiL2 += niValues[sigmaIndex];
                compositionPtL2 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites3[sigmaIndex]){
                numSitesL3Temp ++;
                compositionMoL3 += moValues[sigmaIndex];
                compositionNiL3 += niValues[sigmaIndex];
                compositionPtL3 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites4[sigmaIndex]){
                numSitesL4Temp ++;
                compositionMoL4 += moValues[sigmaIndex];
                compositionNiL4 += niValues[sigmaIndex];
                compositionPtL4 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites5[sigmaIndex]){
                numSitesL5Temp ++;
                compositionMoL5 += moValues[sigmaIndex];
                compositionNiL5 += niValues[sigmaIndex];
                compositionPtL5 += ptValues[sigmaIndex];
                continue;
            }
            
            if(coreSites[sigmaIndex]){
                numSitesCoreTemp ++;
                compositionMoCore += moValues[sigmaIndex];
                compositionNiCore += niValues[sigmaIndex];
                compositionPtCore += ptValues[sigmaIndex];
                continue;
            }
            
            System.out.println("this sigmaIndex is not considered when recorder the position of each site: " + sigmaIndex);
                     
        } // end of for loop
        
        
        if(numSitesL1Temp != numSitesLayer1){
            System.out.println("# of 1st layer does not agree!");
        }
        if(numSitesL2Temp != numSitesLayer2){
            System.out.println("# of 2nd layer does not agree!");
        }
        if(numSitesL3Temp != numSitesLayer3){
            System.out.println("# of 3rd layer does not agree!");
        }
        if(numSitesL4Temp != numSitesLayer4){
            System.out.println("# of 4th does not agree!");
        }
        if(numSitesL5Temp != numSitesLayer5){
            System.out.println("# of 5th does not agree!");
        }
        if(numSitesCoreTemp != numSitesCore){
            System.out.println("# of core sites does not agree!");
        }
        
        compositionMoL1 = compositionMoL1/ numSitesLayer1;
        compositionNiL1 = compositionNiL1/ numSitesLayer1;
        compositionPtL1 = compositionPtL1/ numSitesLayer1;
        
        compositionMoL2 = compositionMoL2/ numSitesLayer2;
        compositionNiL2 = compositionNiL2/ numSitesLayer2;
        compositionPtL2 = compositionPtL2/ numSitesLayer2;
        
        compositionMoL3 = compositionMoL3/ numSitesLayer3;
        compositionNiL3 = compositionNiL3/ numSitesLayer3;
        compositionPtL3 = compositionPtL3/ numSitesLayer3;
        
        compositionMoL4 = compositionMoL4/ numSitesLayer4;
        compositionNiL4 = compositionNiL4/ numSitesLayer4;
        compositionPtL4 = compositionPtL4/ numSitesLayer4;
        
        compositionMoL5 = compositionMoL5/ numSitesLayer5;
        compositionNiL5 = compositionNiL5/ numSitesLayer5;
        compositionPtL5 = compositionPtL5/ numSitesLayer5;
        
        compositionMoCore = compositionMoCore/ numSitesCore;
        compositionNiCore = compositionNiCore/ numSitesCore;
        compositionPtCore = compositionPtCore/ numSitesCore;
        
        int numAllAtoms = numSitesLayer1 + numSitesLayer2 + numSitesLayer3 + numSitesLayer4 + numSitesLayer5 + numSitesCore;
        
        System.out.println("# of sites for each layer are: " + numAllAtoms + "  " + numSitesLayer1 + "  " + numSitesLayer2 + "  " + numSitesLayer3 + "  " + numSitesLayer4 + "  " + numSitesLayer5 + "   " + numSitesCore + "    " + numSitesVacancy);
        
        System.out.println("Mo% for each layer are: " + compositionMoL1 + "  " + compositionMoL2 + "  " + compositionMoL3 + "  " + compositionMoL4 + "  " + compositionMoL5 + "   " + compositionMoCore);
        System.out.println("Ni% for each layer are: " + compositionNiL1 + "  " + compositionNiL2 + "  " + compositionNiL3 + "  " + compositionNiL4 + "  " + compositionNiL5 + "   " + compositionNiCore);
        System.out.println("Pt% for each layer are: " + compositionPtL1 + "  " + compositionPtL2 + "  " + compositionPtL3 + "  " + compositionPtL4 + "  " + compositionPtL5 + "   " + compositionPtCore);

        double[] compositionProfiles = new double[26];

        compositionProfiles[0] = compositionPtL1;
        compositionProfiles[1] = compositionPtL2;
        compositionProfiles[2] = compositionPtL3;
        compositionProfiles[3] = compositionPtL4;
        compositionProfiles[4] = compositionPtL5;
        compositionProfiles[5] = compositionPtCore;
        
        compositionProfiles[6] = compositionNiL1;
        compositionProfiles[7] = compositionNiL2;
        compositionProfiles[8] = compositionNiL3;
        compositionProfiles[9] = compositionNiL4;
        compositionProfiles[10] = compositionNiL5;
        compositionProfiles[11] = compositionNiCore;
        
        compositionProfiles[12] = compositionMoL1;
        compositionProfiles[13] = compositionMoL2;
        compositionProfiles[14] = compositionMoL3;
        compositionProfiles[15] = compositionMoL4;
        compositionProfiles[16] = compositionMoL5;
        compositionProfiles[17] = compositionMoCore;
        
        compositionProfiles[18] = numAllAtoms;
        compositionProfiles[19] = numSitesLayer1;
        compositionProfiles[20] = numSitesLayer2;
        compositionProfiles[21] = numSitesLayer3;
        compositionProfiles[22] = numSitesLayer4;
        compositionProfiles[23] = numSitesLayer5;
        compositionProfiles[24] = numSitesCore;
        compositionProfiles[25] = numSitesVacancy;
        
        return compositionProfiles;
        
        
      }
    
    
    
    
    
    
    
    /**
     * 
     * method recorderCompositionEachLayer needs to get rid of 1st line, which stores the information of temperature.
     *  return composition of Pt, and Ni in order of 1st, 2nd, 3rd, 4th, 5th and core compositions
     */
    public static double[] recorderCompositionEachLayer_PtNi(String fileDir, String structName, String fileNameNi, String fileNamePt) {
        
        ClusterExpansion ce = NanoFitClusterExpansion_PtNiOH.buildCE_PtNiVac();
        PRIM infile = new PRIM(fileDir + "/" + structName);
        SuperStructure superStructure = ce.getBaseStructure().getSuperStructure(infile);
        
        double[] niValues = new double[superStructure.numDefiningSites()];
        double[] ptValues = new double[superStructure.numDefiningSites()];


        double[] vacancyValues = new double[superStructure.numDefiningSites()];
        try {

          LineNumberReader niReader = new LineNumberReader(new FileReader(fileDir + "/" + fileNameNi));
          LineNumberReader ptReader = new LineNumberReader(new FileReader(fileDir + "/" + fileNamePt));
          //LineNumberReader vacancyReader = new LineNumberReader(new FileReader(fileDir + "/" + fileNameVacancy));

          String niLine = niReader.readLine();
          String ptLine = ptReader.readLine();
          //String vacancyLine = vacancyReader.readLine();
          
          /*
           * get rid of 1st line, which tells the info of temperature
           */
          niLine = niReader.readLine();
          ptLine = ptReader.readLine();
          for (int sigmaIndex = 0; sigmaIndex < niValues.length; sigmaIndex++) {
            

            StringTokenizer niTokenizer = new StringTokenizer(niLine);
            StringTokenizer ptTokenizer = new StringTokenizer(ptLine);
            //StringTokenizer vacancyTokenizer = new StringTokenizer(vacancyLine);
            
            niTokenizer.nextToken();
            ptTokenizer.nextToken();
            //vacancyTokenizer.nextToken();
            
            niValues[sigmaIndex] += Double.parseDouble(niTokenizer.nextToken());
            ptValues[sigmaIndex] += Double.parseDouble(ptTokenizer.nextToken());
            //vacancyValues[sigmaIndex] += Double.parseDouble(vacancyTokenizer.nextToken());
            
            niLine = niReader.readLine();
            ptLine = ptReader.readLine();
            //vacancyLine = vacancyReader.readLine();
          }
          
          niReader.close();
          ptReader.close();
          //vacancyReader.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
        
        //niValues = symmetrizeValues(superStructure, niValues);
        //ptValues = symmetrizeValues(superStructure, ptValues);
        //vacancyValues = symmetrizeValues(superStructure, vacancyValues);
        
        //Arrays.fill(moValues, 0);
        
        //TODO, add codes to decorate all sites in vacancy, to distinguish from the atoms within the core of nanoparticles.
        
        int numSitesVacancy = 0;
        int numSitesLayer1 = 0;
        int numSitesLayer2 = 0;
        int numSitesLayer3 = 0;
        int numSitesLayer4 = 0;
        int numSitesLayer5 = 0;
        int numSitesCore = 0;
        boolean[] vacancySites = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {
              vacancySites[sigmaIndex] = true;
              numSitesVacancy ++;
          }

        }
        
        
        boolean[] surfaceSites = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          //if (vacancySites[sigmaIndex]) {continue;}
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (neighbors[neighborNum].getSpecies() == Species.vacancy) {
              surfaceSites[sigmaIndex] = true;
              numSitesLayer1++;
              break;
            }
          }
        }
        
        boolean[] surfaceSites2 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites[neighbors[neighborNum].getIndex()]) {
              surfaceSites2[sigmaIndex] = true;
              numSitesLayer2++;
              break;
            }
          }
        }
        
        boolean[] surfaceSites3 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites2[neighbors[neighborNum].getIndex()]) {
              surfaceSites3[sigmaIndex] = true;
              numSitesLayer3++;
              break;
            }
          }
        }
        
        boolean[] surfaceSites4 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          if (surfaceSites3[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites3[neighbors[neighborNum].getIndex()]) {
              surfaceSites4[sigmaIndex] = true;
              numSitesLayer4++;
              break;
            }
          }
        }
        
        
        boolean[] surfaceSites5 = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          if (surfaceSites3[sigmaIndex]) {continue;}
          if (surfaceSites4[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          Structure.Site[] neighbors = superStructure.getNearbySites(site.getCoords(), 2.6, false);
          for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
            if (surfaceSites4[neighbors[neighborNum].getIndex()]) {
              surfaceSites5[sigmaIndex] = true;
              numSitesLayer5++;
              break;
            }
          }
        }
        
        /*
         * check the sites wihin the core(here, is deeper than 5th layer)
         */
        boolean[] coreSites = new boolean[niValues.length];
        for (int sigmaIndex = 0; sigmaIndex < superStructure.numDefiningSites(); sigmaIndex++) {

          if (surfaceSites[sigmaIndex]) {continue;}
          if (surfaceSites2[sigmaIndex]) {continue;}
          if (surfaceSites3[sigmaIndex]) {continue;}
          if (surfaceSites4[sigmaIndex]) {continue;}
          if (surfaceSites5[sigmaIndex]) {continue;}
          
          Structure.Site site = superStructure.getDefiningSite(sigmaIndex);
          if (site.getSpecies() == Species.vacancy) {continue;}
          
          coreSites[sigmaIndex] = true;
          numSitesCore ++;
        }
        
        
        /*
         * calculate the composition for each layer
         */
        
        int numSitesVacancyTemp = 0;
        int numSitesL1Temp = 0;
        int numSitesL2Temp = 0;
        int numSitesL3Temp = 0;
        int numSitesL4Temp = 0;
        int numSitesL5Temp = 0;
        int numSitesCoreTemp = 0;
        
        double compositionPtL1 = 0;
        double compositionPtL2 = 0;
        double compositionPtL3 = 0;
        double compositionPtL4 = 0;
        double compositionPtL5 = 0;
        double compositionPtCore = 0;
        
        double compositionNiL1 = 0;
        double compositionNiL2 = 0;
        double compositionNiL3 = 0;
        double compositionNiL4 = 0;
        double compositionNiL5 = 0;
        double compositionNiCore = 0;
        
        for (int siteNum = 0; siteNum < infile.numDefiningSites(); siteNum++) {
            Coordinates coords = infile.getSiteCoords(siteNum);
            int sigmaIndex = superStructure.getSiteIndex(coords);
            
            if(vacancySites[sigmaIndex]){
                numSitesVacancyTemp ++;
                continue;
            }
            
            if(surfaceSites[sigmaIndex]){
                numSitesL1Temp ++;
                compositionNiL1 += niValues[sigmaIndex];
                compositionPtL1 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites2[sigmaIndex]){
                numSitesL2Temp ++;
                compositionNiL2 += niValues[sigmaIndex];
                compositionPtL2 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites3[sigmaIndex]){
                numSitesL3Temp ++;
                compositionNiL3 += niValues[sigmaIndex];
                compositionPtL3 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites4[sigmaIndex]){
                numSitesL4Temp ++;
                compositionNiL4 += niValues[sigmaIndex];
                compositionPtL4 += ptValues[sigmaIndex];
                continue;
            }
            
            if(surfaceSites5[sigmaIndex]){
                numSitesL5Temp ++;
                compositionNiL5 += niValues[sigmaIndex];
                compositionPtL5 += ptValues[sigmaIndex];
                continue;
            }
            
            if(coreSites[sigmaIndex]){
                numSitesCoreTemp ++;
                compositionNiCore += niValues[sigmaIndex];
                compositionPtCore += ptValues[sigmaIndex];
                continue;
            }
            
            System.out.println("this sigmaIndex is not considered when recorder the position of each site: " + sigmaIndex);
                     
        } // end of for loop
        
        
        if(numSitesL1Temp != numSitesLayer1){
            System.out.println("# of 1st layer does not agree!");
        }
        if(numSitesL2Temp != numSitesLayer2){
            System.out.println("# of 2nd layer does not agree!");
        }
        if(numSitesL3Temp != numSitesLayer3){
            System.out.println("# of 3rd layer does not agree!");
        }
        if(numSitesL4Temp != numSitesLayer4){
            System.out.println("# of 4th does not agree!");
        }
        if(numSitesL5Temp != numSitesLayer5){
            System.out.println("# of 5th does not agree!");
        }
        if(numSitesCoreTemp != numSitesCore){
            System.out.println("# of core sites does not agree!");
        }
        
        compositionNiL1 = compositionNiL1/ numSitesLayer1;
        compositionPtL1 = compositionPtL1/ numSitesLayer1;
        
        compositionNiL2 = compositionNiL2/ numSitesLayer2;
        compositionPtL2 = compositionPtL2/ numSitesLayer2;
        
        compositionNiL3 = compositionNiL3/ numSitesLayer3;
        compositionPtL3 = compositionPtL3/ numSitesLayer3;
        
        compositionNiL4 = compositionNiL4/ numSitesLayer4;
        compositionPtL4 = compositionPtL4/ numSitesLayer4;
        
        compositionNiL5 = compositionNiL5/ numSitesLayer5;
        compositionPtL5 = compositionPtL5/ numSitesLayer5;
        
        compositionNiCore = compositionNiCore/ numSitesCore;
        compositionPtCore = compositionPtCore/ numSitesCore;
        
        int numAllAtoms = numSitesLayer1 + numSitesLayer2 + numSitesLayer3 + numSitesLayer4 + numSitesLayer5 + numSitesCore;
        
        System.out.println("# of sites for each layer are: " + numAllAtoms + "  " + numSitesLayer1 + "  " + numSitesLayer2 + "  " + numSitesLayer3 + "  " + numSitesLayer4 + "  " + numSitesLayer5 + "   " + numSitesCore + "    " + numSitesVacancy);
        
        System.out.println("Ni% for each layer are: " + compositionNiL1 + "  " + compositionNiL2 + "  " + compositionNiL3 + "  " + compositionNiL4 + "  " + compositionNiL5 + "   " + compositionNiCore);
        System.out.println("Pt% for each layer are: " + compositionPtL1 + "  " + compositionPtL2 + "  " + compositionPtL3 + "  " + compositionPtL4 + "  " + compositionPtL5 + "   " + compositionPtCore);

        double[] compositionProfiles = new double[20];

        compositionProfiles[0] = compositionPtL1;
        compositionProfiles[1] = compositionPtL2;
        compositionProfiles[2] = compositionPtL3;
        compositionProfiles[3] = compositionPtL4;
        compositionProfiles[4] = compositionPtL5;
        compositionProfiles[5] = compositionPtCore;
        
        compositionProfiles[6] = compositionNiL1;
        compositionProfiles[7] = compositionNiL2;
        compositionProfiles[8] = compositionNiL3;
        compositionProfiles[9] = compositionNiL4;
        compositionProfiles[10] = compositionNiL5;
        compositionProfiles[11] = compositionNiCore;
        
        compositionProfiles[12] = numAllAtoms;
        compositionProfiles[13] = numSitesLayer1;
        compositionProfiles[14] = numSitesLayer2;
        compositionProfiles[15] = numSitesLayer3;
        compositionProfiles[16] = numSitesLayer4;
        compositionProfiles[17] = numSitesLayer5;
        compositionProfiles[18] = numSitesCore;
        compositionProfiles[19] = numSitesVacancy;
        
        return compositionProfiles;
        
        
      }
    
    
    
    
    
}

